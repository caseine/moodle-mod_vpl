<?php
// This file is part of VPL for Moodle - http://vpl.dis.ulpgc.es/
//
// VPL for Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// VPL for Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with VPL for Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package mod_vpl
 * @copyright 2014 Juan Carlos Rodríguez-del-Pino
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author Juan Carlos Rodríguez-del-Pino <jcrodriguez@dis.ulpgc.es>
 */

// TODO Organize security checks.

defined( 'MOODLE_INTERNAL' ) || die();

global $CFG;

require_once($CFG->libdir . '/externallib.php');
require_once(dirname( __FILE__ ) . '/locallib.php');
require_once(dirname( __FILE__ ) . '/forms/edit.class.php');
require_once(dirname( __FILE__ ) . '/vpl_submission.class.php');
require_once(dirname( __FILE__ ) . '/mod_form.php');

/**
 * @codeCoverageIgnore
 */
class mod_vpl_webservice extends external_api {

    protected static function cmid_parameter() {
        return new external_value( PARAM_INT, 'Activity id (course_module)', VALUE_REQUIRED );
    }

    protected static function password_parameter() {
        return new external_value( PARAM_RAW, 'Activity password', VALUE_DEFAULT, '' );
    }

    protected static function files_structure($required = VALUE_REQUIRED, $default = null) {
        return new external_multiple_structure( new external_single_structure( [
                'name' => new external_value( PARAM_RAW, 'File name', VALUE_REQUIRED ),
                'data' => new external_value( PARAM_RAW, 'File content', VALUE_REQUIRED ),
                'isbinary' => new external_value( PARAM_BOOL, 'True if file is binary encoded in base 64', VALUE_OPTIONAL )
        ] ), 'Files', $required, $default );
    }

    protected static function encode_files_for_webservice($files) {
        $wsfiles = [];
        // Adapt array[name]=content to format array[]=array(name,data).
        foreach ($files as $filename => $filecontents) {
            $wsfile = [];
            $wsfile['name'] = $filename;
            if (vpl_is_binary($filename, $filecontents)) {
                // File is binary, encode it in base 64 and indicate that it should be decoded.
                $wsfile['data'] = base64_encode($filecontents);
                $wsfile['isbinary'] = 1;
            } else {
                $wsfile['data'] = $filecontents;
                $wsfile['isbinary'] = 0;
            }
            $wsfiles[] = $wsfile;
        }
        return $wsfiles;
    }

    protected static function decode_files_from_webservice($wsfiles) {
        $files = [];
        // Adapts to the file format VPL3.2.
        foreach ($wsfiles as $file) {
            $filename = $file['name'];
            $filecontents = $file['data'];
            if (!empty($file['isbinary'])) {
                // File is base 64 encoded and should be decoded before saving.
                $filename = preg_replace('/\\.b64$/i', '', $filename);
                $filecontents = base64_decode($filecontents);
            }
            $files[$filename] = $filecontents;
        }
        return $files;
    }

    /**
     * Rewrite a text by replacing pluginfile.php/... by tokenpluginfile.php/<token>/...,
     * so that these files are readable from an external source.
     * @param string $text
     * @param int $contextid
     * @return string Text with rewritten pluginfiles to be accessible from external sources.
     */
    protected static function rewrite_pluginfile_for_external($text, $contextid) {
        // First, re-encode urls into @@PLUGINFILE@@ tokens.
        $reencoded = file_rewrite_pluginfile_urls($text, 'pluginfile.php', $contextid,
                'mod_vpl', 'intro', null, [ 'reverse' => true ]);
        // Then, re-decode these @@PLUGINFILE@@ tokens into the tokenpluginfile form.
        return file_rewrite_pluginfile_urls($reencoded, 'pluginfile.php', $contextid,
                'mod_vpl', 'intro', null, [ 'includetoken' => true ]);
    }

    /**
     * Returns VPL activity object for coursemodule id.
     * Does checks fro required netwok and password if setted.
     *
     * @param int $id The coursemodule id.
     * @param string $pssword The password for using the VPL activity.
     * @return \mod_vpl object or throws exception if not available.
     */
    private static function initial_checks($id, $password) {
        $vpl = new mod_vpl( $id );
        if (! $vpl->has_capability( VPL_GRADE_CAPABILITY )) {
            if (! $vpl->pass_network_check()) {
                $message = get_string( 'opnotallowfromclient', VPL ) . ' ' . getremoteaddr();
                throw new Exception( $message );
            }
            if (! $vpl->pass_password_check( $password )) {
                throw new moodle_exception('requiredpassword', VPL);
            }
        }
        return $vpl;
    }

    /*
     * info function. return information of the activity
     */
    public static function info_parameters() {
        return new external_function_parameters( [
                'id' => static::cmid_parameter(),
                'password' => static::password_parameter()
        ], 'Parameters', VALUE_REQUIRED);
    }
    public static function info($id, $password) {
        list($id, $password) = array_values(self::validate_parameters( self::info_parameters(), [
                'id' => $id,
                'password' => $password,
        ] ));
        $context = context_module::instance($id);
        self::validate_context($context);

        $vpl = self::initial_checks( $id, $password );
        $vpl->require_capability( VPL_VIEW_CAPABILITY );
        if (! $vpl->is_visible()) {
            throw new Exception( get_string( 'notavailable' ) );
        }
        $instance = $vpl->get_instance();
        $ret = [
                'name' => format_string($instance->name),
                'shortdescription' => format_string($instance->shortdescription),
                'intro' => self::rewrite_pluginfile_for_external($vpl->get_fulldescription(), $context->id),
                'introformat' => ( int ) $instance->introformat,
                'reqpassword' => ($instance->password > '' ? 1 : 0),
                'example' => ( int ) $instance->example,
                'restrictededitor' => ( int ) $instance->restrictededitor,
                'maxfiles' => ( int ) $instance->maxfiles,
                'reqfiles' => [],
                'execfiles' => [],
                'corrfiles' => [],
        ];
        $files = $vpl->get_fgm('required')->getAllFiles();
        $ret['reqfiles'] = static::encode_files_for_webservice( $files );

        $hasteacherrights = $vpl->has_capability( VPL_MANAGE_VIA_WS_CAPABILITY );
        if ($hasteacherrights) {
            $execfiles = $vpl->get_fgm('execution')->getAllFiles();
            $ret['execfiles'] = static::encode_files_for_webservice( $execfiles );
            $correctedfiles = $vpl->get_fgm('corrected')->getAllFiles();
            $ret['corrfiles'] = static::encode_files_for_webservice( $correctedfiles );
        }
        return $ret;
    }
    public static function info_returns() {
        return new external_single_structure( [
                'name' => new external_value( PARAM_TEXT, 'Name', VALUE_REQUIRED),
                'shortdescription' => new external_value( PARAM_TEXT, 'Short description', VALUE_REQUIRED),
                'intro' => new external_value( PARAM_RAW, 'Full description', VALUE_REQUIRED),
                'introformat' => new external_value( PARAM_INT, 'Description format', VALUE_REQUIRED ),
                'reqpassword' => new external_value( PARAM_INT, 'Activity requiere password', VALUE_REQUIRED),
                'example' => new external_value( PARAM_INT, 'Activity is an example', VALUE_REQUIRED),
                'restrictededitor' => new external_value( PARAM_INT, 'Activity edition is restricted', VALUE_REQUIRED),
                'maxfiles' => new external_value( PARAM_INT, 'Maximum number of file acepted', VALUE_REQUIRED),
                'reqfiles' => static::files_structure(),
                'execfiles' => static::files_structure(),
                'corrfiles' => static::files_structure()
        ], 'Parameters', VALUE_REQUIRED);
    }

    /*
     * save teacher files (required, corrected and execution) functions. save/submit the teacher files
     */
    public static function save_required_files_parameters() {
        return self::save_teacher_files_parameters();
    }
    public static function save_required_files( $id, $files = [], $password = '' ) {
        self::save_teacher_files( $id, $files, $password, 'required' );
    }
    public static function save_required_files_returns() {
        return null;
    }

    public static function save_corrected_files_parameters() {
        return self::save_teacher_files_parameters();
    }
    public static function save_corrected_files( $id, $files = [], $password = '' ) {
        self::save_teacher_files( $id, $files, $password, 'corrected' );
    }
    public static function save_corrected_files_returns() {
        return null;
    }

    public static function save_execution_files_parameters() {
        return self::save_teacher_files_parameters();
    }
    public static function save_execution_files( $id, $files = [], $password = '' ) {
        self::save_teacher_files( $id, $files, $password, 'execution' );
    }
    public static function save_execution_files_returns() {
        return null;
    }

    private static function save_teacher_files_parameters() {
        return new external_function_parameters( [
                'id' => static::cmid_parameter(),
                'files' => static::files_structure(VALUE_DEFAULT, []),
                'password' => static::password_parameter(),
        ] );
    }

    private static function save_teacher_files($id, $files = [], $password = '', $type) {
        global $USER;
        list($id, $files, $password) = array_values(self::validate_parameters( self::save_teacher_files_parameters(), [
                'id' => $id,
                'files' => $files,
                'password' => $password,
        ] ));
        self::validate_context(context_module::instance($id));

        $vpl = self::initial_checks( $id, $password );
        $vpl->require_capability( VPL_MANAGE_VIA_WS_CAPABILITY );
        mod_vpl_edit::save_teacher_files( $vpl, static::decode_files_from_webservice($files), $type );
    }

    /*
     * save function. save/submit the students files
     */
    public static function save_parameters() {
        $descuserid = 'User ID to use (required mod/vpl:manage capability)';
        return new external_function_parameters( [
                'id' => static::cmid_parameter(),
                'files' => static::files_structure(),
                'password' => static::password_parameter(),
                'userid' => new external_value( PARAM_INT, $descuserid , VALUE_DEFAULT, -1 ),
                'comments' => new external_value( PARAM_RAW, 'Student\'s comments', VALUE_DEFAULT, '' ),
        ], 'Parameters', VALUE_REQUIRED );
    }
    public static function save($id, $files=[], $password='', $userid=-1, $comments='') {
        global $USER;
        list($id, $files, $password, $userid) = array_values(self::validate_parameters( self::save_parameters(), [
                'id' => $id,
                'files' => $files,
                'password' => $password,
                'userid' => $userid,
                'comments' => $comments,
        ] ));

        self::validate_context(context_module::instance($id));
        $vpl = self::initial_checks( $id, $password );
        if ($userid == -1) {
            $userid = $USER->id;
            $vpl->require_capability( VPL_SUBMIT_CAPABILITY );
            if (! $vpl->is_submit_able()) {
                throw new Exception( get_string( 'notavailable' ) );
            }
        } else {
            $vpl->require_capability( VPL_MANAGE_CAPABILITY );
        }
        $instance = $vpl->get_instance();
        if ($instance->example || ($instance->restrictededitor && ! $vpl->has_capability(VPL_MANAGE_CAPABILITY))) {
            throw new Exception( get_string( 'notavailable' ) );
        }
        mod_vpl_edit::save( $vpl, $userid, static::decode_files_from_webservice($files), $comments );
    }

    public static function save_returns() {
        return null;
    }

    /*
     * open function. return the student's submitted files
     */
    public static function open_parameters() {
        $descuserid = 'User ID to use (required mod/vpl:grade capability)';
        return new external_function_parameters( [
                'id' => static::cmid_parameter(),
                'password' => static::password_parameter(),
                'userid' => new external_value( PARAM_INT, $descuserid, VALUE_DEFAULT, -1 ),
        ], 'Parameters', VALUE_REQUIRED );
    }
    public static function open($id, $password = '', $userid = -1) {
        global $USER;
        list($id, $password, $userid) = array_values(self::validate_parameters( self::open_parameters(), [
                'id' => $id,
                'password' => $password,
                'userid' => $userid,
        ] ));
        self::validate_context(context_module::instance($id));

        $vpl = self::initial_checks( $id, $password );
        $vpl->require_capability( VPL_VIEW_CAPABILITY );
        if ($userid == -1) {
            $userid = $USER->id;
            if (! $vpl->is_visible()) {
                throw new Exception( get_string( 'notavailable' ) );
            }
        } else {
            $vpl->require_capability( VPL_GRADE_CAPABILITY );
        }
        $compilationexecution = new stdClass();
        $files = mod_vpl_edit::get_submitted_files( $vpl, $userid, $compilationexecution );
        $files = static::encode_files_for_webservice( $files );
        $ret = [
                'files' => $files,
                'comments' => '',
                'compilation' => '',
                'evaluation' => '',
                'grade' => '',
        ];
        $attributes = ['compilation', 'evaluation', 'grade', 'comments'];
        foreach ($attributes as $attribute) {
            if (isset($compilationexecution->$attribute)) {
                $ret[$attribute] = $compilationexecution->$attribute;
            }
        }
        return $ret;
    }
    public static function open_returns() {
        return new external_single_structure( [
                'files' => static::files_structure(),
                'comments' => new external_value( PARAM_RAW, 'Student\'s comments', VALUE_REQUIRED ),
                'compilation' => new external_value( PARAM_RAW, 'Compilation result', VALUE_REQUIRED),
                'evaluation' => new external_value( PARAM_RAW, 'Evaluation result', VALUE_REQUIRED),
                'grade' => new external_value( PARAM_RAW, 'Proposed or final grade', VALUE_REQUIRED),
        ], 'Parameters', VALUE_REQUIRED );
    }

    /*
     * evaluate function. evaluate the student's submitted files
     */
    public static function evaluate_parameters() {
        $descuserid = 'User ID to use (required mod/vpl:grade capability)';
        return new external_function_parameters( [
                'id' => static::cmid_parameter(),
                'password' => static::password_parameter(),
                'userid' => new external_value( PARAM_INT, $descuserid , VALUE_DEFAULT, -1 ),
        ], 'Parameters', VALUE_REQUIRED );
    }
    public static function evaluate($id, $password = ' ', $userid = -1) {
        global $USER;
        list($id, $password, $userid) = array_values(self::validate_parameters( self::evaluate_parameters(), [
                'id' => $id,
                'password' => $password,
                'userid' => $userid,
        ] ));
        self::validate_context(context_module::instance($id));

        $vpl = self::initial_checks( $id, $password );
        $instance = $vpl->get_instance();
        if ($userid == -1) {
            $userid = $USER->id;
            $vpl->require_capability( VPL_SUBMIT_CAPABILITY );
        } else {
            $vpl->require_capability( VPL_GRADE_CAPABILITY );
        }
        if (! $vpl->has_capability(VPL_GRADE_CAPABILITY) ) {
            if (! $vpl->is_visible()) {
                throw new Exception( get_string('notavailable') );
            }
            if (! $vpl->is_submit_able()) {
                throw new Exception( get_string('notavailable') );
            }
            if ($instance->example || ! $instance->evaluate) {
                throw new Exception( get_string('notavailable') );
            }
        }

        $res = mod_vpl_edit::execute( $vpl, $userid, 'evaluate' );
        $monitorurl = 'ws://' . $res->server . ':' . $res->port . '/' . $res->monitorPath;
        $smonitorurl = 'wss://' . $res->server . ':' . $res->securePort . '/' . $res->monitorPath;
        return [ 'monitorURL' => $monitorurl, 'smonitorURL' => $smonitorurl  ];
    }
    public static function evaluate_returns() {
        $desc = "URL to the service that monitor the evaluation in the jail server.
Protocol WebSocket may be ws: or wss: (SSL).
The jail send information as text in this format:
    (message|retrieve|close):(state(:detail)?)?
'message': the jail server reports about the changes to the client.
           With 'state' and optional 'detail?'
'retrieve': the client must get the results of the evaluation
            (call mod_vpl_get_result, the server is waiting).
'close': the conection is to be closed.
if the websocket client send something to the server then the evaluation is stopped.";
        return new external_single_structure( [
                'monitorURL' => new external_value( PARAM_RAW, $desc, VALUE_REQUIRED),
                'smonitorURL' => new external_value( PARAM_RAW, $desc, VALUE_REQUIRED),
        ], 'Parameters', VALUE_REQUIRED );
    }

    /*
     * get_result function. retrieve the result of the evaluation
     */
    public static function get_result_parameters() {
        $descuserid = 'User ID to use (required mod/vpl:grade capability)';
        return new external_function_parameters( [
                'id' => static::cmid_parameter(),
                'password' => static::password_parameter(),
                'userid' => new external_value( PARAM_INT, $descuserid , VALUE_DEFAULT, -1 ),
        ] );
    }
    public static function get_result($id, $password = ' ', $userid = -1) {
        global $USER;
        list($id, $password, $userid) = array_values(self::validate_parameters( self::get_result_parameters(), [
                'id' => $id,
                'password' => $password,
                'userid' => $userid,
        ] ));
        self::validate_context(context_module::instance($id));

        $vpl = self::initial_checks( $id, $password );
        $vpl->require_capability( VPL_SUBMIT_CAPABILITY );
        $instance = $vpl->get_instance();
        if ($userid == -1) {
            $userid = $USER->id;
            $vpl->require_capability( VPL_SUBMIT_CAPABILITY );
        } else {
            $vpl->require_capability( VPL_GRADE_CAPABILITY );
        }
        if (! $vpl->has_capability(VPL_GRADE_CAPABILITY) ) {
            if (! $vpl->is_visible()) {
                throw new Exception( get_string('notavailable') );
            }
            if (! $vpl->is_submit_able()) {
                throw new Exception( get_string('notavailable') );
            }
            if ($instance->example || ! $instance->evaluate) {
                throw new Exception( get_string('notavailable') );
            }
        }
        $compilationexecution = mod_vpl_edit::retrieve_result( $vpl, $userid);
        $ret = [
            'compilation' => '',
            'evaluation' => '',
            'grade' => '',
        ];
        $attributes = ['compilation', 'evaluation', 'grade'];
        foreach ($attributes as $attribute) {
            if (isset($compilationexecution->$attribute)) {
                $ret[$attribute] = $compilationexecution->$attribute;
            }
        }
        return $ret;
    }
    public static function get_result_returns() {
        return new external_single_structure( [
                'compilation' => new external_value( PARAM_RAW, 'Compilation result', VALUE_REQUIRED),
                'evaluation' => new external_value( PARAM_RAW, 'Evaluation result', VALUE_REQUIRED),
                'grade' => new external_value( PARAM_RAW, 'Proposed or final grade', VALUE_REQUIRED),
        ], 'Parameters', VALUE_REQUIRED);
    }

    /*
     * get_last_evaluation function. retrieve the last evaluation result
     */
    public static function get_last_evaluation_parameters() {
        return new external_function_parameters( [
                'id' => static::cmid_parameter(),
                'password' => static::password_parameter(),
        ] );
    }
    public static function get_last_evaluation($id, $password) {
        global $USER;
        list($id, $password) = array_values(self::validate_parameters( self::get_last_evaluation_parameters(), [
                'id' => $id,
                'password' => $password,
        ] ));
        self::validate_context(context_module::instance($id));

        $vpl = self::initial_checks( $id, $password );
        $vpl->require_capability( VPL_SUBMIT_CAPABILITY );
        $instance = $vpl->get_instance();
        if (! $vpl->is_submit_able()) {
            throw new Exception( get_string( 'notavailable' ) );
        }
        if ($instance->example or $instance->restrictededitor or ! $instance->evaluate) {
            throw new Exception( get_string( 'notavailable' ) );
        }
        $subrecord = $vpl->last_user_submission($USER->id);
        if ($subrecord) {
            $submission = new mod_vpl_submission($vpl, $subrecord);
            $compilationexecution = $submission->get_CE_for_editor();
            return [
                    'compilation' => $compilationexecution->compilation,
                    'evaluation' => $compilationexecution->evaluation,
                    'grade' => $compilationexecution->grade,
                    'timegraded' => $subrecord->dategraded,
                    'timesubmitted' => $subrecord->datesubmitted,
            ];
        } else {
            return [
                    'compilation' => null,
                    'evaluation' => null,
                    'grade' => null,
                    'timegraded' => null,
                    'timesubmitted' => null,
            ];
        }
    }
    public static function get_last_evaluation_returns() {
        return new external_single_structure( [
                'compilation' => new external_value( PARAM_RAW, 'Compilation result' ),
                'evaluation' => new external_value( PARAM_RAW, 'Evaluation result' ),
                'grade' => new external_value( PARAM_RAW, 'Proposed or final grade' ),
                'timegraded' => new external_value( PARAM_INT, 'Timestamp of evaluation' ),
                'timesubmitted' => new external_value( PARAM_INT, 'Timestamp of submission' ),
        ] );
    }

    /*
     * subrestrictions function. Get the information about the submissions restrictions for the user.
     */
    public static function subrestrictions_parameters() {
        return new external_function_parameters( [
                'id' => static::cmid_parameter(),
                'password' => static::password_parameter(),
        ] );
    }
    public static function subrestrictions( $id, $password = '' ) {
        global $USER;
        list($id, $password) = array_values(self::validate_parameters( self::subrestrictions_parameters(), [
                'id' => $id,
                'password' => $password,
        ] ));
        self::validate_context(context_module::instance($id));

        $vpl = self::initial_checks( $id, $password );
        $vpl->require_capability( VPL_VIEW_CAPABILITY );

        $duedate = $vpl->get_effective_setting('duedate', $USER->id);
        $timeleft = $duedate ? $duedate - time() : null;

        $subrecord = $vpl->last_user_submission($USER->id);
        $nevaluations = $subrecord ? (new mod_vpl_submission($vpl, $subrecord))->get_instance()->nevaluations : 0;
        $reductionbyevaluation = $vpl->get_effective_setting('reductionbyevaluation', $USER->id);
        $freeevaluations = $reductionbyevaluation > 0 ? $vpl->get_effective_setting('freeevaluations', $USER->id) : null;
        $minevaluationdelay = $vpl->get_effective_setting('minevaluationdelay', $USER->id);

        return [
                'timeleft' => $timeleft,
                'freeevaluations' => $freeevaluations ?: null,
                'nevaluations' => $nevaluations,
                'reductionbyevaluation' => $reductionbyevaluation ?: null,
                'minevaluationdelay' => $minevaluationdelay,
        ];
    }
    public static function subrestrictions_returns() {
        return new external_single_structure( [
                'timeleft' => new external_value( PARAM_INT,
                        'Number of seconds remaining, null if no limit', VALUE_REQUIRED, null, NULL_ALLOWED ),
                'freeevaluations' => new external_value( PARAM_INT,
                        'Number of free evaluations before reduction, null if no reduction', VALUE_REQUIRED, null, NULL_ALLOWED ),
                'nevaluations' => new external_value( PARAM_INT,
                        'Number of evaluations already requested' ),
                'reductionbyevaluation' => new external_value( PARAM_RAW,
                        'Reduction by automatic evaluation, null if no reduction', VALUE_REQUIRED, null, NULL_ALLOWED ),
                'minevaluationdelay' => new external_value( PARAM_INT,
                        'Minimal delay in seconds between two consecutive automatic evaluations', VALUE_REQUIRED, null, NULL_ALLOWED ),
        ] );
    }

    /*
     * set_setting function. Change a setting of the VPL activity.
     */
    public static function set_setting_parameters() {
        return new external_function_parameters( [
                'id' => static::cmid_parameter(),
                'settingname' => new external_value( PARAM_RAW, 'Setting name to set', VALUE_REQUIRED ),
                'settingvalue' => new external_value( PARAM_RAW, 'Setting new value', VALUE_REQUIRED ),
                'password' => static::password_parameter(),
        ] );
    }
    public static function set_setting($id, $settingname, $settingvalue, $password) {
        list($id, $settingname, $settingvalue, $password) = array_values(self::validate_parameters( self::set_setting_parameters(), [
                'id' => $id,
                'settingname' => $settingname,
                'settingvalue' => $settingvalue,
                'password' => $password,
        ] ));
        self::validate_context(context_module::instance($id));

        $vpl = self::initial_checks( $id, $password );
        $vpl->require_capability( VPL_MANAGE_VIA_WS_CAPABILITY );

        self::validate_setting($vpl, $settingname, $settingvalue);

        $instance = $vpl->get_instance();
        $protectedfields = [ 'id', 'course' ];
        if (property_exists($instance, $settingname) && !in_array($settingname, $protectedfields)) {
            $instance->$settingname = $settingvalue;
            $success = $vpl->update();
        } else {
            throw new moodle_exception('nosuchsetting', VPL);
        }
        return [ 'success' => $success ];
    }
    private static function validate_setting($vpl, $settingname, $settingvalue) {
        if (isset(mod_vpl_mod_form::$fieldsformat[$settingname])) {
            $pattern = mod_vpl_mod_form::$fieldsformat[$settingname];
            if (!preg_match($pattern, $settingvalue)) {
                throw new moodle_exception('invalidsettingformat', VPL, null, mod_vpl_mod_form::$formatmessage[$pattern]);
            }
        }
        if ($settingname === 'basedon') {
            // Check that it it an available base.
            if (!isset(vpl_get_available_bases($vpl, false)[$settingvalue])) {
                throw new moodle_exception('invalidbasedon', VPL);
            }
        }
    }
    public static function set_setting_returns() {
        return new external_single_structure( [
                'success' => new external_value( PARAM_RAW ),
        ] );
    }

    /*
     * get_setting function. Get a setting of the VPL activity.
     */
    public static function get_setting_parameters() {
        return new external_function_parameters( [
                'id' => static::cmid_parameter(),
                'settingname' => new external_value( PARAM_RAW, 'Setting name to set', VALUE_REQUIRED ),
                'password' => static::password_parameter(),
        ] );
    }
    public static function get_setting($id, $settingname, $password) {
        list($id, $settingname, $password) = array_values(self::validate_parameters( self::get_setting_parameters(), [
                'id' => $id,
                'settingname' => $settingname,
                'password' => $password,
        ] ));
        self::validate_context(context_module::instance($id));

        $vpl = self::initial_checks( $id, $password );
        $vpl->require_capability( VPL_MANAGE_VIA_WS_CAPABILITY );
        $instance = $vpl->get_instance();
        if (property_exists($instance, $settingname)) {
             $value = $instance->$settingname;
        } else {
            throw new moodle_exception('nosuchsetting', VPL);
        }
        return [ 'value' => $value ];
    }
    public static function get_setting_returns() {
        return new external_single_structure( [
                'value' => new external_value( PARAM_RAW ),
        ] );
    }

    public static function get_files_to_keep_when_running_parameters() {
        return new external_function_parameters( [
                'id' => static::cmid_parameter(),
                'password' => static::password_parameter(),
        ] );
    }
    public static function get_files_to_keep_when_running( $id, $password = '' ) {
        list($id, $password) = array_values(self::validate_parameters( self::get_files_to_keep_when_running_parameters(), [
                'id' => $id,
                'password' => $password,
        ] ));
        self::validate_context(context_module::instance($id));

        $vpl = self::initial_checks( $id, $password );
        $vpl->require_capability( VPL_MANAGE_VIA_WS_CAPABILITY );

        return $vpl->get_fgm('execution')->getfilekeeplist();
    }
    public static function get_files_to_keep_when_running_returns() {
        return new external_multiple_structure( new external_value( PARAM_RAW, 'File name' ) );
    }

    public static function set_files_to_keep_when_running_parameters() {
        return new external_function_parameters( [
                'id' => static::cmid_parameter(),
                'files' => new external_multiple_structure( new external_value( PARAM_PATH, 'File name' ), '', VALUE_DEFAULT, [] ),
                'password' => static::password_parameter(),
        ] );
    }
    public static function set_files_to_keep_when_running( $id, $files = [], $password = '' ) {
        list($id, $files, $password) = array_values(self::validate_parameters( self::set_files_to_keep_when_running_parameters(), [
                'id' => $id,
                'files' => $files,
                'password' => $password,
        ] ));
        self::validate_context(context_module::instance($id));

        $vpl = self::initial_checks( $id, $password );
        $vpl->require_capability( VPL_MANAGE_VIA_WS_CAPABILITY );

        $vpl->get_fgm('execution')->setfilekeeplist($files);
    }
    public static function set_files_to_keep_when_running_returns() {
        return null;
    }
}
