<?php
// This file is part of VPL for Moodle - http://vpl.dis.ulpgc.es/
//
// VPL for Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// VPL for Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with VPL for Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Process ajax files edit
 *
 * @package mod_vpl
 * @copyright 2012 Juan Carlos Rodríguez-del-Pino
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author Juan Carlos Rodríguez-del-Pino <jcrodriguez@dis.ulpgc.es>
 */

define( 'AJAX_SCRIPT', true );

require(__DIR__ . '/../../../config.php');

global $USER;
$result = new stdClass();
$result->success = true;
$result->response = new stdClass();
$result->error = '';
try {
    require_once(dirname( __FILE__ ) . '/../locallib.php');
    require_once(dirname( __FILE__ ) . '/../vpl.class.php');
    require_once(dirname( __FILE__ ) . '/edit.class.php');

    if (! isloggedin()) {
        throw new Exception( get_string( 'loggedinnot' ) );
    }

    $id = required_param( 'id', PARAM_INT ); // Course id.
    $type = required_param( 'type', PARAM_ALPHANUMEXT );
    $action = required_param( 'action', PARAM_ALPHANUMEXT );
    $vpl = new mod_vpl( $id );

    // TODO use or not sesskey "require_sesskey();".
    require_login( $vpl->get_course(), false );
    $vpl->require_capability(VPL_MANAGE_CAPABILITY);

    raise_memory_limit(MEMORY_EXTRA);
    $actiondata = json_decode( file_get_contents( 'php://input' ), null, 512, JSON_INVALID_UTF8_SUBSTITUTE );
    if ($type == 'testcases') {
        $action .= 'cases';
    }
    switch ($action) {
        case 'save':
            $postfiles = mod_vpl_edit::filesfromide($actiondata->files);
            $result->response->requestsconfirmation = false;
            $fgm = $vpl->get_fgm($type);
            $oldversion = $fgm->getversion();
            if ($actiondata->version != 0 && $actiondata->version != $oldversion) {
                $result->response->question = get_string('replacenewer', VPL);
                $result->response->requestsconfirmation = true;
                $result->response->version = $oldversion;
                break;
            }
            $fgm->deleteallfiles();
            $fgm->addallfiles($postfiles);
            $result->response->version = $fgm->getversion();
            break;
        case 'load':
            $fgm = $vpl->get_fgm($type);
            $result->response->version = $fgm->getversion();
            if ($type == 'corrected' && empty($fgm->getfilelist())) {
                // By default, load required files into corrected files.
                $fgm = $vpl->get_fgm('required');
            }
            if ($type === 'execution') {
                $result->response->files = mod_vpl_edit::filestoide( $fgm->getallfiles(), $fgm->getfilekeeplist() );
            } else {
                $result->response->files = mod_vpl_edit::filestoide( $fgm->getallfiles() );
            }
            break;
        case 'resetfiles':
            $fgm = $vpl->get_fgm('required');
            $result->response->files = mod_vpl_edit::filestoide( $fgm->getallfiles() );
            break;
        case 'run':
        case 'debug':
        case 'evaluate':
            if ($action == 'evaluate' && $type == 'execution') {
                $action = 'test_evaluate';
            }
            $result->response = mod_vpl_edit::execute( $vpl, $USER->id, $action, $actiondata );
            break;
        case 'retrieve':
            $result->response = mod_vpl_edit::retrieve_result( $vpl, $USER->id );
            break;
        case 'savecases':
            $filename = 'vpl_evaluate.cases';
            $postfiles = mod_vpl_edit::filesfromide($actiondata->files);
            if (count( $postfiles ) != 1 || ! isset( $postfiles[$filename] )) {
                throw new Exception( get_string( 'incorrect_file_name', VPL ) );
            }
            $fgm = $vpl->get_fgm('execution');
            $result->response->requestsconfirmation = false;
            $oldversion = $fgm->getversion();
            if ($actiondata->version != 0 && $actiondata->version != $oldversion) {
                $result->response->question = get_string('replacenewer', VPL);
                $result->response->requestsconfirmation = true;
                $result->response->version = $oldversion;
                break;
            }
            $fgm->addFile( $filename, $postfiles[$filename] );
            $result->response->version = $fgm->getversion();
            $vpl->update();
            break;
        case 'loadcases':
            $filename = 'vpl_evaluate.cases';
            $fgm = $vpl->get_fgm('execution');
            $files = [];
            $files[$filename] = $fgm->getfiledata($filename);
            $result->response->files = mod_vpl_edit::filestoide($files);
            $result->response->version = $fgm->getversion();
            break;
        default :
            throw new Exception( 'ajax action error: ' + $action );
    }
} catch (\Throwable $e ) {
    $result->success = false;
    $result->error = $e->getMessage();
}
echo json_encode( $result );
die();
