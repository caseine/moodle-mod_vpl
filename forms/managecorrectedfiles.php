<?php
// This file is part of VPL for Moodle - http://vpl.dis.ulpgc.es/
//
// VPL for Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// VPL for Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with VPL for Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Corrected files management page.
 * @package mod_vpl
 * @copyright 2024 Astor Bizard
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname( __FILE__ ) . '/../../../config.php');
require_once(dirname( __FILE__ ) . '/../vpl.class.php');

require_login();

global $PAGE, $OUTPUT;

$id = required_param('id', PARAM_INT);
$vpl = new mod_vpl($id);
$PAGE->navbar->add(get_string('correctedfilesmanagement', 'mod_vpl'));
$vpl->prepare_page('forms/managecorrectedfiles.php', [ 'id' => $id ]);
$vpl->require_capability(VPL_MANAGE_CAPABILITY);
$PAGE->force_settings_menu();

$returnurl = new moodle_url(optional_param('returnurl', '/mod/vpl/forms/files.php?id=' . $id .'&type=corrected', PARAM_RAW));
$formaction = $PAGE->url;
$formaction->param('returnurl', $returnurl);

$form = new mod_vpl\correctedfiles\manage_form($vpl, $formaction);
if ($fromform = $form->get_data()) {
    require_sesskey();
    $vpl->get_instance()->correctedfilesavailability = $fromform->availability;
    $vpl->get_instance()->correctedfilesforexecution = $fromform->forexecution;
    $vpl->update();
    redirect($returnurl, get_string('optionssaved', 'mod_vpl'), null, \core\output\notification::NOTIFY_SUCCESS);
} else if ($form->is_cancelled()) {
    redirect($returnurl);
}
$vpl->print_header(get_string('correctedfilesmanagement', 'mod_vpl'));
echo $OUTPUT->heading(get_string('correctedfilesmanagement', 'mod_vpl'));
$form->set_data([
        'availability' => $vpl->get_instance()->correctedfilesavailability,
        'forexecution' => $vpl->get_instance()->correctedfilesforexecution,
]);
$form->display();
$vpl->print_footer();
