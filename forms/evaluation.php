<?php
// This file is part of VPL for Moodle - http://vpl.dis.ulpgc.es/
//
// VPL for Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// VPL for Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with VPL for Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Atomatic evaluation from link
 * @package mod_vpl
 * @copyright 2012 Juan Carlos Rodríguez-del-Pino
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author Juan Carlos Rodríguez-del-Pino <jcrodriguez@dis.ulpgc.es>
 */

require_once(dirname(__FILE__).'/../../../config.php');
require_once(dirname(__FILE__).'/../locallib.php');
require_once(dirname(__FILE__).'/../vpl.class.php');
require_once(dirname(__FILE__).'/../vpl_submission_CE.class.php');
require_once(dirname(__FILE__).'/../editor/editor_utility.php');

global $USER, $DB, $OUTPUT;

require_login();

global $USER, $DB, $OUTPUT;
$id = required_param( 'id', PARAM_INT );
$userid = optional_param( 'userid', $USER->id, PARAM_INT );
$userlist = optional_param( 'userlist', null, PARAM_RAW );
$nexturl = optional_param( 'returnurl', "../forms/submissionview.php?id={$id}&userid={$userid}", PARAM_RAW );
$vpl = new mod_vpl( $id );
$parms = [ 'id' => $id ];
if ($userid) {
    $parms['userid'] = $userid;
}
$vpl->prepare_page( 'forms/evaluation.php',  $parms);
if ($userid == $USER->id && $vpl->get_instance()->evaluate) { // Evaluate own submission.
    $vpl->require_capability( VPL_SUBMIT_CAPABILITY );
    $vpl->restrictions_check();
} else { // Evaluate other user submission.
    $vpl->require_capability( VPL_GRADE_CAPABILITY );
}

vpl_editor_util::generate_requires_evaluation();
// Display page.
$vpl->print_header( get_string( 'evaluation', VPL ) );
flush();

echo '<h2>' . s( get_string( 'evaluating', VPL ) ) . '</h2>';
$text = '';
if ($userlist !== null) {
    $users = explode(',', $userlist);
    $index = array_search($userid, $users);
    if ($index !== false) {
        $text .= ($index + 1) . '/' . count($users) . ' ';
        $next = $index + 1;
        if ($next < count($users)) {
            $escapedurl = urlencode($nexturl);
            $nexturl = "../forms/evaluation.php?id={$id}&userid={$users[$next]}&userlist={$userlist}&returnurl={$escapedurl}";
        }
    }
}
$user = $DB->get_record( 'user', [ 'id' => $userid ] );
$text .= $vpl->user_fullname_picture( $user, false );
echo $OUTPUT->box( $text );
$ajaxurl = "edit.json.php?id={$id}&userid={$userid}&action=";
vpl_editor_util::generate_evaluate_script( $ajaxurl, $nexturl );
$vpl->print_footer();
