<?php
// This file is part of VPL for Moodle - http://vpl.dis.ulpgc.es/
//
// VPL for Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// VPL for Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with VPL for Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Edit teacher files
 *
 * @package mod_vpl
 * @copyright    2012 Juan Carlos Rodríguez-del-Pino, 2020 Astor Bizard
 * @license        http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author        Juan Carlos Rodríguez-del-Pino <jcrodriguez@dis.ulpgc.es>
 */

require_once(dirname(__FILE__). '/../../../config.php');
require_once(dirname(__FILE__). '/../locallib.php');
require_once(dirname(__FILE__). '/../vpl.class.php');
require_once(dirname(__FILE__). '/../editor/editor_utility.php');

require_login();
$id = required_param( 'id', PARAM_INT );
$type = required_param( 'type', PARAM_ALPHANUMEXT );

$vpl = new mod_vpl( $id );
$vpl->prepare_page( 'forms/files.php', [ 'id' => $id, 'type' => $type ] );

$vpl->require_capability( VPL_MANAGE_CAPABILITY );

global $PAGE;
$PAGE->force_settings_menu();

$options = [];
$options['restrictededitor'] = false;
$options['save'] = true;
$options['run'] = ($type == 'execution');
$options['debug'] = ($type == 'execution');
$options['evaluate'] = ($type == 'execution');
$options['ajaxurl'] = "files.json.php?id={$id}&type={$type}&action=";
$options['download'] = "../views/downloadfiles.php?id={$id}&type={$type}";
$options['resetfiles'] = ($type == 'corrected');
$options['correctedfiles'] = false;
$options['showparentfiles'] = ($type == 'execution');
$options['showparentfilesurl'] = (new moodle_url('/mod/vpl/views/concatexecfiles.php?id=' . $id))->out();
$options['saved'] = ($type != 'corrected' || !empty($vpl->get_files($type)));
$options['readOnlyFiles'] = [];

switch ($type) {
    case 'required':
    case 'corrected':
        $options['minfiles'] = 0;
        $options['maxfiles'] = $vpl->get_instance()->maxfiles;
        break;
    case 'execution':
        $options['minfiles'] = $vpl->get_fgm('execution')->get_numstaticfiles();
        $options['maxfiles'] = 1000;
        break;
    case 'testcases':
        $options['minfiles'] = 1;
        $options['maxfiles'] = 1;
        break;
    default:
        throw new moodle_exception('unknownfiletype', VPL, $vpl->get_view_url(), $type);
}

vpl_editor_util::generate_requires($options);
$vpl->print_header( get_string( $type.'files', VPL ) );
$vpl->print_heading_with_help( $type.'files' );
if ($type === 'corrected') {
    mod_vpl\correctedfiles\availability::print_settings($vpl);
    mod_vpl\correctedfiles\executionmanager::print_settings($vpl);
    echo html_writer::link(new moodle_url('/mod/vpl/forms/managecorrectedfiles.php', [ 'id' => $id, 'returnurl' => $PAGE->url ]),
            get_string('manage', VPL), [ 'class' => 'btn btn-secondary mb-2 mt-1' ]);
}
vpl_editor_util::print_editor();
vpl_editor_util::print_js_description($vpl);
$vpl->print_footer();
