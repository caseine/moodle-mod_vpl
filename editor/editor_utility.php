<?php
// This file is part of VPL for Moodle - http://vpl.dis.ulpgc.es/
//
// VPL for Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// VPL for Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with VPL for Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * IDE utility functions
 *
 * @package mod_vpl
 * @copyright 2012 Juan Carlos Rodríguez-del-Pino
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author Juan Carlos Rodríguez-del-Pino <jcrodriguez@dis.ulpgc.es>
 */

class vpl_editor_util {
    private static $jquerynoload = true;
    public static function generate_jquery() {
        global $PAGE;
        if (self::$jquerynoload) {
            $PAGE->requires->jquery();
            $PAGE->requires->jquery_plugin('ui');
            $PAGE->requires->jquery_plugin('ui-css');
            self::$jquerynoload = false;
        }
    }
    public static function generate_requires_evaluation() {
        self::generate_jquery();
    }
    public static function generate_requires($options, $vpl = null) {
        global $PAGE, $CFG;
        $currenteditor = static::get_current_editor($vpl);
        $plugincfg = get_config('mod_vpl');
        if ( isset($plugincfg->{'editor_' . $currenteditor . '_theme'}) ) {
            $defaulttheme = $plugincfg->{'editor_' . $currenteditor . '_theme'};
        } else {
            $defaulttheme = static::get_default_editor_themes()[$currenteditor];
        }
        $options['fontSize'] = get_user_preferences('vpl_editor_fontsize', 12);
        $options['currentEditor'] = $currenteditor;
        $options['theme'] = get_user_preferences('vpl_' . $currenteditor . 'theme', $defaulttheme);
        $options['lang'] = $CFG->lang;
        $options['postMaxSize'] = \mod_vpl\util\phpconfig::get_post_max_size();
        $options['isGroupActivity'] = $vpl !== null && $vpl->is_group_activity();
        $options['isTeacher'] = $vpl !== null
                && ($vpl->has_capability(VPL_GRADE_CAPABILITY) || $vpl->has_capability(VPL_MANAGE_CAPABILITY));
        $options['str'] = self::strings_for_js();
        self::generate_jquery();

        require_once($CFG->dirroot . '/mod/vpl/views/sh_factory.class.php');
        vpl_sh_factory::include_jsnotebook_highlighter();

        if ($currenteditor == 'monaco') {
            $config = ['paths' => ['vs' => '/mod/vpl/editor/monaco/min/vs']];
            $requirejs = 'require.config(' . json_encode($config) . ');';
            $lang = current_language();
            if (file_exists($CFG->dirroot . '/mod/vpl/editor/monaco/min/vs/editor/editor.main.nls.' . $lang . '.js')) {
                $requirejs .= 'require.config({ "vs/nls": { availableLanguages: { "*": "' . $lang . '" } } });';
            }
            $PAGE->requires->js_amd_inline($requirejs);

            $languageserversraw = explode("\n", $plugincfg->language_servers);
            $languageservers = [];
            foreach ($languageserversraw as $languageserver) {
                $languageserver = trim($languageserver);
                if (substr($languageserver, 0, 1) == '#') {
                    continue;
                }
                $servermapping = explode(' ', $languageserver);
                if (count($servermapping) != 2) {
                    continue;
                }
                list($language, $server) = $servermapping;
                $languageservers[$language] = $server;
            }
            if (!empty($languageservers)) {
                $PAGE->requires->js( new moodle_url( '/mod/vpl/editor/monaco/language-client/normalize-url/normalizeurl.min.js' ), true );
                $PAGE->requires->js( new moodle_url( '/mod/vpl/editor/monaco/language-client/reconnecting-websocket/reconnecting-websocket.min.js' ), true );
                $PAGE->requires->js( new moodle_url( '/mod/vpl/editor/monaco/language-client/monaco-jsonrpc-languageclient.min.js' ), true );
            }
            $options['languageservers'] = $languageservers;
        }

        $opt = new stdClass();
        $opt->scriptPath = $CFG->wwwroot . '/mod/vpl/editor';
        $PAGE->requires->js_call_amd('mod_vpl/vplutil', 'init', [ $opt ]);
        $PAGE->requires->js_call_amd('mod_vpl/vplide', 'init', [ 'vplide', $options ]);

        $PAGE->requires->js_init_call('M.util.init_colour_picker', [ 'interfacetheme-colorpicker-primary' ]);
        $PAGE->requires->js_init_call('M.util.init_colour_picker', [ 'interfacetheme-colorpicker-secondary' ]);
    }
    public static function print_js_description($vpl, $userid = 0) {
        $html = $vpl->get_variation_html($userid);
        $html .= $vpl->get_fulldescription_with_basedon();
        ?>
        <script>
        window.VPLDescription = <?php echo json_encode($html);?>;
        </script>
        <?php
    }

    public static function print_editor($vpl = null) {
        global $OUTPUT;
        $context = new stdClass();
        $context->currenttheme = get_user_preferences('vpl_interfacetheme', 'modernblue');
        $context->currentprimarycolor = get_user_preferences('vpl_custom_interface_theme_color_primary', '#000000');
        $context->currentsecondarycolor = get_user_preferences('vpl_custom_interface_theme_color_secondary', '#FFFFFF');

        $editors = static::get_installed_editors();
        $currenteditor = static::get_current_editor($vpl);

        if ($vpl !== null) {
            $forbiddeneditors = explode(',', trim($vpl->get_instance()->forbiddeneditors));
        } else {
            $forbiddeneditors = [];
        }

        $context->currenteditorname = $editors[$currenteditor];
        $context->availableeditors = [];
        foreach ($editors as $id => $name) {
            if ($id == $currenteditor) {
                continue;
            }
            $editor = new stdClass();
            $editor->id = $id;
            $editor->name = $name;
            if (in_array($id, $forbiddeneditors)) {
                $editor->unavailable = true;
            }
            $context->availableeditors[] = $editor;
        }

        $context->installedthemes = [];
        foreach (self::get_installed_themes($currenteditor) as $id => $name) {
            $theme = new stdClass();
            $theme->id = $id;
            $theme->name = $name;
            $context->installedthemes[] = $theme;
        }
        echo $OUTPUT->render_from_template('mod_vpl/editor', $context);
    }

    public static function strings_for_js() {
        global $PAGE;
        $PAGE->requires->strings_for_js(
                [
                        'about',
                        'acceptcertificates',
                        'acceptcertificatesnote',
                        'acetheme',
                        'binaryfile',
                        'browserupdate',
                        'changesNotSaved',
                        'clipboard',
                        'comments',
                        'compilation',
                        'connected',
                        'connecting',
                        'connection_closed',
                        'connection_fail',
                        'console',
                        'copy',
                        'correctedfiles',
                        'create_new_file',
                        'cut',
                        'description',
                        'debug',
                        'debugging',
                        'delete',
                        'delete_file_fq',
                        'delete_file_q',
                        'directory_not_renamed',
                        'donotshowagain',
                        'download',
                        'edit',
                        'evaluate',
                        'evaluating',
                        'execution',
                        'getjails',
                        'file',
                        'filelist',
                        'filenotadded',
                        'filenotdeleted',
                        'filenotrenamed',
                        'find',
                        'find_replace',
                        'fullscreen',
                        'incorrect_directory_name',
                        'incorrect_file_name',
                        'interfacetheme',
                        'keptforexecution',
                        'keyboard',
                        'maxfilesexceeded',
                        'morerecentversionloadprompt',
                        'morerecentversionsaved',
                        'monaconotloadingnotice',
                        'new',
                        'next',
                        'load',
                        'loading',
                        'options',
                        'outofmemory',
                        'paste',
                        'print',
                        'readonly',
                        'redo',
                        'regularscreen',
                        'rename',
                        'rename_directory',
                        'rename_file',
                        'resetfiles',
                        'retrieve',
                        'run',
                        'running',
                        'save',
                        'saving',
                        'select_all',
                        'shortcuts',
                        'starting',
                        'sureresetfiles',
                        'surecorrectedfiles',
                        'timeleft',
                        'timeout',
                        'undo',
                        'multidelete',
                        'basic',
                        'intermediate',
                        'advanced',
                        'variables',
                        'operatorsvalues',
                        'control',
                        'inputoutput',
                        'functions',
                        'lists',
                        'math',
                        'text',
                        'shrightpanel',
                        'start',
                        'startanimate',
                        'stop',
                        'pause',
                        'resume',
                        'step',
                        'badjsonnotebook',
                        'jsnotebooknotice',
                        'showparentfiles',
                ], 'mod_vpl');
        $PAGE->requires->strings_for_js(
                [
                        'cancel',
                        'closebuttontitle',
                        'error',
                        'import',
                        'modified',
                        'no',
                        'notice',
                        'ok',
                        'required',
                        'sort',
                        'warning',
                        'yes',
                        'deleteselected',
                        'selectall',
                        'deselectall',
                        'reset',
                        'theme',
                ], 'moodle');
        return [
                'close' => get_string( 'closebuttontitle' ),
                'more' => get_string( 'showmore', 'form' ),
                'less' => get_string( 'showless', 'form' ),
                'fontsize' => get_string( 'fontsize', 'editor' ),
        ];
    }

    public static function generate_evaluate_script($ajaxurl, $nexturl) {
        global $PAGE;
        $options = [];
        $options['str'] = self::strings_for_js();
        $options['ajaxurl'] = $ajaxurl;
        $options['nexturl'] = $nexturl;
        $PAGE->requires->js_call_amd('mod_vpl/evaluationmonitor', 'init', [ $options ] );
    }

    public static function get_current_editor($vpl = null) {
        $plugincfg = get_config('mod_vpl');
        if ( isset($plugincfg->default_editor) ) {
            $defaulteditor = $plugincfg->default_editor;
        } else {
            $defaulteditor = 'ace';
        }
        $preferrededitor = get_user_preferences('vpl_editor', $defaulteditor);
        if ($vpl !== null) {
            $forbiddeneditors = explode(',', trim($vpl->get_instance()->forbiddeneditors));
            if (in_array($preferrededitor, $forbiddeneditors)) {
                if (in_array($defaulteditor, $forbiddeneditors)) {
                    // Both preferred and default editor are forbidden.
                    // Find the first allowed editor.
                    foreach (static::get_installed_editors() as $editor => $name) {
                        if (!in_array($editor, $forbiddeneditors)) {
                            return $editor;
                        }
                    }
                }
                return $defaulteditor;
            }
        }
        return $preferrededitor;
    }

    public static function get_installed_editors() {
        return [
                'ace' => 'Ace',
                'monaco' => 'Monaco',
        ];
    }

    public static function get_installed_themes($editor = 'ace') {
        global $CFG;
        $themes = [];
        if ($editor == 'ace') {
            // Search for theme files.
            $acefiles = array_diff(scandir($CFG->dirroot . '/mod/vpl/editor/ace9/'), [ '.', '..' ]);
            $themefiles = array_filter($acefiles, function($name) {
                return substr($name, 0, 6) == 'theme-';
            });
            // Process theme files names to get displayable name,
            // by replacing underscores by spaces and
            // by putting upper case letters at the beginning of words.
            foreach ($themefiles as $themefile) {
                $theme = substr($themefile, 6, strlen($themefile) - 9);
                $themename = preg_replace_callback('/(^|_)([a-z])/', function($matches) {
                    return ' ' . strtoupper($matches[2]);
                }, $theme);
                $themes[$theme] = trim($themename);
            }
            // Some exceptions.
            $themes['github'] = 'GitHub';
            $themes['idle_fingers'] = 'idle Fingers';
            $themes['iplastic'] = 'IPlastic';
            $themes['katzenmilch'] = 'KatzenMilch';
            $themes['kr_theme'] = 'krTheme';
            $themes['kr'] = 'kr';
            $themes['pastel_on_dark'] = 'Pastel on dark';
            $themes['sqlserver'] = 'SQL Server';
            $themes['textmate'] = 'TextMate';
            $themes['tomorrow_night_eighties'] = 'Tomorrow Night 80s';
            $themes['xcode'] = 'XCode';
        } else if ($editor == 'monaco') {
            $file = $CFG->dirroot . '/mod/vpl/editor/monaco/themes/themelist.json';
            if (file_exists($file)) {
                $themes = json_decode(file_get_contents($file), true);
            }
            $themes['vs'] = 'VS';
            $themes['vs-dark'] = 'VS Dark';
            $themes['hc-black'] = 'High Contrast (Dark)';
            ksort($themes);
        }

        return $themes;
    }

    public static function get_default_editor_themes() {
        return [
                'ace' => 'chrome',
                'monaco' => 'vs',
        ];
    }
}
