@     whitespace: [
@       [/[ \t\r\n]+/, ""],
@       [/\/\*\*(?!\/)/, "comment.doc", "@jsdoc"],
@       [/\/\*/, "comment", "@comment"],
        [/\/\/\/.*$/, "comment.teacher"],
@       [/\/\/.*$/, "comment"]
@     ],