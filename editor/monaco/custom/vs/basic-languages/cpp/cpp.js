@     whitespace: [
@       [/[ \t\r\n]+/, ""],
@       [/\/\*\*(?!\/)/, "comment.doc", "@doccomment"],
@       [/\/\*/, "comment", "@comment"],
        [/\/\/\/.*\\$/, "comment.teacher", "@linecommentteacher"],
        [/\/\/\/.*$/, "comment.teacher"],
@       [/\/\/.*\\$/, "comment", "@linecomment"],
@       [/\/\/.*$/, "comment"],
@     ],
@     comment: [
@       ...
@     ],
      linecomment: [
        [/(^|.*[^\\])$/, "comment", "@pop"],
        [/[^]+/, "comment"]
      ],
      linecommentteacher: [
        [/(^|.*[^\\])$/, "comment.teacher", "@pop"],
        [/[^]+/, "comment.teacher"]
      ],
@     doccomment: [
@       ...