@     whitespace: [
@       [/[ \t\r\n]+/, ""],
@       [/\/\*\*(?!\/)/, "comment.doc", "@javadoc"],
@       [/\/\*/, "comment", "@comment"],
        [/\/\/\/.*$/, "comment.teacher"],
@       [/\/\/.*$/, "comment"]
@     ],