@   tokenizer: {
@     root: [
@       ...
@       [/#'$/, "comment.doc"],
@       [/#'/, "comment.doc", "@roxygen"],
        [/(^###.*$)/, "comment.teacher"],
@       [/(^#.*$)/, "comment"],
@       [/\s+/, "white"],
@       [/[,:;]/, "delimiter"],
@       ...