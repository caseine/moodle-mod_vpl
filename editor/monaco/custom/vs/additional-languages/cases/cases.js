/*!-----------------------------------------------------------------------------
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Version: 0.31.1(337587859b1c171314b40503171188b6cea6a32a)
 * Released under the MIT license
 * https://github.com/microsoft/monaco-editor/blob/main/LICENSE.txt
 *-----------------------------------------------------------------------------*/
define("vs/additional-languages/cases/cases",[],()=>{
var moduleExports = (() => {
  var __defProp = Object.defineProperty;
  var __markAsModule = (target) => __defProp(target, "__esModule", { value: true });
  var __export = (target, all) => {
    __markAsModule(target);
    for (var name in all)
      __defProp(target, name, { get: all[name], enumerable: true });
  };

  var cases_exports = {};
  __export(cases_exports, {
    conf: () => conf,
    language: () => language
  });
  var conf = {
    autoClosingPairs: [
      { open: '"', close: '"', notIn: ["string"] }
    ]
  };
  
  var language = {
    defaultToken: "",
    tokenPostfix: ".cases",
    keywords: [
      "True",
      "False"
    ],
    directives: [
      "Case",
      "output",
      "input",
      "Grade reduction",
      "Fail message",
      "Program to run",
      "Program arguments",
      "Expected exit code",
      "Variation",
      "Class",
      "Classe",
      "Method",
      "JunitFiles",
      "ValsInCons",
      "ValsIn",
      "ValsOut",
      "Grade",
      "ShowInput",
      "MessageErr",
      "MethodGet",
      "TimeLimitInSec",
      "Complexity",
      "TimeLimitAnalyserInSec",
      "RangeMin",
      "RangeMax",
      "Sorted",
      "LocationArg",
      "Increment",
      "ComplexityType",
      "MethodCPUStudent",
      "InterpretedJava",
    ],
    escapes: /\\(?:[abfnrtv\\"']|x[0-9A-Fa-f]{1,4}|u[0-9A-Fa-f]{4}|U[0-9A-Fa-f]{8})/,
    number: /\d+(\.\d+)?/,
    tokenizer: {
      root: [
        [
          /^([a-zA-Z_][\w ]*\w) *(?==)/,
          {
            cases: {
              "$1==Case": { token: "keyword.$1", next: "@singlelinedirective" },
              "$1==Grade reduction": { token: "function.builtin.$1", next: "@reductiondirective" },
              "$1@directives": { token: "function.builtin.$1" },
              "@default": "identifier"
            }
          }
        ],
        [/[ \t\r\n]+/, ""],
        [/=/, "keyword"],
        [/[;,.]/, "delimiter"],
        [/"/, "string", "@literalstring"],
        [/(@number)/, "number"],
        [/\/([^\\\/]|\\.)+\/([dgimsuy]*)/, "regexp"],
        [
          /[a-zA-Z_]\w*/,
          {
            cases: {
              "@keywords": { token: "keyword.$0" },
              "@default": "identifier"
            }
          }
        ],
        [/./, "identifier"]
      ],
      singlelinedirective: [
        [/=/, "keyword"],
        [/[ \t\r\n]+$/, "", "@pop"],
        [/[ \t\r\n]+/, ""],
        [/.$/, "identifier", "@pop"],
        [/./, "identifier"]
      ],
      literalstring: [
        [/[^\\"]+/, "string"],
        [/@escapes/, "string.escape"],
        [/\\./, "string.escape.invalid"],
        [/"/, "string", "@pop"]
      ],
      reductiondirective: [
        [/(@number)%?$/, "number.hex", "@pop"],
        [/(@number)%?/, "number.hex"],
        { include: "@singlelinedirective" }
      ]
    }
  };
  return cases_exports;
})();
return moduleExports;
});
