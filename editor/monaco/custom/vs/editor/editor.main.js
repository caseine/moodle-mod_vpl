@   exports.hc_black = exports.vs_dark = exports.vs = void 0;
@   ...
@   exports.vs = {
@       ...
@       rules: [
            ...
            { token: 'operator', foreground: '222222' },
            { token: 'function.builtin', foreground: '0088FF' },
            { token: 'comment.teacher', foreground: 'FF0000', fontStyle: 'bold underline' }
            // Minified:
            {token:"operator",foreground:"222222"},{token:"function.builtin",foreground:"0088FF"},{token:"comment.teacher",foreground:"FF0000",fontStyle:"bold underline"}
@       ],
@       ...
@   };
@   exports.vs_dark = {
@       ...
@       rules: [
            ...
            { token: 'operator', foreground: '77AAEE' },
            { token: 'function.builtin', foreground: '99CCFF' },
            { token: 'comment.teacher', foreground: 'FF0000', fontStyle: 'bold underline' }
            // Minified:
            {token:"operator",foreground:"77AAEE"},{token:"function.builtin",foreground:"99CCFF"},{token:"comment.teacher",foreground:"FF0000",fontStyle:"bold underline"}
@       ],
@       ...
@   };
@   exports.hc_black = {
@       ...
@       rules: [
            ...
            { token: 'operator', foreground: '77AAEE' },
            { token: 'function.builtin', foreground: '99CCFF' },
            { token: 'comment.teacher', foreground: 'FF0000', fontStyle: 'bold underline' }
            // Minified:
            {token:"operator",foreground:"77AAEE"},{token:"function.builtin",foreground:"99CCFF"},{token:"comment.teacher",foreground:"FF0000",fontStyle:"bold underline"}
@       ],
@       ...
@   };


  registerLanguage({
    id: "opl",
    extensions: [".mod"],
    aliases: ["OPL"],
    loader: () => {
      if (true) {
        return new Promise((resolve, reject) => {
          __require(["vs/additional-languages/opl/opl"],
            function(languagedata) {
              resolve(languagedata);
              monaco_editor_core_exports.languages.registerCompletionItemProvider('opl', {
                provideCompletionItems: function (model, position) {
                  var word = model.getWordUntilPosition(position);
                  var range = {
                    startLineNumber: position.lineNumber,
                    endLineNumber: position.lineNumber,
                    startColumn: word.startColumn,
                    endColumn: word.endColumn
                  };
                  return {
                    suggestions: languagedata.language.functions.map(function(fun) {
                      return {
                        label: fun,
                        kind: monaco_editor_core_exports.languages.CompletionItemKind.Function,
                        insertText: fun + "()",
                        range: range
                      };
                    }).concat(languagedata.language.keywords.map(function(keyword) {
                      keyword = keyword == 'subject' ? 'subject to' : keyword;
                      keyword = keyword == 'not' ? 'not in' : keyword;
                      return {
                        label: keyword,
                        kind: monaco_editor_core_exports.languages.CompletionItemKind.Keyword,
                        insertText: keyword,
                        range: range
                      };
                    }))
                  };
                }
              });
            },
            reject
          );
        });
      } else {
        return null;
      }
    }
  });

  registerLanguage({
    id: "cases",
    extensions: [".cases"],
    aliases: ["Cases"],
    loader: () => {
      if (true) {
        return new Promise((resolve, reject) => {
          __require(["vs/additional-languages/cases/cases"],
            function(languagedata) {
              resolve(languagedata);
              monaco_editor_core_exports.languages.registerCompletionItemProvider('cases', {
                provideCompletionItems: function (model, position) {
                  var range = {
                    startLineNumber: position.lineNumber,
                    endLineNumber: position.lineNumber,
                    startColumn: 0,
                    endColumn: position.column
                  };
                  return {
                    suggestions: languagedata.language.directives.map(function(directive) {
                      return {
                        label: directive,
                        kind: monaco_editor_core_exports.languages.CompletionItemKind.Property,
                        insertText: directive + "=",
                        range: range
                      };
                    })
                  };
                }
              });
            },
            reject
          );
        });
      } else {
        return null;
      }
    }
  });

// Minified:
a({id:"opl",extensions:[".mod"],aliases:["OPL"],loader:()=>new Promise((r,e)=>{i(["vs/additional-languages/opl/opl"],function(languagedata){r(languagedata);n.languages.registerCompletionItemProvider("opl",{provideCompletionItems:function(model,position){var word=model.getWordUntilPosition(position);var range={startLineNumber:position.lineNumber,endLineNumber:position.lineNumber,startColumn:word.startColumn,endColumn:word.endColumn};return{suggestions:languagedata.language.functions.map(function(fun){return{label:fun,kind:n.languages.CompletionItemKind.Function,insertText:fun+"()",range:range};}).concat(languagedata.language.keywords.map(function(keyword){keyword=keyword=='subject'?'subject to':keyword;keyword=keyword=='not'?'not in':keyword;return{label:keyword,kind:n.languages.CompletionItemKind.Keyword,insertText:keyword,range:range};}))};}});},e)})});
a({id:"cases",extensions:[".cases"],aliases:["Cases"],loader:()=>new Promise((r,e)=>{i(["vs/additional-languages/cases/cases"],function(languagedata){r(languagedata);n.languages.registerCompletionItemProvider("cases",{provideCompletionItems:function(model,position){var range={startLineNumber:position.lineNumber,endLineNumber:position.lineNumber,startColumn:0,endColumn:position.column};return{suggestions:languagedata.language.directives.map(function(directive){return{label:directive,kind:n.languages.CompletionItemKind.Property,insertText:directive+"=",range:range};})};}});},e)})});
