/*!-----------------------------------------------------------------------------
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Version: 0.31.1(337587859b1c171314b40503171188b6cea6a32a)
 * Released under the MIT license
 * https://github.com/microsoft/monaco-editor/blob/main/LICENSE.txt
 *-----------------------------------------------------------------------------*/
define("vs/additional-languages/opl/opl",[],()=>{
var moduleExports = (() => {
  var __defProp = Object.defineProperty;
  var __markAsModule = (target) => __defProp(target, "__esModule", { value: true });
  var __export = (target, all) => {
    __markAsModule(target);
    for (var name in all)
      __defProp(target, name, { get: all[name], enumerable: true });
  };

  var opl_exports = {};
  __export(opl_exports, {
    conf: () => conf,
    language: () => language
  });
  var conf = {
    comments: {
      lineComment: "//",
      blockComment: ["/*", "*/"]
    },
    brackets: [
      ["{", "}"],
      ["[", "]"],
      ["(", ")"]
    ],
    autoClosingPairs: [
      { open: "[", close: "]" },
      { open: "{", close: "}" },
      { open: "(", close: ")" },
      { open: "'", close: "'", notIn: ["string", "comment"] },
      { open: '"', close: '"', notIn: ["string"] }
    ],
    surroundingPairs: [
      { open: "{", close: "}" },
      { open: "[", close: "]" },
      { open: "(", close: ")" },
      { open: '"', close: '"' },
      { open: "'", close: "'" }
    ]
  };
  
  var language = {
    defaultToken: "",
    tokenPostfix: ".opl",
    brackets: [
      { token: "delimiter.curly", open: "{", close: "}" },
      { token: "delimiter.parenthesis", open: "(", close: ")" },
      { token: "delimiter.square", open: "[", close: "]" }
    ],
    keywords: [
      "all",
      "and",
      "assert",
      "boolean",
      "constraint",
      "constraints",
      "CP",
      "CPLEX",
      "cumulFunction",
      "dexpr",
      "diff",
      "div",
      "dvar",
      "else",
      "execute",
      "false",
      "float",
      "float+",
      "forall",
      "from",
      "if",
      "in",
      "include",
      "infinity",
      "int",
      "int+",
      "intensity",
      "inter",
      "interval",
      "invoke",
      "key",
      "main",
      "max",
      "maximize",
      "maxint",
      "min",
      "minimize",
      "mod",
      "not",
      "optional",
      "or",
      "ordered",
      "piecewise",
      "prepare",
      "prod",
      "pwlFunction",
      "range",
      "reversed",
      "sequence",
      "setof",
      "SheetConnection",
      "SheetRead",
      "SheetWrite",
      "size",
      "sorted",
      "stateFunction",
      "stepFunction",
      "stepwise",
      "string",
      "subject",
      "sum",
      "symdiff",
      "to",
      "true",
      "tuple",
      "types",
      "union",
      "using",
      "with"
    ],
    operators: [
      "all",
      "^",
      "prod",
      "inter",
      "*",
      "/",
      "div",
      "%",
      "mod",
      "inter",
      "sum",
      "max",
      "min",
      "union",
      "+",
      "-",
      "!",
      "union",
      "diff",
      "symdiff",
      "..",
      "in",
      "not",
      "==",
      "<=",
      ">=",
      "<",
      ">",
      "!=",
      "(<=.<=)",
      "(>=.>=)",
      "&&",
      "and",
      "||",
      "or",
      "=>",
      "?",
      ":"
    ],
    functions: [
      "abs",
      "allDifferent",
      "allMinDistance",
      "allowedAssignments",
      "alternative",
      "alwaysConstant",
      "alwaysEqual",
      "alwaysIn",
      "alwaysNoState",
      "append",
      "asSet",
      "before",
      "card",
      "ceil",
      "count",
      "cumulFunctionValue",
      "distToInt",
      "dual",
      "element",
      "endAtEnd",
      "endAtStart",
      "endBeforeEnd",
      "endBeforeStart",
      "endEval",
      "endOf",
      "exp",
      "first",
      "first",
      "floatValue",
      "floor",
      "forbiddenAssignments",
      "forbidEnd",
      "forbidExtent",
      "forbidStart",
      "frac",
      "ftoi",
      "heightAtEnd",
      "heightAtStart",
      "intValue",
      "inverse",
      "item",
      "last",
      "last",
      "length",
      "lengthEval",
      "lengthOf",
      "lex",
      "lg",
      "ln",
      "log",
      "matchAt",
      "maxl",
      "minl",
      "next",
      "nextc",
      "noOverlap",
      "numberOfSegments",
      "ord",
      "pack",
      "pow",
      "presenceOf",
      "prev",
      "prev",
      "prevc",
      "pulse",
      "rand",
      "reducedCost",
      "round",
      "segmentEnd",
      "segmentStart",
      "segmentValue",
      "sgn",
      "sizeEval",
      "sizeOf",
      "slack",
      "span",
      "sqrt",
      "srand",
      "standardDeviation",
      "startAtEnd",
      "startAtStart",
      "startBeforeEnd",
      "startBeforeStart",
      "startEval",
      "startOf",
      "stateFunctionValue",
      "step",
      "stepAtEnd",
      "stepAtStart",
      "synchronize",
      "trunc",
      "writeln"
    ],
    symbols: /[=><!~?:&|+\-*\/\^%]+/,
    escapes: /\\(?:[abfnrtv\\"']|x[0-9A-Fa-f]{1,4}|u[0-9A-Fa-f]{4}|U[0-9A-Fa-f]{8})/,
    tokenizer: {
      root: [
        [
          /[a-zA-Z_][\w+]*/,
          {
            cases: {
              "@keywords": { token: "keyword.$0" },
              "@functions": { token: "function.builtin.$0" },
              "@default": "identifier"
            }
          }
        ],
        { include: "@whitespace" },
        [/[{}()\[\]]/, "@brackets"],
        [
          /@symbols/,
          {
            cases: {
              "@operators": "operator",
              "@default": ""
            }
          }
        ],
        [/\d+(\.\d+)?/, "number"],
        [/[;,.]/, "delimiter"],
        [/"([^"\\]|\\.)*$/, "string.invalid"],
        [/'([^'\\]|\\.)*$/, "string.invalid"],
        [/"/, "string", "@string_double"],
        [/'/, "string", "@string_single"],
      ],
      whitespace: [
        [/[ \t\r\n]+/, ""],
        [/\/\*/, "comment", "@comment"],
        [/\/\/\/.*$/, "comment.teacher"],
        [/\/\/.*$/, "comment"]
      ],
      comment: [
        [/[^\/*]+/, "comment"],
        [/\*\//, "comment", "@pop"],
        [/[\/*]/, "comment"]
      ],
      string_double: [
        [/[^\\"]+/, "string"],
        [/@escapes/, "string.escape"],
        [/\\./, "string.escape.invalid"],
        [/"/, "string", "@pop"]
      ],
      string_single: [
        [/[^\\']+/, "string"],
        [/@escapes/, "string.escape"],
        [/\\./, "string.escape.invalid"],
        [/'/, "string", "@pop"]
      ]
    }
  };
  return opl_exports;
})();
return moduleExports;
});
