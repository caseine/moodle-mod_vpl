<?php
// This file is part of VPL for Moodle - http://vpl.dis.ulpgc.es/
//
// VPL for Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// VPL for Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with VPL for Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Common functions of VPL
 *
 * @package mod_vpl
 * @copyright 2012 Juan Carlos Rodríguez-del-Pino
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author Juan Carlos Rodríguez-del-Pino <jcrodriguez@dis.ulpgc.es>
 */
defined('MOODLE_INTERNAL') || die();

require_once(dirname(__FILE__).'/locallib_consts.php');
require_once(dirname(__FILE__).'/vpl.class.php');

/**
 * @codeCoverageIgnore
 *
 * Set get vpl session var
 *
 * @param string $varname
 *            name of the session var without 'vpl_'
 * @param string $default
 *            default value
 * @param string $parname
 *            optional parameter name
 * @return $varname/param value
 */
function vpl_get_set_session_var($varname, $default, $parname = null) {
    global $SESSION;
    if ($parname == null) {
        $parname = $varname;
    }
    $res = $default;
    $fullname = 'vpl_' . $varname;
    if (isset( $SESSION->$fullname )) {
        $res = $SESSION->$fullname;
    }
    $res = optional_param( $parname, $res, PARAM_RAW );
    $SESSION->$fullname = $res;
    return $res;
}

/**
 * @codeCoverageIgnore
 *
 * Create directory if not exist
 *
 * @param $dir string path to directory
 */
function vpl_create_dir($dir) {
    global $CFG;
    if (! file_exists( $dir )) { // Create dir?
        if (! mkdir( $dir, $CFG->directorypermissions, true ) ) {
            throw new file_exception('storedfileproblem', 'Error creating a directory to save files in VPL');
        }
    }
}


/**
 * @codeCoverageIgnore
 *
 * Open/create a file and its dir
 *
 * @param $filename string path to file
 * @return Object file descriptor
 */
function vpl_fopen($filename) {
    if (! file_exists( $filename )) { // Exists file?
        $dir = dirname( $filename );
        vpl_create_dir($dir);
    }
    $fp = fopen( $filename, 'w+b' );
    if ($fp === false) {
        if (DIRECTORY_SEPARATOR == '\\' ) {
            $pathparts = pathinfo($filename);
            $name = $pathparts['filename'];
            if (preg_match( '/(^aux$)|(^con$)|(^prn$)|(^nul$)|(^com\d$)|(^lpt\d$)/i' , $name) == 1) {
                $patchedfilename = $pathparts['dirname'] . '\\_n_p_' . $pathparts['basename'];
                return vpl_fopen($patchedfilename);
            }
        }
    }
    if ($fp === false) {
        debugging( "Error creating file in VPL '$filename'.", DEBUG_NORMAL );
        throw new file_exception('storedfileproblem', "Error creating file in VPL.");
    }
    return $fp;
}

/**
 * @codeCoverageIgnore
 *
 * Open/create a file and its dir and write contents
 *
 * @param string $filename. Path to the file to open
 * @param string $contents. Contents to write into the file
 * @exception file_exception
 * @return void
 */
function vpl_fwrite($filename, $contents) {
    if ( is_file($filename) ) {
        unlink($filename);
    }
    $fd = vpl_fopen( $filename );
    $res = ftruncate ( $fd, 0);
    $res = $res && (fwrite( $fd, $contents ) !== false);
    $res = fclose( $fd ) && $res;
    if ($res === false) {
        throw new file_exception('storedfileproblem', 'Error writing a file in VPL');
    }
}

/**
 * @codeCoverageIgnore
 *
 * Recursively delete a directory
 *
 * @return bool All delete
 */
function vpl_delete_dir($dirname) {
    $ret = false;
    if (file_exists( $dirname )) {
        $ret = true;
        if (is_dir( $dirname )) {
            $dd = opendir( $dirname );
            if (! $dd) {
                return false;
            }
            $list = [];
            while ( $name = readdir( $dd ) ) {
                if ($name != '.' && $name != '..') {
                    $list[] = $name;
                }
            }
            closedir( $dd );
            foreach ($list as $name) {
                $ret = vpl_delete_dir( $dirname . '/' . $name ) && $ret;
            }
            $ret = rmdir( $dirname ) && $ret;
        } else {
            $ret = unlink( $dirname );
        }
    }
    return $ret;
}

/**
 * @codeCoverageIgnore
 *
 * Outputs a zip file and removes it. Must be called before any other output
 *
 * @param string $zipfilename. Name of the ZIP file with the data
 * @param string $name of file to be shown, without '.zip'
 *
 */
function vpl_output_zip($zipfilename, $name) {
    if (! file_exists($zipfilename)) {
        debugging("Zip file not found " . $zipfilename, DEBUG_DEVELOPER);
        throw new moodle_exception('error:zipnotfound', 'mod_vpl');
    }
    // Send zipdata.
    $blocksize = 1000 * 1024;
    $size = filesize( $zipfilename );
    $cname = rawurlencode( $name . '.zip' );
    $contentdisposition = 'Content-Disposition: attachment;';
    $contentdisposition .= ' filename="' . $name . '.zip";';
    $contentdisposition .= ' filename*=utf-8\'\'' . $cname;

    @header( 'Content-Length: ' . $size );
    @header( 'Content-Type: application/zip; charset=utf-8' );
    @header( $contentdisposition );
    @header( 'Cache-Control: private, must-revalidate, pre-check=0, post-check=0, max-age=0' );
    @header( 'Content-Transfer-Encoding: binary' );
    @header( 'Expires: 0' );
    @header( 'Pragma: no-cache' );
    @header( 'Accept-Ranges: none' );
    // Get zip data.
    $offset = 0;
    while ($offset < $size) {
        echo file_get_contents( $zipfilename, false,  null, $offset, $blocksize);
        $offset += $blocksize;
    }
    // Remove zip file.
    unlink( $zipfilename );
}

/**
 * @codeCoverageIgnore
 *
 * Get lang code @parm $bashadapt true adapt lang to bash LANG (default true)
 *
 * @return string
 */
function vpl_get_lang($bashadapt = true) {
    global $SESSION, $USER, $CFG;
    $commonlangs = [
            'aa' => 'DJ',
            'af' => 'ZA',
            'am' => 'ET',
            'an' => 'ES',
            'az' => 'AZ',
            'ber' => 'DZ',
            'bg' => 'BG',
            'ca' => 'ES',
            'cs' => 'CZ',
            'da' => 'DK',
            'de' => 'DE',
            'dz' => 'BT',
            'en' => 'US',
            'es' => 'ES',
            'et' => 'EE',
            'fa' => 'IR',
            'fi' => 'FI',
            'fr' => 'FR',
            'he' => 'IL',
            'hu' => 'HU',
            'ig' => 'NG',
            'it' => 'IT',
            'is' => 'IS',
            'ja' => 'JP',
            'km' => 'KH',
            'ko' => 'KR',
            'lo' => 'LA',
            'lv' => 'LV',
            'pt' => 'PT',
            'ro' => 'RO',
            'ru' => 'RU',
            'se' => 'NO',
            'sk' => 'sk',
            'so' => 'SO',
            'sv' => 'SE',
            'or' => 'IN',
            'th' => 'th',
            'ti' => 'ET',
            'tk' => 'TM',
            'tr' => 'TR',
            'uk' => 'UA',
            'yo' => 'NG',
    ];
    if (isset( $SESSION->lang )) {
        $lang = $SESSION->lang;
    } else if (isset( $USER->lang )) {
        $lang = $USER->lang;
    } else if (isset( $CFG->lang )) {
        $lang = $CFG->lang;
    } else {
        return "en";
    }
    if ($bashadapt) {
        $parts = explode( '_', $lang );
        if (count($parts) == 2) {
            $lang = $parts[0];
        }
        if (isset($commonlangs[$lang])) {
            $lang = $lang . '_' . $commonlangs[$lang];
        }
        $lang .= '.UTF-8';
    }
    return $lang;
}

/**
 * @codeCoverageIgnore
 *
 * generate URL to page with params
 *
 * @param $page string
 *            page from wwwroot
 * @param $var1 string
 *            var1 name optional
 * @param $value1 string
 *            value of var1 optional
 * @param $var2 string
 *            var2 name optional
 * @param $value2 string
 *            value of var2 optional
 * @param
 *            ...
 */
function vpl_abs_href() {
    global $CFG;
    $parms = func_get_args();
    $l = count( $parms );
    $href = $CFG->wwwroot . $parms[0];
    for ($p = 1; $p < $l - 1; $p += 2) {
        $href .= ($p > 1 ? '&amp;' : '?') . urlencode( $parms[$p] ) . '=' . urlencode( $parms[$p + 1] );
    }
    return $href;
}

/**
 * @codeCoverageIgnore
 *
 * generate URL to page with params
 *
 * @param $page string
 *            page from wwwroot/mod/vpl/
 * @param $var1 string
 *            var1 name optional
 * @param $value1 string
 *            value of var1 optional
 * @param $var2 string
 *            var2 name optional
 * @param $value2 string
 *            value of var2 optional
 * @param
 *            ...
 */
function vpl_mod_href() {
    global $CFG;
    $parms = func_get_args();
    $l = count( $parms );
    $href = $CFG->wwwroot . '/mod/vpl/' . $parms[0];
    for ($p = 1; $p < $l - 1; $p += 2) {
        $href .= ($p > 1 ? '&amp;' : '?') . urlencode( $parms[$p] ) . '=' . urlencode( $parms[$p + 1] );
    }
    return $href;
}

/**
 * @codeCoverageIgnore
 *
 * @todo This function is to be remove when Moodle 3.10 be not supported by VPL.
 * Return 'gradeoun' or 'grade' for backward compatibility.
 * @return string
 */
function vpl_get_gradenoun_str() {
    if (get_string_manager()->string_exists('gradenoun', 'core')) {
        return 'gradenoun';
    }
    return 'grade';
}

/**
 * @codeCoverageIgnore
 *
 * generate URL relative page with params
 *
 * @param $page string
 *            page relative
 * @param $var1 string
 *            var1 name optional
 * @param $value1 string
 *            value of var1 optional
 * @param $var2 string
 *            var2 name optional
 * @param $value2 string
 *            value of var2 optional
 * @param
 *            ...
 */
function vpl_rel_url() {
    $parms = func_get_args();
    $l = count( $parms );
    $url = $parms[0];
    for ($p = 1; $p < $l - 1; $p += 2) {
        $url .= ($p > 1 ? '&amp;' : '?') . urlencode( $parms[$p] ) . '=' . urlencode( $parms[$p + 1] );
    }
    return $url;
}
/**
 * @codeCoverageIgnore
 *
 * Add a parm to a url
 *
 * @param $url string
 * @param $parm string
 *            name
 * @param $value string
 *            value of parm
 */
function vpl_url_add_param($url, $parm, $value) {
    if (strpos( $url, '?' )) {
        return $url . '&amp;' . urlencode( $parm ) . '=' . urlencode( $value );
    } else {
        return $url . '?' . urlencode( $parm ) . '=' . urlencode( $value );
    }
}

/**
 * @codeCoverageIgnore
 *
 * Print a message and redirect
 *
 * @param string $link. The URL to redirect to
 * @param string $message to be print
 * @param string $type of message (success, info, warning, error). Default = info
 * @return void
 */
function vpl_redirect($link, $message, $type = 'info', $errorcode='') {
    global $OUTPUT;
    if (!$OUTPUT->has_started()) {
        echo $OUTPUT->header();
    }
    echo $OUTPUT->notification($message, $type);
    echo $OUTPUT->continue_button($link);
    echo $OUTPUT->footer();
    die();
}

/**
 * @codeCoverageIgnore
 *
 * Set JavaScript file from subdir jscript to be load
 *
 * @param $file string
 *            name of file to load
 * @param $defer boolean
 *            optional set if the load is inmediate or deffered
 * @return void
 */
function vpl_include_jsfile($file, $defer = true) {
    global $PAGE;
    $PAGE->requires->js( new moodle_url( '/mod/vpl/jscript/' . $file ), ! $defer );
}

/**
 * @codeCoverageIgnore
 *
 * Set JavaScript code to be included
 *
 * @param $jscript string
 *            JavaScript code
 * @return void
 */
function vpl_include_js($jscript) {
    if ($jscript == '') {
        return '';
    }
    $ret = '<script type="text/javascript">';
    $ret .= "\n//<![CDATA[\n";
    $ret .= $jscript;
    $ret .= "\n//]]>\n</script>\n";
    return $ret;
}

/**
 * @codeCoverageIgnore
 *
 * Popup message box to show text
 *
 * @param string $text to show. It use s() to sanitize text
 * @param boolean $print or not
 * @return void
 */
function vpl_js_alert($text, $print = true) {
    $aux = addslashes( $text ); // Sanitize text.
    $aux = str_replace( "\n", "\\n", $aux ); // Add \n to show multiline text.
    $aux = str_replace( "\r", "", $aux ); // Remove \r.
    $ret = vpl_include_js( 'alert("' . $aux . '");' );
    if ($print) {
        echo $ret;
        @ob_flush();
        flush();
    } else {
        return $ret;
    }
}

/**
 * @codeCoverageIgnore
 */
function vpl_get_select_time($maximum = null, $minimum = 4) {
    $minute = 60;
    if ($maximum === null) { // Default value.
        $maximum = 35 * $minute;
    }
    $ret = [
            0 => get_string( 'select' ),
    ];
    if ($maximum <= 0) {
        return $ret;
    }
    $value = $minimum;
    if ($maximum < $value) {
        $value = $maximum;
    }
    while ( $value <= $maximum ) {
        if ($value < $minute) {
            $ret[$value] = get_string( 'numseconds', '', $value );
        } else {
            $num = ( int ) ($value / $minute);
            $ret[$num * $minute] = get_string( 'numminutes', '', $num );
            $value = $num * $minute;
        }
        $value *= 2;
    }
    return $ret;
}

/**
 * @codeCoverageIgnore
 *
 * Converts a size in byte to string in Kb, Mb, Gb and Tb.
 * Follows IEC "Prefixes for binary multiples".
 *
 * @param int $size Size in bytes
 *
 * @return string
 */
function vpl_conv_size_to_string($size) {
    static $measure = [
            1024,
            1048576,
            1073741824,
            1099511627776,
            PHP_INT_MAX,
    ];
    static $measurename = [
            'KiB',
            'MiB',
            'GiB',
            'TiB',
    ];
    for ($i = 0; $i < count( $measure ) - 1; $i ++) {
        if ($measure[$i] <= 0) { // Check for int overflow.
            $num = $size / $measure[$i - 1];
            return sprintf( '%.2f %s', $num, $measurename[$i - 1] );
        }
        if ($size < $measure[$i + 1]) {
            $num = $size / $measure[$i];
            if ($num >= 3 || $size % $measure[$i] == 0) {
                return sprintf( '%4d %s', $num, $measurename[$i] );
            } else {
                return sprintf( '%.2f %s', $num, $measurename[$i] );
            }
        }
    }
}

/**
 * @codeCoverageIgnore
 *
 * Return the array key after or equal to value
 *
 * @param $array
 * @param int $value of key to search >=
 * @return int key found
 */
function vpl_get_array_key($array, int $value) {
    reset($array);
    $last = 0;
    while (($key = key($array)) !== null) {
        if ($key >= $value) {
            reset($array);
            return $key;
        }
        $last = $key;
        next($array);
    }
    reset($array);
    return $last;
}

/**
 * @codeCoverageIgnore
 *
 * Returns un array with the format [size in bytes] => size in text.
 * The first element is [0] => select.
 *
 * @param int $minimum the initial value
 * @param int $maximum the limit of values generates
 *
 * @return array Key value => Text value
 */
function vpl_get_select_sizes(int $minimum = 0, int $maximum = PHP_INT_MAX): array {
    $maximum = ( int ) $maximum;
    if ($maximum < 0) {
        $maximum = PHP_INT_MAX;
    }
    if ($maximum > 17.0e9) {
        $maximum = 16 * 1073741824;
    }
    $ret = [
            0 => get_string( 'select' ),
    ];
    if ($minimum > 0) {
        $value = $minimum;
    } else {
        $value = 256 * 1024;
    }
    $pre = 0;
    $increment = $value / 4;
    while ( $value <= $maximum && $value > 0 ) { // Avoid int overflow.
        $ret[$value] = vpl_conv_size_to_string( $value );
        $pre = $value;
        $value += $increment;
        $value = ( int ) $value;
        if ($pre >= $value) { // Check for loop end.
            break;
        }
        if ($value >= 8 * $increment) {
            $increment = $value / 4;
        }
    }
    if ($pre < $maximum) { // Show limit value.
        $ret[$maximum] = vpl_conv_size_to_string($maximum);
    }
    return $ret;
}

/**
 * @codeCoverageIgnore
 *
 * Detects end of line separator.
 *
 * @param string& $data Text to check.
 *
 * @return string Newline separator "\r\n", "\n", "\r".
 */
function vpl_detect_newline(&$data) {
    // Detect text newline chars.
    if (strrpos( $data, "\r\n" ) !== false) {
        return "\r\n"; // Windows.
    } else if (strrpos($data, "\n") !== false) {
        return "\n"; // UNIX.
    } else if (strrpos($data, "\r") !== false) {
        return "\r"; // Mac.
    } else {
        return "\n"; // Default Unix.
    }
}

/**
 * @codeCoverageIgnore
 */
function vpl_notice(string $text, $type = 'success') {
    global $OUTPUT;
    echo $OUTPUT->notification($text, $type);
}

/**
 * @codeCoverageIgnore
 *
 * Get if filename has image extension
 * @param string $filename
 * @return boolean
 */
function vpl_is_image($filename) {
    return preg_match( '/^(avif|bmp|gif|ico|jpe?g|png|svg|tiff?|webp)$/i', pathinfo( $filename, PATHINFO_EXTENSION ) );
}

/**
 * @codeCoverageIgnore
 *
 * Get if filename has binary extension or binary data
 * @param string $filename
 * @param string &$data file contents
 * @return boolean
 */
function vpl_is_binary($filename, &$data = false) {
    if ( vpl_is_image( $filename ) ) {
        return true;
    }
    $fileext = 'zip|jar|pdf|tar|bin|7z|arj|deb|gzip|';
    $fileext .= 'rar|rpm|db|dll|rtf|doc|docx|odt|xls|xlsx|exe|com';
    if ( preg_match( '/^(' . $fileext . ')$/i', pathinfo( $filename, PATHINFO_EXTENSION ) ) ) {
        return true;
    }
    if ($data === false) {
        return false;
    }
    return mb_detect_encoding( $data, 'UTF-8', true ) != 'UTF-8';
}

function vpl_is_notebook($filename) {
    return preg_match( '/^ipynb$/i', pathinfo( $filename, PATHINFO_EXTENSION ) );
}

/**
 * @codeCoverageIgnore
 *
 * Return if path is valid
 * @param string $path
 * @return boolean
 */
function vpl_is_valid_path_name($path) {
    if (strlen( $path ) > 256) {
        return false;
    }
    $dirs = explode( '/', $path );
    for ($i = 0; $i < count( $dirs ); $i ++) {
        if (! vpl_is_valid_file_name( $dirs[$i] )) {
            return false;
        }
    }
    return true;
}

/**
 * @codeCoverageIgnore
 *
 * Return if file or directory name is valid
 * @param string $name
 * @return boolean
 */
function vpl_is_valid_file_name($name) {
    $backtick = chr( 96 ); // Avoid warnning in codecheck.
    $regexp = '/[\x00-\x1f]|[:-@]|[{-~]|\\\\|\[|\]|[\/\^';
    $regexp .= $backtick . '´]|^\-|^ | $|^\.$|^\.\.$/';
    if (strlen( $name ) < 1) {
        return false;
    }
    if (strlen( $name ) > 128) {
        return false;
    }
    return preg_match( $regexp, $name ) === 0;
}

/**
 * @codeCoverageIgnore
 *
 * Truncate string to the limit passed
 * @param string &$string
 * @param int $limit
 */
function vpl_truncate_string(&$string, $limit) {
    if (strlen( $string ) <= $limit) {
        return;
    }
    $string = substr( $string, 0, $limit - 3 ) . '...';
}

/**
 * @codeCoverageIgnore
 */
function vpl_bash_export($var, $value) {
    if ( is_int($value) ) {
        $ret = "export $var=$value\n";
    } else if (is_array($value)) {
        $ret = "export $var=( ";
        foreach ($value as $data) {
            $ret .= '"' . str_replace('"', '\"', $data) . '" ';
        }
        $ret .= ")\n";
    } else {
        $ret = "export $var=\"";
        $ret .= str_replace('"', '\"', $value);
        $ret .= "\"\n";
    }
    return $ret;
}

/**
 * @codeCoverageIgnore
 *
 * For debug purpose
 * Return content of vars ready to HTML
 */
function vpl_s() {
    $var = func_get_args();
    ob_start();
    call_user_func_array('var_dump', $var);
    $content = ob_get_contents();
    ob_end_clean();
    return htmlspecialchars($content, ENT_QUOTES);
}

/**
 * @codeCoverageIgnore
 *
 * Truncate string fields of the VPL table
 * @param $instance object with the record
 * @return void
 */
function vpl_truncate_vpl($instance) {
    if (isset($instance->password)) {
        $instance->password = trim($instance->password);
    }
    foreach (['name', 'requirednet', 'password', 'variationtitle'] as $field) {
        if (isset($instance->$field)) {
            vpl_truncate_string( $instance->$field, 255 );
        }
    }
}

/**
 * @codeCoverageIgnore
 *
 * Truncate string fields of the variations table
 * @param $instance object with the record
 * @return void
 */
function vpl_truncate_variations($instance) {
    vpl_truncate_string( $instance->identification, 40 );
}

/**
 * @codeCoverageIgnore
 *
 * Truncate string fields of the running_processes table
 * @param $instance object with the record
 * @return void
 */
function vpl_truncate_running_processes($instance) {
    vpl_truncate_string( $instance->server, 255 );
}

/**
 * @codeCoverageIgnore
 *
 * Truncate string fields of the jailservers table
 * @param $instance object with the record
 * @return void
 */
function vpl_truncate_jailservers($instance) {
    vpl_truncate_string( $instance->laststrerror, 255 );
    vpl_truncate_string( $instance->server, 255 );
}

/**
 * @codeCoverageIgnore
 *
 * Check if IP is within networks
 *
 * @param $networks string with conma separate networks
 * @param $ip string optional with the IP to check, if omited then remote IP
 *
 * @return boolean true found
 */
function vpl_check_network($networks, $ip = false) {
    $networks = trim($networks);
    if ($networks == '') {
        return true;
    }
    if ($ip === false) {
        $ip = getremoteaddr();
    }
    return address_in_subnet( $ip, $networks );
}

/**
 * @codeCoverageIgnore
 *
 * Get awesome icon for action
 * @param String $id
 * @return string
 */
function vpl_get_awesome_icon($str, $classes = '') {
    $icon = 'mod_vpl:' . $str;
    $imap = mod_vpl_get_fontawesome_icon_map();
    if ( isset( $imap[$icon]) ) {
        $ficon = $imap[$icon];
        return '<i class="fa ' . $ficon . $classes . '"></i> ';
    }
    return '';
}


/**
 * @codeCoverageIgnore
 *
 * Create a new tabobject for navigation
 * @param String $id
 * @param string|moodle_url $href
 * @param string $str to be i18n
 * @param string $comp component
 * @return tabobject
 */
function vpl_create_tabobject($id, $href, $str, $comp = 'mod_vpl') {
    $stri18n = get_string( $str, $comp);
    $strdescription = vpl_get_awesome_icon($str) . $stri18n;
    return new tabobject( $id, $href, $strdescription, $stri18n );
}

/**
 * @codeCoverageIgnore
 *
 * Get version string
 * @return string
 */
function vpl_get_version() {
    static $version = '';
    if ($version === '') {
        $plugin = new stdClass();
        require(dirname( __FILE__ ) . '/version.php');
        $version = $plugin->release;
    }
    return $version;
}

/**
 * @codeCoverageIgnore
 *
 * Polyfill for getting user picture fields
 * @return string List of fields separated by "," u.field
 */
function vpl_get_picture_fields() {
    if (method_exists('\core_user\fields', 'get_picture_fields')) {
        return 'u.' . implode(',u.', \core_user\fields::get_picture_fields());
    } else {
        return user_picture::fields( 'u' );
    }
}

/**
 * @codeCoverageIgnore
 * Agregate usersids and groupsids of array of objects of override assigned records
 * @param array $overridesseparated of objects of records
 * @return array
 */
function vpl_agregate_overrides($overridesseparated) {
    $usersids = [];
    $groupids = [];
    foreach ($overridesseparated as $override) {
        if (!isset($usersids[$override->id])) {
            $usersids[$override->id] = [];
            $groupids[$override->id] = [];
        }
        if (!empty($override->usersids)) {
            array_push($usersids[$override->id], $override->usersids);
        }
        if (!empty($override->groupids)) {
            array_push($groupids[$override->id], $override->groupids);
        }
    }
    $overrides = [];
    foreach ($overridesseparated as $override) {
        if (!isset($overrides[$override->id])) {
            $override->usersids = implode(',', $usersids[$override->id]);
            $override->groupids = implode(',', $groupids[$override->id]);
            $overrides[$override->id] = $override;
        }
    }
    return $overrides;
}

/**
 * Calls a function with lock.
 * @param string $locktype Name of the lock type (unique)
 * @param string $resource Name of the resourse (unique)
 * @param string $function Name of the function to call
 * @param array $parms Parameters to pass to the function
 * @return mixed Value returned by the function or throw exception
 */
function vpl_call_with_lock(string $locktype, string $resource, string $function, array & $parms) {
    $lockfactory = \core\lock\lock_config::get_lock_factory($locktype);
    if ($lock = $lockfactory->get_lock($resource, VPL_LOCK_TIMEOUT)) {
        try {
            $result = $function(...$parms);
            $lock->release();
            return $result;
        } catch (\Throwable $e) {
            $lock->release();
            throw $e;
        }
    } else {
        throw new moodle_exception('locktimeout');
    }
    return false;
}

/**
 * Calls a function with DB transactions.
 * @param string $function Name of the function to call
 * @param array $parms Parameters to pass to the function
 * @return mixed Value returned by the function or throw exception
 */
function vpl_call_with_transaction(string $function, array $parms) {
    global $DB;
    $transaction = $DB->start_delegated_transaction();
    try {
        $result = $function(...$parms);
        $transaction->allow_commit();
        return $result;
    } catch (\Throwable $e) {
        $transaction->rollback($e);
    }
}

/**
 * Return full path to the directory of scripts.
 *
 * @return string
 * @codeCoverageIgnore
 */
function vpl_get_scripts_dir() {
    global $CFG;
    return $CFG->dirroot . '/mod/vpl/jail/default_scripts';
}

function vpl_get_name_fields_display() {
    if (method_exists('\core_user\fields', 'get_name_fields')) {
        $namefields = \core_user\fields::get_name_fields();
    } else {
        $namefields = get_all_user_name_fields();
    }
    // Load name display format from language.
    return implode(' / ', array_map('get_string', order_in_string($namefields, get_string('fullnamedisplay'))));
}

/**
 * Return action_menu_link for menu in list
 * @param string $str
 * @param moodle_url $link
 * @param string $comp value for get_string
 * @return action_menu_link_secondary
 */
function vpl_get_menu_action_link($str, $link, $comp = 'mod_vpl') {
    $stri18n = get_string($str, $comp);
    return new action_menu_link_secondary($link, new pix_icon($str, '', 'mod_vpl'),  $stri18n);
}

function vpl_info_icon() {
    global $OUTPUT;
    return $OUTPUT->pix_icon('i/info', get_string('info'), 'moodle', [ 'class' => 'text-info' ]);
}

function vpl_get_copytoclipboard_control($text) {
    $text = addslashes_js(str_replace("\r", '', str_replace("\n", '\n', $text)));
    $strsuccess = addslashes_js(nl2br(get_string('copytoclipboardsuccess', VPL)));
    $strfailure = addslashes_js(nl2br(get_string('copytoclipboarderror', VPL)));
    $js = "
        var parentDiv = this.parentNode;
        var notify = function(message) {
            var x = document.createElement('span');
            x.textContent = message;
            x.classList = 'badge badge-primary rounded mx-1 align-text-bottom';
            parentDiv.append(x);
            setTimeout(() => x.remove(), 1300);
        };
        navigator.clipboard.writeText('$text')
        .then(
            () => notify('$strsuccess'),
            () => notify('$strfailure')
        );";
    return html_writer::span('<i class="fa fa-clone"></i>', 'clickable btn-link text-decoration-none mx-1',
            [ 'title' => get_string('copytoclipboard', VPL), 'onclick' => $js ]);
}

function vpl_print_copyable_info($title, $displayedinfo, $copyinfo = null) {
    if ($copyinfo === null) {
        $copyinfo = $displayedinfo;
    }
    echo html_writer::div('<span style="user-select:none;">' . $title . ' </span>' .
            '<b>' . $displayedinfo . '</b>' . vpl_get_copytoclipboard_control($copyinfo));
}

/**
 * Fetch all VPLs that can be used as a base for the specified VPL.
 * The computation depends on the capabilities of current $USER.
 * @param mod_vpl $vpl
 * @param boolean $asgroupmenu If true, then this function returns the result as an associative array of arrays,
 *                             to be used in a selectgroups form element (grouped by course and ordered alphabetically).
 *                             If false, then this function returns the result as an associative array [vplid => vplname].
 * @return string[][]||string[]
 */
function vpl_get_available_bases($vpl, $asgroupmenu = false) {
    global $DB;
    $vplid = $vpl->get_instance()->id;
    $bases = [];
    if ($asgroupmenu) {
        $bases[''] = [ get_string( 'select' ) ];
    }
    // Search VPLs in courses where user is enrolled. Always consider current course for consistency (and admins not enrolled).
    $courseslist = [ $vpl->get_course()->id => $vpl->get_course() ] + enrol_get_my_courses();
    foreach (array_keys($courseslist) as $courseid) {
        $basesincourse = [];
        foreach (get_coursemodules_in_course(VPL, $courseid) as $cm) {
            if ($cm->instance != $vplid) {
                $base = new mod_vpl($cm->id);
                if ($base->is_use_as_base() && $vpl->has_capability(VPL_GRADE_CAPABILITY)) {
                    $basesincourse[$cm->instance] = $base->get_printable_name();
                }
            }
        }
        if (!empty($basesincourse)) {
            if ($asgroupmenu) {
                // Sort alphabetically.
                asort($basesincourse);
                $bases[get_string('fromcourse', VPL, get_course($courseid)->shortname)] = $basesincourse;
            } else {
                $bases += $basesincourse;
            }
        }
    }

    // Add shared VPLs if local_metadata and local_sharedspaceh are installed, and required capability is held.
    $metadataplugin = core_component::get_plugin_directory('local', 'metadata');
    $sharedspaceplugin = core_component::get_plugin_directory('local', 'sharedspaceh');
    if ($metadataplugin !== null && $sharedspaceplugin !== null
            && has_capability('local/sharedspaceh:accesstospaceh', context_system::instance())) {
        $sharedbases = [];
        $sharedvpls = $DB->get_records_sql(
                "SELECT cm.id, cm.instance
                   FROM {course_modules} cm
                   JOIN {modules} m ON m.id = cm.module AND m.name = 'vpl'
                   JOIN {vpl} vpl ON vpl.id = cm.instance
                   JOIN {local_metadata} lm ON cm.id = lm.instanceid
                  WHERE vpl.usableasbase = 1 AND lm.fieldid = :sharedfieldid AND lm.data = 1 AND cm.instance <> :vplid",
                [ 'sharedfieldid' => get_config('local_sharedspaceh', 'metadatasharedfield') ?: 0, 'vplid' => $vplid ]);
        foreach ($sharedvpls as $sharedvpl) {
            $sharedbases[$sharedvpl->instance] = (new mod_vpl($sharedvpl->id))->get_printable_name();
        }
        if (!empty($sharedbases)) {
            if ($asgroupmenu) {
                // Sort alphabetically.
                asort($sharedbases);
                $bases[get_string('shared', VPL)] = $sharedbases;
            } else {
                $bases += $sharedbases;
            }
        }
    }

    return $bases;
}

/**
 * Get current vpl instance data to fill in forms. This is meant to be called directly as an argument to $form->set_data().
 * @param mod_vpl $vpl
 * @return array Associative array
 */
function vpl_get_current_instance_data_for_form($vpl) {
    $currentdata = [];
    foreach ($vpl->get_instance() as $field => $value) {
        if ($field === 'id') {
            // Do not mix up vpl instance id and course module id!
            continue;
        }
        if ($value !== null && $value !== '') {
            $currentdata[$field] = $value; // Build data with only non-empty values (else use default).
        }
    }
    return $currentdata;
}
