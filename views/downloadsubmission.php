<?php
// This file is part of VPL for Moodle - http://vpl.dis.ulpgc.es/
//
// VPL for Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// VPL for Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with VPL for Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Download submission in zip file
 *
 * @package mod_vpl
 * @copyright 2012 Juan Carlos Rodríguez-del-Pino
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author Juan Carlos Rodríguez-del-Pino <jcrodriguez@dis.ulpgc.es>
 */

define( 'NO_DEBUG_DISPLAY', true );

require_once(dirname(__FILE__).'/../../../config.php');
require_once(dirname(__FILE__).'/../similarity/watermark.class.php');
require_once(dirname(__FILE__).'/../locallib.php');
require_once(dirname(__FILE__).'/../vpl.class.php');
require_once(dirname(__FILE__).'/../vpl_submission.class.php');

global $DB, $USER;
require_login();
$id = required_param( 'id', PARAM_INT );
$userid = optional_param( 'userid', $USER->id, PARAM_INT );
$submissionid = optional_param( 'submissionid', false, PARAM_INT );
$vpl = new mod_vpl( $id );

if (!$vpl->has_capability( VPL_GRADE_CAPABILITY )) {
    // Download own submission.
    $vpl->require_capability( VPL_VIEW_CAPABILITY );
    $userid = $USER->id;
    $vpl->restrictions_check();
}

// Read record.
if ($submissionid) {
    $subinstance = $DB->get_record( 'vpl_submissions', [ 'id' => $submissionid ] );
} else {
    $subinstance = $vpl->last_user_submission( $userid );
}

// Check consistence.
if (! $subinstance) {
    throw new moodle_exception( 'nosubmission', VPL );
}
if ($subinstance->vpl != $vpl->get_instance()->id) {
    throw new moodle_exception( 'vpl submission vpl inconsistence' );
}
if ($vpl->is_inconsistent_user( $subinstance->userid, $userid ) && !$vpl->has_capability( VPL_GRADE_CAPABILITY )) {
    // A student can't download another student/group's submission.
    throw new moodle_exception( 'nopermissions', null, null, get_string('download') );
}
$submission = new mod_vpl_submission( $vpl, $subinstance );
$plugincfg = get_config('mod_vpl');
$watermark = isset( $plugincfg->use_watermarks ) && $plugincfg->use_watermarks;
$fgm = $submission->get_submitted_fgm();
$fgm->download_files( strip_tags($vpl->get_instance()->name), $watermark );
die();
