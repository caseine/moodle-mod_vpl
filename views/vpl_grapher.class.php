<?php
// This file is part of VPL for Moodle - http://vpl.dis.ulpgc.es/
//
// VPL for Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// VPL for Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with VPL for Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Graph submissions statistics for a vpl instance and a user
 *
 * @package mod_vpl
 * @copyright 2012 onwards Juan Carlos Rodríguez-del-Pino
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author Juan Carlos Rodríguez-del-Pino <jcrodriguez@dis.ulpgc.es>
 */

defined('MOODLE_INTERNAL') || die();

require_once(dirname(__FILE__).'/../locallib.php');
require_once(dirname(__FILE__).'/../vpl.class.php');
require_once(dirname(__FILE__).'/../vpl_submission.class.php');
require_once(dirname(__FILE__).'/../similarity/diff.class.php');

class vpl_grapher {

    protected $vpl;
    protected $submissions;
    protected $userid;
    protected $username;

    public function __construct(&$vpl, $userid = null) {
        $this->vpl = $vpl;
        $this->userid = $userid;
        if ($userid !== null) {
            global $DB;
            $user = $DB->get_record( 'user', [ 'id' => $userid ]);
            $this->username = $vpl->fullname($user, false);
            $usersubs = $vpl->user_submissions( $userid );
            $this->submissions = $usersubs;
            $this->userssubmissions = [];
            $this->userssubmissions[] = $usersubs;
        } else {
            $this->submissions = [];
            $this->userssubmissions = [];
            $cm = $vpl->get_course_module();
            $currentgroup = groups_get_activity_group( $cm );
            if (! $currentgroup) {
                $currentgroup = '';
            }
            $list = $vpl->get_students( $currentgroup );
            foreach ($list as $userinfo) {
                if ($vpl->is_group_activity() && $userinfo->id != $vpl->get_group_leaderid( $userinfo->id )) {
                    continue;
                }
                $usersubs = $vpl->user_submissions( $userinfo->id );
                $this->submissions = array_merge($this->submissions, $usersubs);
                $this->userssubmissions[] = $usersubs;
            }
        }
    }

    /**
     * Draw a graph in page.
     *
     * @param $title string title of graph
     * @param $xlabel string x label
     * @param $ylabel string y label
     * @param $xdata array x labels
     * @param $ydata array of array of numbers first array is indexed by legend.
     * @param $seriesnames array of strings, values are the names of every series.
     *      If provided, the chart will render as stacked line series. Otherwise, it will render as a bar chart.
     */
    protected static function draw($title, $xlabel, $ylabel, $xdata, $ydata, $seriesnames = null) {
        global $OUTPUT, $CFG;
        require_once($CFG->libdir . '/graphlib.php');
        $chart = new \core\chart_bar();
        $chart->set_title($title);
        $chart->set_labels($xdata);
        $chart->get_xaxis(0, true)->set_label($xlabel);
        $chart->get_yaxis(0, true)->set_label($ylabel);
        $chart->get_xaxis(0, true)->set_labels($xdata);
        if ( $seriesnames === null) {
            $serie = new \core\chart_series($ylabel, $ydata);
            $chart->add_series($serie);
        } else {
            $chart->set_stacked(false);
            foreach ($seriesnames as $seriesname) {
                $serie = new \core\chart_series($seriesname, $ydata[$seriesname]);
                $serie->set_smooth(false);
                $serie->set_type($serie::TYPE_LINE);
                $chart->add_series($serie);
            }
        }
        echo $OUTPUT->render($chart);
    }

    /**
     * Computes a value in {1,5,10,25,50,100,250,500,...} that will render fine as x-axis legend.
     * @param $totalvalues int The number of values on x-axis
     * @param $legendsnumber int The number of legends to display on x-axis
     */
    protected static function compute_xaxis_legend_interval($totalvalues, $legendsnumber) {
        $x = ( int ) ($totalvalues / $legendsnumber); // First estimation.
        if ($x == 0) {
            return 1;
        }
        $interval = 10 ** strlen($x);
        if ($interval / 2 > $x) {
            $interval /= 2;
            if ($interval / 2 > $x) {
                $interval /= 2;
            }
        }
        return max( $interval, 5 );
    }

    /**
     * Draw a graph showing submitted files evolution.
     * @param boolean $diff Whether the graph should show the diff with required files, or the total size of the files.
     */
    public function draw_files_evolution_graph($diff = false) {
        if (count( $this->submissions ) == 0 || $this->userid === null) {
            return;
        }
        try {
            $title = $this->username . ' - ';
            if ($diff) {
                $title .= get_string( 'filesdiffevolution', VPL );
            } else {
                $title .= get_string( 'filessizeevolution', VPL );
            }

            $submissionslist = array_reverse( $this->submissions );
            $interval = self::compute_xaxis_legend_interval(count( $submissionslist ), 20);

            $i = 1;
            $subsn = [];
            $series = [];
            $names = [];
            $totalseries = [];
            if ($diff) {
                raise_memory_limit(MEMORY_EXTRA);
                $reqfiles = $this->vpl->get_fgm('required')->getAllFiles();
                if (optional_param('forcegraphload', false, PARAM_INT)) {
                    $timelimit = 0;
                } else {
                    $timelimit = 500;
                }
            }
            foreach ($submissionslist as $subinstance) {
                $submission = new mod_vpl_submission( $this->vpl, $subinstance );
                $files = $submission->get_submitted_files();
                $total = 0;
                foreach ($files as $name => $data) {
                    if (!isset($series[$name])) {
                        $names[] = $name;
                        $series[$name] = array_fill(0, $i - 1, null);
                    }
                    if ($diff && isset($reqfiles[$name])) {
                        $value = vpl_diff::compute_filediff($reqfiles[$name], $data, $timelimit);
                    } else {
                        $value = strlen( $data );
                    }
                    $series[$name][] = $value;
                    $total += $value;
                }
                foreach ($series as &$serie) {
                    if (count($serie) < $i) {
                        $serie[] = null;
                    }
                }
                $totalseries[] = $total;
                $subsn[] = $i % $interval == 0 ? $i : '';
                $i++;
            }
            if ($diff && count($names) > 1) {
                // Filter out all zero-diff files.
                $names = array_filter($names, function($name) use(&$series) {
                    foreach ($series[$name] as $value) {
                        if ($value != 0) {
                            return true;
                        }
                    }
                    // All diffs are zero for this file - do not include it.
                    unset($series[$name]);
                    return false;
                });

                if (count($names) != 1) {
                    // Add Total series.
                    $names[] = get_string('total');
                    $series[get_string('total')] = $totalseries;
                }
            }
            self::draw( $title, get_string( 'submissions', VPL ) , get_string( "sizeb" ), $subsn, $series, $names );
        } catch (moodle_exception $e) {
            if ($e->errorcode != 'difftoolarge') {
                // The exception was not thrown by diff calculation, don't catch it.
                throw $e;
            }
            global $PAGE;
            echo '<div class="chart-area nograph m-t-3 text-center align-top">';
            echo '<b>' . $title . '</b><br>';
            echo get_string('graphcomputationfailed', VPL) . '<br>';
            echo '<a href="'. $PAGE->url->out(true, [ 'forcegraphload' => 1 ]) .'">' . get_string('forceload', VPL) . '</a>';
            echo '</div>';
        }
    }

    protected static function new_working_period($workduration, &$firstwork, $intervals) {
        if ($intervals > 0) { // First work as average.
            $firstwork = (float) $workduration / $intervals;
        }
        // Else use the last $firstwork.
        return ($workduration + $firstwork);
    }

    protected function get_working_periods() {
        $workperiods = [];
        foreach ($this->userssubmissions as $usersubs) {
            if (count( $usersubs ) == 0) {
                continue;
            }
            $usersubs = array_reverse( $usersubs );
            $userworkperiods = [];
            $lastsavetime = 0;
            $resttime = 20 * 60; // Rest period before next work 20 minutes.
            $firstwork = 10 * 60; // Work before first save 10 minutes.
            $intervals = - 1;
            $workstart = 0;
            foreach ($usersubs as $submission) {
                // Start new work period.
                if ($submission->datesubmitted - $lastsavetime >= $resttime) {
                    if ($workstart > 0) { // Is not the first submission.
                        $userworkperiods[] = self::new_working_period($lastsavetime - $workstart, $firstwork, $intervals);
                    }
                    $workstart = $submission->datesubmitted;
                    $intervals = 0;
                } else { // Count interval.
                    $intervals ++;
                }
                $lastsavetime = $submission->datesubmitted;
            }
            $userworkperiods[] = self::new_working_period($lastsavetime - $workstart, $firstwork, $intervals);
            $workperiods[] = $userworkperiods;
        }
        return $workperiods;
    }

    public function draw_working_periods_graph() {
        if (count( $this->submissions ) == 0) {
            return;
        }
        if ($this->userid === null) {
            $alldata = self::get_working_periods();
            // For every student, total time, number of period.
            $totaltime = 0;
            $maxstudenttime = 0;
            $maxperiodtime = 0;
            $totalperiods = 0;
            $times = [];
            foreach ($alldata as $workingperiods) {
                $totalperiods += count( $workingperiods );
                $time = 0;
                foreach ($workingperiods as $period) {
                    $time += $period / 3600.0;
                    $maxperiodtime = max( $maxperiodtime, $period / 3600.0 );
                }
                $totaltime += $time;
                $maxstudenttime = max( $maxstudenttime, $time );
                $times[] = $time;
            }
            if ($maxstudenttime <= 3) {
                $timeslice = 0.25;
                $xformat = "%3.2f-%3.2f";
            } else if ($maxstudenttime <= 6) {
                $timeslice = 0.50;
                $xformat = "%3.1f-%3.1f";
            } else {
                $timeslice = 1;
                $xformat = "%3.0f-%3.0f";
            }
            $ydata = [];
            $xdata = [];
            for ($slice = 0; $slice <= $maxstudenttime; $slice += $timeslice) {
                $ydata[] = 0;
                $xdata[] = sprintf( $xformat, $slice, ($slice + $timeslice) );
            }
            foreach ($times as $time) {
                $ydata[( int ) ($time / $timeslice)] ++;
            }
            $title = $this->vpl->get_printable_name();
            $n = count( $times );
            $straveragetime = get_string( 'averagetime', VPL, sprintf( '%3.1f', ((float) $totaltime / $n) ) );
            $straverageperiods = get_string( 'averageperiods', VPL, sprintf( '%3.1f', ((float) $totalperiods / $n) ) );
            $strvmaximumperiod = get_string( 'maximumperiod', VPL, sprintf( '%3.1f', ((float) $maxperiodtime) ) );
            $xtitle = sprintf( '%s - %s - %s - %s', get_string( 'hours' ),
                    $straveragetime, $straverageperiods, $strvmaximumperiod );
            $ytitle = get_string( 'defaultcoursestudents' );
            self::draw( $title, $xtitle, $ytitle, $xdata, $ydata );
        } else {
            $workingperiods = self::get_working_periods()[0];
            $xdata = [];
            $totaltime = 0.0;
            $i = 1;
            foreach ($workingperiods as &$period) {
                $xdata[] = '#' . ($i++);
                $totaltime += $period;
                $period = round($period / 60.0, 2);
            }
            $title = $this->username . ' - ' . format_time( $totaltime );
            self::draw( $title, get_string( 'workingperiods', VPL ), get_string( 'minutes' ), $xdata, $workingperiods );
        }
    }

    public function draw_daily_activity_graph() {
        if (count( $this->submissions ) == 0) {
            return;
        }

        // Beginning of time span is either module start date, either first submission date.
        $startdate = $this->vpl->get_effective_setting('startdate', $this->userid);
        if ($startdate > 0) {
            $start = $startdate;
        } else {
            $start = time();
            foreach ($this->submissions as $subinstance) {
                $start = min($start, $subinstance->datesubmitted);
            }
        }
        $start = usergetmidnight($start, get_user_timezone());

        // End of time span is either module due date, either last submission date.
        $duedate = $this->vpl->get_effective_setting('duedate', $this->userid);
        if ($duedate > 0) {
            $end = $duedate;
        } else {
            $end = 0;
            foreach ($this->submissions as $subinstance) {
                $end = max($end, $subinstance->datesubmitted);
            }
        }
        $end = usergetmidnight($end, get_user_timezone());

        // Compute per-day activity (ie. number of submissions).
        $days = [];
        $dailyactivity = [];
        for ($day = $start; $day <= $end; $day += 86400) {
            $days[] = userdate($day, get_string('strftimedate', 'langconfig'));
            $dailyactivity[] = 0;
        }

        foreach ($this->submissions as $subinstance) {
            $daysubmitted = usergetmidnight($subinstance->datesubmitted, get_user_timezone());
            $dayoffset = round(($daysubmitted - $start) / 86400); // Round because DST affects usergetmidnight().
            if (isset($dailyactivity[$dayoffset])) {
                $dailyactivity[$dayoffset] ++;
            }
        }

        // Draw the graph.
        if ($this->userid === null) {
            $title = $this->vpl->get_printable_name();
        } else {
            $title = $this->username;
        }
        $title .= ' - ' . get_string( 'nsubmissions', VPL, count($this->submissions) );
        self::draw( $title, get_string( 'day' ), get_string( 'submissions', VPL ), $days, $dailyactivity );
    }
}
