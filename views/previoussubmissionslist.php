<?php
// This file is part of VPL for Moodle - http://vpl.dis.ulpgc.es/
//
// VPL for Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// VPL for Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with VPL for Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * List previous submissions for a vpl and user
 *
 * @package mod_vpl
 * @copyright 2012 Juan Carlos Rodríguez-del-Pino
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author Juan Carlos Rodríguez-del-Pino <jcrodriguez@dis.ulpgc.es>
 */

require_once(dirname(__FILE__).'/../../../config.php');
require_once(dirname(__FILE__).'/../locallib.php');
require_once(dirname(__FILE__).'/../vpl.class.php');
require_once(dirname(__FILE__).'/../vpl_submission.class.php');
require_once(dirname(__FILE__).'/vpl_grapher.class.php');

global $USER, $OUTPUT, $PAGE, $DB;

require_login();

function vpl_actions_menu($id, $userid, $subid) {
    $menu = new action_menu();
    $linkparms = [ 'id' => $id, 'userid' => $userid, 'submissionid' => $subid ];
    $link = new moodle_url('/mod/vpl/forms/submissionview.php', $linkparms);
    $menu->add( vpl_get_menu_action_link('submissionview', $link) );
    $link = new moodle_url('/mod/vpl/forms/edit.php', array_merge($linkparms, [ 'privatecopy' => 1 ]));
    $menu->add( vpl_get_menu_action_link('copy', $link) );
    return $menu;
}

$id = required_param( 'id', PARAM_INT );
$userid = optional_param( 'userid', $USER->id, PARAM_INT );
$detailed = optional_param( 'detailed', 0, PARAM_INT );
$vpl = new mod_vpl( $id );
if ( $userid == null ) {
    if ( $vpl->has_capability( VPL_GRADE_CAPABILITY ) ) { // TODO add VPL course setting check.
        $userid = $USER->id;
    } else {
        vpl_notice( get_string( 'notavailable' ), 'error');
        die;
    }
}
$PAGE->force_settings_menu();
// TODO add VPL course setting check and user information available.
$vpl->prepare_page( 'views/previoussubmissionslist.php', [
        'id' => $id,
        'userid' => $userid,
] );

if ($USER->id != $userid) {
    $vpl->require_capability(VPL_GRADE_CAPABILITY);
} else {
    $vpl->require_capability(VPL_VIEW_CAPABILITY);
}
\mod_vpl\event\submission_previous_upload_viewed::log( [
        'objectid' => $vpl->get_instance()->id,
        'context' => context_module::instance( $id ),
        'relateduserid' => $userid,
] );
if ($detailed) {
    require_once(dirname(__FILE__).'/../views/sh_factory.class.php');
    vpl_sh_factory::include_js();
}

$vpl->print_header( get_string( 'previoussubmissionslist', VPL ) );
$vpl->print_view_tabs( basename( __FILE__ ) );

$reloadurl = new moodle_url('previoussubmissionslist.php', [
        'id' => $id,
        'userid' => $userid,
]);
if ($vpl->has_capability(VPL_GRADE_CAPABILITY)) {
    if (!optional_param('nograph', 0, PARAM_BOOL)) {
        $reloadurl->param('nograph', 1);
        echo '<div id="mod_vpl-graph-error-message" class="my-3 d-none">' .
                  get_string('reloadwithnographserror', VPL, $reloadurl->out()) . '
              </div>
              <script>
                  setTimeout(() => document.querySelector("table.generaltable") ||
                        document.getElementById("mod_vpl-graph-error-message").classList.remove("d-none"),0);
              </script>';
        echo '<noscript><div>' . html_writer::link($reloadurl, get_string('reloadwithnographsif', VPL)) . '</div></noscript>';
        $grapher = new vpl_grapher($vpl, $userid);
        $grapher->draw_files_evolution_graph();
        $grapher->draw_working_periods_graph();
        $grapher->draw_files_evolution_graph(true);
        $grapher->draw_daily_activity_graph();
    } else {
        echo html_writer::div(html_writer::link($reloadurl, get_string('reloadwithgraphs', VPL)), 'my-3');
    }
}

$table = new html_table();
$table->head = [
        '#',
        get_string( 'datesubmitted', VPL ),
        get_string( 'grade', 'core_grades' ),
        get_string( 'files' ),
        get_string( 'action' ),
];
$table->align = [
        'right',
        'left',
        'left',
        'right',
        'left',
];
$table->nowrap = [
        true,
        true,
        true,
        true,
        true,
        true,
];
$submissionslist = $vpl->user_submissions( $userid );
$submissions = [];
$nsub = count( $submissionslist );
foreach ($submissionslist as $submission) {
    if ($detailed) {
        $href = '#f' . $nsub;
    } else {
        $href = vpl_mod_href( 'forms/submissionview.php', 'id', $id, 'userid', $userid, 'submissionid', $submission->id );
    }
    $text = userdate($submission->datesubmitted);
    $link = '<a href="' . $href . '"';
    if ($submission->grader != 0) {
        // Highlight submission as graded.
        $gradingdetails = new stdClass();
        $gradingdetails->date = userdate($submission->dategraded, get_string('strftimedate', 'langconfig'));
        $grader = $DB->get_record('user', [ 'id' => $submission->grader ]);
        $gradingdetails->gradername = $vpl->fullname($grader, false);

        $text .= ' (' . get_string('gradedonby', VPL, $gradingdetails) . ')';
        $link .= ' style="color: red;"';
    }
    $link .= '>' . $text . '</a>';
    $date = '<a href="' . $link . '">' . userdate( $submission->datesubmitted ) . '</a>';
    $sub = new mod_vpl_submission( $vpl, $submission );
    $grade = $sub->get_grade(true);
    $submissions[] = $sub;
    $actions = vpl_actions_menu($id, $userid, $submission->id);
    $table->data[] = [
            $nsub --,
            $link,
            $grade,
            s( $sub->get_detail() ),
            $OUTPUT->render($actions),
    ];
}

echo html_writer::table( $table );

$urlbase = vpl_mod_href('views/previoussubmissionslist.php', 'id', $id, 'userid', $userid);
$urls = [
        vpl_url_add_param($urlbase, 'detailed', 0),
        vpl_url_add_param($urlbase, 'detailed', 1),
];
echo $OUTPUT->url_select( [
        $urls[0] => get_string( 'detailedless' ),
        $urls[1] => get_string( 'detailedmore' ),
], $urls[$detailed], null );
if ($detailed) {
    $nsub = count( $submissionslist );
    foreach ($submissions as $sub) {
        echo '<hr><h2><a name="f' . $nsub . '"># ' . $nsub . '</a></h2>';
        $nsub --;
        $sub->print_submission();
    }
    vpl_sh_factory::syntaxhighlight();
}
$vpl->print_footer();
