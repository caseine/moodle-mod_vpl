<?php
// This file is part of VPL for Moodle - http://vpl.dis.ulpgc.es/
//
// VPL for Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// VPL for Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with VPL for Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * List student submissions of a VPL instances
 *
 * @package mod_vpl
 * @copyright 2012 onwards Juan Carlos Rodríguez-del-Pino
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author Juan Carlos Rodríguez-del-Pino <jcrodriguez@dis.ulpgc.es>
 */

require_once(dirname( __FILE__ ) . '/../../../config.php');
global $CFG, $USER, $OUTPUT, $PAGE, $DB;
require_once($CFG->dirroot.'/mod/vpl/locallib.php');
require_once($CFG->dirroot.'/mod/vpl/vpl.class.php');
require_once($CFG->dirroot.'/mod/vpl/vpl_submission.class.php');
require_once($CFG->dirroot.'/mod/vpl/vpl_submission_CE.class.php');
require_once($CFG->libdir . '/tablelib.php');

function get_this_page_url($id, $showgrades, $group, $getasstring = false) {
    $url = new moodle_url('/mod/vpl/views/submissionslist.php', [ 'id' => $id ]);
    if ($showgrades) {
        $url->param('showgrades', $showgrades);
    }
    if ($group) {
        $url->param('group', $group);
    }
    if ($getasstring) {
        return $url->out(false);
    } else {
        return $url;
    }
}

function vpl_get_listmenu($id, $showgrades, $group) {
    $menu = new action_menu();
    $url = new moodle_url('/mod/vpl/views/activityworkinggraph.php', [ 'id' => $id ]);
    $menu->add(vpl_get_menu_action_link('submissionscharts', $url));
    if ($showgrades) {
        $url = get_this_page_url($id, 0, $group);
        $menu->add(vpl_get_menu_action_link('submissionslist', $url));
    } else {
        $url = get_this_page_url($id, 1, $group);
        $menu->add(vpl_get_menu_action_link('gradercomments', $url));
    }
    $url = new moodle_url('/mod/vpl/views/downloadallsubmissions.php', [ 'id' => $id ]);
    $menu->add(vpl_get_menu_action_link('downloadsubmissions', $url));
    $url = new moodle_url('/mod/vpl/views/downloadallsubmissions.php', [ 'id' => $id, 'all' => 1 ]);
    $menu->add(vpl_get_menu_action_link('downloadallsubmissions', $url));
    return $menu;
}

require_login();

$id = required_param( 'id', PARAM_INT );
$group = optional_param( 'group', 0, PARAM_INT );
$showgrades = optional_param( 'showgrades', 0, PARAM_INT );
$downloadformat = optional_param('downloadformat', '', PARAM_RAW);
$subselection = vpl_get_set_session_var( 'subselection', 'allsubmissions', 'selection' );

$vpl = new mod_vpl( $id );
$vpl->prepare_page('views/submissionslist.php', [ 'id' => $id ]);

$cm = $vpl->get_course_module();
$vpl->require_capability(VPL_GRADE_CAPABILITY);
\mod_vpl\event\vpl_all_submissions_viewed::log($vpl);

// Create an invisible table for initials control.
// We do not use a flexible table for the real display because we have our own sorting mecanism.
$controltable = new flexible_table('mod_vpl-submissionslist');
$controltable->set_attribute('class', 'd-none');
$controltable->define_columns([ 'fullname' ]);
$controltable->define_headers([ 'fullname' ]);
$controltable->initialbars(true);
$controltable->define_baseurl(get_this_page_url($id, $showgrades, $group));
$controltable->setup();

// Unblock user session.
session_write_close();

raise_memory_limit(MEMORY_EXTRA);

// Find if using variations.
$vplinstance = $vpl->get_instance();
$usevariations = $vplinstance->usevariations;
if ($usevariations) {
    $variations = $DB->get_records(VPL_VARIATIONS, ['vpl' => $vplinstance->id]);
    $usevariations = count($variations) > 0;
}
if ($usevariations) {
    $assignedvariations = $DB->get_records(VPL_ASSIGNED_VARIATIONS, ['vpl' => $vplinstance->id]);
    $uservariation = [];
    foreach ($assignedvariations as $assignedvariation) {
        $uservariation[$assignedvariation->userid] = $variations[$assignedvariation->variation];
    }
}

// Get graders.
$gradeable = $vpl->get_grade() != 0;

// Get students.
$currentgroup = groups_get_activity_group( $cm, true );
if (! $currentgroup) {
    $currentgroup = '';
}
if ($vpl->is_group_activity()) {
    $list = groups_get_all_groups($vpl->get_course()->id, 0, $cm->groupingid);
} else {
    $list = $vpl->get_students( $currentgroup );
}
$submissions = $vpl->all_last_user_submission();
$submissionsnumber = $vpl->get_submissions_number();
$maxgrades = $vpl->get_max_grades();

$popupoptions = [
        'height' => 550,
        'width' => 780,
        'directories' => 0,
        'location' => 0,
        'menubar' => 0,
        'personalbar' => 0,
        'status' => 0,
        'toolbar' => 0,
];

if ($vpl->is_group_activity()) {
    $strname = get_string( 'group' );
} else {
    $strname = vpl_get_name_fields_display();
}
$strsubtime = get_string( 'submittedon', VPL );
$gradenoun = vpl_get_gradenoun_str();
$strgrade = get_string($gradenoun);
if (!$showgrades) {
    $hrefgradedetails = get_this_page_url($id, 1, $group);
    $linkgradedetails = $OUTPUT->action_link( $hrefgradedetails, '', null,
            [ 'title' => get_string( 'gradercomments', VPL ) ],
            new pix_icon('gradercomments', '', 'mod_vpl') );
    $strgrade .= ' ' . $linkgradedetails;
}
$strvariations = get_string ( 'variations', VPL );
$strgrader = get_string( 'grader', VPL );
$strgradedon = get_string( 'gradedon', VPL );
$strmaxgrade = get_string( 'maxgradeobtained', VPL );
$strcomments = get_string( 'gradercomments', VPL );
$hrefnsub = vpl_mod_href( 'views/activityworkinggraph.php', 'id', $id );
$action = new popup_action( 'click', $hrefnsub, 'activityworkinggraph' . $id, $popupoptions );
$linkworkinggraph = $OUTPUT->action_link( $hrefnsub, '', $action, [ 'title' => get_string( 'submissionscharts' , VPL ) ],
        new pix_icon('submissionscharts', '', 'mod_vpl') );
$strsubmissions = get_string( 'submissions', VPL ) . ' ' . $linkworkinggraph;
$strnevaluations = get_string( 'evaluations', VPL );

$isdownloading = ($downloadformat > '');
$downloaddata = [];

$table = new html_table();

$table->head = [
        '<input type="checkbox" name="selectallug">',
        '#',
        $strname,
];

if ($usevariations) {
    $table->head[] = $strvariations;
}

if ($showgrades) {
    $table->head = array_merge($table->head, [
            $strgrade,
            $strcomments,
            $strgrader,
            $strgradedon,
    ]);
} else if ($gradeable) {
    $table->head = array_merge($table->head, [
            $strsubtime,
            $strsubmissions,
            $strnevaluations,
            $strgrade,
            $strmaxgrade,
    ]);
} else {
    $table->head = array_merge($table->head, [
            $strsubtime,
            $strsubmissions,
    ]);
}

$table->head[] = $OUTPUT->render(vpl_get_listmenu($id, $showgrades, $group));

$table->id = 'submissionstable';
$PAGE->requires->js_call_amd('mod_vpl/sorttable', 'makeSortable', [ 'submissionstable', [ 0, 1, -1 ], 0 ]);

$usernumber = 0;
$ngrades = []; // Number of revisions made by teacher.

$userslists = [
        'notexecuted' => [ 'list' => [], 'manual' => [] ],
        'notgraded' => [ 'list' => [], 'manual' => [] ],
        'selected' => [ 'list' => [], 'manual' => [] ],
        'all' => [ 'list' => [], 'manual' => [] ],
];

// Get all information.
foreach ($list as $uginfo) {
    $submission = null;
    $nsubmissions = null;
    $maxgradevalue = null;
    if (! isset( $submissions[$uginfo->id] )) {
        if ($subselection != 'all') {
            continue;
        }
    } else {
        $subinstance = $submissions[$uginfo->id];
        $submission = new mod_vpl_submission_CE( $vpl, $subinstance );
        $subid = $subinstance->id;
        if ($submission->is_graded()) {
            if ($subselection == 'notgraded') {
                continue;
            }
            if ($subselection == 'gradedbyuser' && $subinstance->grader != $USER->id) {
                continue;
            }
        } else {
            $subinstance->grade = null;
            if ($subselection == 'graded' || $subselection == 'gradedbyuser') {
                continue;
            }
        }
        if (isset( $submissionsnumber[$uginfo->id] )) {
            $nsubmissions = $submissionsnumber[$uginfo->id]->submissions;
        }

        if (isset( $maxgrades[$uginfo->id] )) {
            $maxgradevalue = $maxgrades[$uginfo->id]->maxgrade;
        }
    }
    $actions = new action_menu();
    // When group activity => add lastname to groupname for order purpose.
    if ($vpl->is_group_activity()) {
        $gr = $uginfo;
        $users = $vpl->get_group_members($gr->id);
        if ( count($users) == 0 ) {
            continue;
        }
        $user = reset( $users );
        $user->firstname = '';
        $user->lastname = $gr->name;
    } else {
        $user = $uginfo;

        // Filter by initials.
        if (($ifirst = $controltable->get_initial_first()) !== null) {
            if (!preg_match("/^$ifirst/i", $user->firstname)) {
                continue;
            }
        }
        if (($ilast = $controltable->get_initial_last()) !== null) {
            if (!preg_match("/^$ilast/i", $user->lastname)) {
                continue;
            }
        }
    }

    $gradecomments = '';
    $linkparms = [ 'id' => $id, 'userid' => $user->id ];
    $viewlink = new moodle_url('/mod/vpl/forms/submissionview.php', $linkparms);
    $actions->add(vpl_get_menu_action_link('submissionview', $viewlink));
    if ($submission == null) {
        $subtime = $OUTPUT->action_link( $viewlink, get_string( 'nosubmission', VPL ) );
        $prev = '';
        $evals = '';
        $reductionstr = '';
        $grade = '';
        $grader = '';
        $gradedon = '';
        $maxgrade = '';
    } else {
        $subtime = $OUTPUT->action_link( $viewlink, userdate( $subinstance->datesubmitted), null,
                [
                        'title' => get_string('submissionview', VPL),
                        'value' => $subinstance->datesubmitted,
                ] );
        if ($nsubmissions !== null) {
            $prevlink = new moodle_url('/mod/vpl/views/previoussubmissionslist.php', $linkparms);
            $prev = $OUTPUT->action_link( $prevlink, $nsubmissions, null,
                    [
                            'title' => get_string('previoussubmissionslist', VPL),
                    ] );
            $actions->add(vpl_get_menu_action_link('previoussubmissionslist', $prevlink));
        } else {
            $prev = '';
        }
        $evals = $subinstance->nevaluations;

        $reduction = 0;
        $percent = false;
        $submission->grade_reduction($reduction, $percent);
        if ($reduction) {
            if ($percent) {
                $reduction = format_float((( 1 - $reduction ) * 100), 2, true, true) . '%';
            } else {
                $reduction = format_float($reduction, 2, true, true);
            }
            $reductioninfo = strip_tags($submission->reduce_grade_string(false));
            $reductionstr = '-' . $reduction;
            if (!$isdownloading) {
                $reductionstr = '<small class="ml-2" style="cursor:help" title="' . $reductioninfo . '">
                                    (' . $reductionstr . ')
                                </small>';
                $evals = '<span value="' . $evals . '">' . $evals . $reductionstr . '</span>';
            }
        } else {
            $reductionstr = '';
        }

        $subid = $subinstance->id;
        $userslists['all']['list'][] = $user->id;
        $gradevalue = $subinstance->grade;
        $result = $submission->getCE();
        if ($result['executed'] !== 0) {
            $prograde = $submission->proposedGrade( $result['execution'] );
        } else {
            $userslists['notexecuted']['list'][] = $user->id;
            $prograde = '';
        }
        if ($submission->is_graded()) {
            $gradetext = $submission->get_grade(true);
            // Add proposed grade diff.
            if ($prograde > '' && $prograde != $subinstance->grade) {
                $gradetext .= ' (' . $prograde . ')';
            }
            $graderid = $subinstance->grader;
            // Count evaluator marks.
            if (isset( $ngrades[$graderid] )) {
                $ngrades[$graderid] ++;
            } else {
                $ngrades[$graderid] = 1;
            }
            $grader = fullname( mod_vpl_submission::get_grader( $graderid ) );
            if ($graderid != 0) {
                $userslists['all']['manual'][] = $user;
                if ($result['executed'] === 0) {
                    $userslists['notexecuted']['manual'][] = $user;
                }
            }
            if ($showgrades) {
                $gradecomments .= $submission->get_detailed_grade();
                $gradecomments .= $submission->print_ce(true);
            }
        } else {
            $userslists['notgraded']['list'][] = $user->id;
            if ($prograde > '') {
                $gradetext = get_string( 'proposedgrade', VPL, $submission->get_grade_core( $prograde ) );
                $gradevalue = $prograde;
            } else {
                $gradetext = get_string( 'nograde' );
            }
            $grader = '';
        }

        $gradedsubrecord = $vpl->get_effective_submission_for_grade($user->id);
        if ($gradedsubrecord->id != $subinstance->id) {
            $gradedsubmission = new mod_vpl_submission($vpl, $gradedsubrecord);
            $suburl = new moodle_url('/mod/vpl/forms/submissionview.php',
                    [ 'id' => $id, 'userid' => $user->id, 'submissionid' => $gradedsubrecord->id ]);
            $gradetext = $gradedsubmission->get_grade(true) . html_writer::link($suburl,
                    '*',
                    [
                            'class' => 'bold ml-1',
                            'title' => get_string('fromsub', VPL, get_string($vpl->get_instance()->usegradefrom . 'mission', VPL)),
                    ]);
        }

        $gradesuffix = '';
        if ($vpl->get_gradebook_grade(false, $user->id) === null && $gradedsubrecord->id != $subinstance->id) {
            $gradesuffix = ' (' . $gradetext . ')';
            $gradetext = get_string('nograde');
            $gradevalue = 0;
        }

        if ($subinstance->grader == $USER->id || !$submission->is_graded()) {
            $hrefgrade = vpl_mod_href( 'forms/gradesubmission.php', 'id', $id, 'userid', $user->id, 'inpopup', 1 );
            $action = new popup_action( 'click', $hrefgrade, 'gradesub' . $user->id, $popupoptions );
            $grade = $OUTPUT->action_link( $hrefgrade, $gradetext, $action,
                    [
                            'title' => get_string('dograde', VPL),
                    ] );
        } else {
            $grade = $gradetext;
        }

        $grade .= $gradesuffix;
        $grade = '<div id="g' . $subid . '" class="gd' . $subid . '" value="' . $gradevalue . '">' . $grade . '</div>';

        if ($maxgradevalue !== null) {
            $maxgradetext = $vpl->format_grade($maxgradevalue);
            $maxgrade = '<span value="'.$maxgradevalue.'">'.$maxgradetext.'</span>';
        } else {
            $maxgrade = '';
        }

        $link = new moodle_url('/mod/vpl/forms/gradesubmission.php', $linkparms);
        $actions->add(vpl_get_menu_action_link('dograde', $link));
        // Add div id to submission info.
        $grader = '<div id="m' . $subid . '" class="gd' . $subid . '">' . $grader . '</div>';
        $gradedon = '<div id="o' . $subid . '" class="gd' . $subid . '">'.
                        $submission->get_gradedon_date_formatted().
                    '</div>';
    }

    if ($usevariations && isset($uservariation[$uginfo->id])) {
        $variationname = $uservariation[$uginfo->id]->identification;
    } else {
        $variationname = '';
    }

    if (isset($subid)) {
        $gradecomments = '<div id="c' . $subid . '" class="gd' . $subid . '">' . $gradecomments . '</div>';
    }

    $copylink = new moodle_url('/mod/vpl/forms/edit.php', [ 'id' => $id, 'userid' => $user->id, 'privatecopy' => 1 ]);
    $action = new popup_action( 'click', $copylink, 'privatecopyl' . $id, $popupoptions );
    $usernumber ++;
    $usernumberlink = $OUTPUT->action_link( $copylink, $usernumber, $action,
            [
                    'title' => get_string('copy', VPL),
            ] );
    $actions->add(vpl_get_menu_action_link('copy', $copylink));

    $submissiondata = [
            '<input type="checkbox" name="selectug' . $user->id . '">',
            $usernumberlink,
            '<span value="'.$user->lastname.'">' . $vpl->user_fullname_picture($user, false) . '</span>',
    ];

    if ($usevariations) {
        $submissiondata[] = $variationname;
    }

    if ($showgrades) {
        $submissiondata = array_merge($submissiondata, [
                $grade,
                $gradecomments,
                $grader,
                $gradedon,
        ]);
    } else if ($gradeable) {
        $submissiondata = array_merge($submissiondata, [
                $subtime,
                $prev,
                $evals,
                $grade,
                $maxgrade,
        ]);
    } else {
        $submissiondata = array_merge($submissiondata, [
                $subtime,
                $prev,
        ]);
    }

    $table->data[] = array_merge($submissiondata, [
            $OUTPUT->render($actions),
    ]);

    $downloaddata[] = [
            $vpl->fullname($user, false),
            $variationname,
            strip_links($subtime),
            strip_links($grade),
            $gradecomments,
            $grader,
            $gradedon,
            strip_links($prev),
            $evals,
            $reductionstr,
            $maxgrade,
    ];
}

if ($isdownloading) {
    // This is a request to download the table.
    confirm_sesskey();

    $headers = [
            $strname,
            $strvariations,
            $strsubtime,
            $strgrade,
            $strcomments,
            $strgrader,
            $strgradedon,
            $strsubmissions,
            $strnevaluations,
            get_string('finalreduction', VPL),
            $strmaxgrade
    ];

    $file = $CFG->dirroot . '/dataformat/' . $downloadformat . '/classes/writer.php';
    if (is_readable($file)) {
        include_once($file);
    }
    $writerclass = 'dataformat_' . $downloadformat. '\writer';
    if (!class_exists($writerclass)) {
        throw new moodle_exception('invalidparameter', 'debug');
    }

    $writer = new $writerclass();

    $writer->set_filename(clean_filename('mod_vpl_sumbissionslist_export_' . format_string($vpl->get_instance()->name)));
    $writer->send_http_headers();
    $writer->set_sheettitle('submissionslist-' . format_string($vpl->get_instance()->name));
    $writer->start_output();

    $writer->start_sheet($headers);

    foreach ($downloaddata as $rownum => $row) {
        $writer->write_record($row, $rownum + 1);
    }

    $writer->close_sheet($headers);

    $writer->close_output();
    exit();
}


if (count( $ngrades )) {
    $tablegraders = new html_table();
    $tablegraders->head = ['#', $strgrader, get_string($gradenoun)];
    $tablegraders->align = ['right', 'left', 'center'];
    $tablegraders->wrap = ['nowrap', 'nowrap', 'nowrap'];
    $tablegraders->data = [];
    $gradernumber = 0;
    foreach ($ngrades as $graderid => $marks) {
        $gradernumber ++;
        $grader = mod_vpl_submission::get_grader($graderid);
        $picture = '';
        if ($graderid > 0) { // No automatic grading.
            $picture = $OUTPUT->user_picture( $grader );
        }
        $tablegraders->data[] = [
                $gradernumber,
                $picture . ' ' . fullname( $grader ),
                sprintf( '%d/%d  (%.2f%%)', $marks, $usernumber, ( float ) 100.0 * $marks / $usernumber ),
        ];
    }
}


// Print header.
$vpl->print_header( get_string( 'submissionslist', VPL ) );
$vpl->print_view_tabs( basename( __FILE__ ) );
@ob_flush();
flush();

// Find out current groups mode.
// Menu for groups.
if ( groups_get_activity_groupmode( $cm ) || groups_get_course_groupmode( $vpl->get_course() ) ) {
    $groupsurl = get_this_page_url($id, $showgrades, 0);
    groups_print_activity_menu( $cm, $groupsurl );
}
// Print user selection by submission state.
$urlbase = get_this_page_url($id, $showgrades, $group, true) . '&selection=';
$urls = [];
$urls[$urlbase . 'all'] = get_string('all');
foreach ([ 'allsubmissions', 'notgraded', 'graded', 'gradedbyuser' ] as $value) {
    $urls[$urlbase . $value] = get_string( $value, VPL );
}
$urlsel = new url_select( $urls, $urlbase . $subselection, null );
$urlsel->set_label( get_string( 'submissionselection', VPL ) );
echo $OUTPUT->render( $urlsel );

// Print evaluation buttons.
if ($gradeable || $vpl->get_instance()->evaluate) {
    $returnurl = get_this_page_url($id, $showgrades, $group, true);
    $baseurl = vpl_mod_href('forms/evaluation.php', 'id', $id, 'returnurl', $returnurl);
    echo '<br>';
    echo '<span>' . get_string( 'evaluate', VPL ) . ' </span>';
    $stringmanager = get_string_manager();
    foreach ($userslists as $type => $userslist) {
        if ($stringmanager->string_exists($type, VPL)) {
            $label = get_string($type, VPL);
        } else {
            $label = get_string($type);
        }
        $label .= ' (' . count($userslist['list']) . ')';
        $url = vpl_url_add_param($baseurl, 'userid', reset($userslist['list']));
        $url = vpl_url_add_param($url, 'userlist', implode(',', $userslist['list']));
        $button = new single_button(new moodle_url($url), $label, 'get');
        $button->disabled = empty($userslist['list']);
        $button->formid = 'evaluate' . $type . 'form';
        if (!empty($userslist['manual']) || ($type == 'selected' && !empty($userslists['all']['manual']))) {
            if ($type == 'selected') {
                $message = get_string('confirmevaluateselected', VPL);
            } else {
                $names = [];
                foreach ($userslist['manual'] as $user) {
                    $names[] = '&bull;&nbsp;' . $vpl->fullname($user, false);
                }
                $message = get_string('confirmevaluate', VPL, implode('<br>', $names));
            }
            $button->add_confirm_action($message);
        }
        echo $OUTPUT->render($button);
    }
    echo '<br>';
    $PAGE->requires->js_call_amd('mod_vpl/submissionslist', 'setupEvaluateSelectedButton');
}
echo '<br>';
@ob_flush();
flush();

if (!$vpl->is_group_activity()) {
    $controltable->add_data([ 'dummy' ]);
    $controltable->finish_output();
}

echo html_writer::table( $table );
echo $OUTPUT->download_dataformat_selector(
        get_string('downloadas', 'table'),
        $PAGE->url->out_omit_querystring(),
        'downloadformat',
        $PAGE->url->params()
);
if (count( $ngrades ) > 0) {
    echo '<br>';
    echo html_writer::table( $tablegraders );
}
$vpl->print_footer();
