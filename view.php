<?php
// This file is part of VPL - http://vpl.dis.ulpgc.es/
//
// VPL is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// VPL is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with VPL.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package   VPL. Show a VPL instance
 * @copyright 2012 onwards Juan Carlos Rodríguez-del-Pino
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author    Juan Carlos Rodríguez-del-Pino <jcrodriguez@dis.ulpgc.es>
 */

require_once(dirname(__FILE__).'/../../config.php');
require_once(dirname(__FILE__).'/locallib.php');
require_once(dirname(__FILE__).'/vpl.class.php');

global $USER, $OUTPUT;

require_login();
$id = required_param('id', PARAM_INT); // Course Module ID.
$vpl = new mod_vpl( $id );
$urlparms = [ 'id' => $id ];
if ( optional_param( 'userid', -1, PARAM_INT ) != -1 ) {
    $urlparms['userid'] = required_param( 'userid', PARAM_INT );
}
$vpl->prepare_page( 'view.php', $urlparms );
$vpl->require_capability( VPL_VIEW_CAPABILITY );
$id = $vpl->get_course_module()->id;

if (! $vpl->is_visible()) {
    vpl_redirect( '?id=' . $id, get_string( 'notavailable' ) );
    die;
}
if (! $vpl->has_capability( VPL_MANAGE_CAPABILITY ) && ! $vpl->has_capability( VPL_GRADE_CAPABILITY )) {
    $vpl->restrictions_check();
    $userid = $USER->id;
} else {
    $userid = optional_param( 'userid', $USER->id, PARAM_INT );
}

\mod_vpl\event\vpl_description_viewed::log( $vpl );

// Prepares showing required and execution files.
$fr = $vpl->get_fgm('required');
$showfr = $fr->is_populated();
$fe = $vpl->get_fgm('execution');
$showfe = $vpl->has_capability( VPL_GRADE_CAPABILITY ) && $fe->is_populated();

if ( $showfr || $showfe ) {
    require_once(dirname(__FILE__).'/views/sh_factory.class.php');
    $filelist = ($showfr ? $fr->getfilelist() : []) + ($showfe ? $fe->getfilelist() : []);
    foreach ($filelist as $filename) {
        if (vpl_is_notebook($filename)) {
            vpl_sh_factory::include_jsnotebook_highlighter();
            break;
        }
    }
    vpl_sh_factory::include_js();
}

// Print the page header.
$vpl->print_header( get_string( 'description', VPL ) );

// Print the main part of the page.
$vpl->print_view_tabs( basename( __FILE__ ) );
$vpl->print_name();

echo $OUTPUT->box_start('generalbox no-overflow');

echo '<div class="float-right border p-3" style="max-width:50%">';
echo '<h2>' . get_string( 'workstatesummary', VPL ) . '</h2>';
$nsubmissions = count($vpl->user_submissions( $userid ));
echo '<div>' . $vpl->str_restriction('submissions', $nsubmissions) . '</div>';
$lastsub = $vpl->last_user_submission( $userid );
if ($lastsub !== false) {
    echo $vpl->str_restriction('lastsubmission', '<br>');
    echo '<div class="pl-3">';
    require_once(__DIR__ . '/vpl_submission.class.php');
    $submission = new mod_vpl_submission( $vpl, $lastsub );
    $submission->print_info();
    $vpl->print_current_effective_grade(false, $userid);
    \mod_vpl\event\submission_grade_viewed::log($submission);
    $subviewurl = vpl_mod_href('forms/submissionview.php', 'id', $id, 'userid', $userid);
    echo '<a href="' . $subviewurl . '">' . get_string('details', VPL) . '</a>';
    echo '</div>';
} else {
    echo $vpl->str_restriction('lastsubmission', get_string('none'));
}
echo '</div>';

$corfgm = $vpl->get_fgm('corrected');
if (mod_vpl\correctedfiles\availability::can_see($vpl, $userid) && $corfgm->is_modified_from($vpl->get_fgm('required'))) {
    $correctedfiles = $corfgm->getfilelist();
    $url = new moodle_url( '/mod/vpl/views/downloadfiles.php', [ 'id' => $id, 'type' => 'corrected' ]);
    $link = ' (' . vpl_get_awesome_icon('download') . html_writer::link($url, get_string( 'download', VPL )) . ')';
    echo $vpl->str_restriction_with_icon( 'correctedfiles', s(implode(', ', $correctedfiles)) . $link ) . '<br>';
}
$vpl->print_submission_period( $userid );
$vpl->print_submission_restriction( $userid );
$vpl->print_variation( $userid );
$vpl->print_fulldescription();


if ( $showfr ) {
    echo '<h2 style="clear:both">' . get_string( 'requiredfiles', VPL ) . "</h2>\n";
    $fr->print_files( false );
}
if ( $showfe ) {
    echo '<h2 style="clear:both">' . get_string( 'executionfiles', VPL ) . "</h2>\n";
    $fe->print_files( false );
}

echo $OUTPUT->box_end();

if (mod_vpl\webservice\manager::service_is_available()) {
    echo html_writer::link('/mod/vpl/views/show_webservice.php?id=' . $id, get_string('webservice', VPL));
}

$vpl->print_footer();
vpl_sh_factory::syntaxhighlight();
