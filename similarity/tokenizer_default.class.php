<?php
// This file is part of VPL for Moodle - http://vpl.dis.ulpgc.es/
//
// VPL for Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// VPL for Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with VPL for Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Default tokenizer class
 *
 * @package mod_vpl
 * @copyright 2023 Astor Bizard, 2012 Juan Carlos Rodríguez-del-Pino
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author Juan Carlos Rodríguez-del-Pino <jcrodriguez@dis.ulpgc.es>
 */

defined('MOODLE_INTERNAL') || die();

require_once(dirname(__FILE__).'/tokenizer_base.class.php');

class vpl_tokenizer_default extends vpl_tokenizer_base {
    const REGULAR = 0;
    const IN_NUMBER = 1;
    protected $linenumber;
    protected $tokens;
    protected function is_number($text) {
        if (strlen( $text ) == 0) {
            return false;
        }
        $first = $text[0];
        return $first >= '0' && $first <= '9';
    }
    protected function add_pending(&$rawpending) {
        $pending = strtolower( $rawpending );
        if (strlen(trim($pending)) == 0) {
            return;
        }
        if (strpos( "&'\"()*+,-./:;<=>|{}[]", $pending ) !== false) {
            $type = vpl_token_type::OPERATOR;
        } else if ($this->is_number( $pending )) {
            $type = vpl_token_type::LITERAL;
        } else {
            $type = vpl_token_type::IDENTIFIER;
        }
        $this->tokens [] = new vpl_token( $type, $pending, $this->linenumber );
        $rawpending = '';
    }
    public function parse($filedata) {
        $this->tokens = [];
        $this->linenumber = 1;
        $state = self::REGULAR;
        $pending = '';
        $l = strlen( $filedata );
        $current = '';
        $previous = '';
        for ($i = 0; $i < $l; $i ++) {
            $previous = $current;
            $current = $filedata [$i];
            if ($i < ($l - 1)) {
                $next = $filedata [$i + 1];
            } else {
                $next = '';
            }
            if ($previous == self::LF) {
                $this->linenumber ++;
            }
            if ($current == self::CR) {
                if ($next == self::LF) {
                    continue;
                } else {
                    $this->linenumber ++;
                    $current = self::LF;
                }
            }
            switch ($state) {
                case self::IN_NUMBER :
                    if (($current >= '0' && $current <= '9') || $current == '.' || $current == '_' || $current == 'e' || $current == 'e') {
                        $pending .= $current;
                        break;
                    }
                    if (($current == '-' || $current == '+') && ($previous == 'E' || $previous == 'e')) {
                        $pending .= $current;
                        break;
                    }
                    $this->add_pending( $pending );
                    $state = self::REGULAR;
                    // Process current as REGULAR.
                case self::REGULAR :
                    if (strpos( " \n\r\t\v\f", $current ) !== false) { // A separator.
                        $this->add_pending( $pending );
                        break;
                    } else if (strpos( "&'\"()*+,-./:;<=>|{}[]", $current ) !== false) { // A delimiter.
                        $this->add_pending( $pending );
                        $this->add_pending( $current );
                        break;
                    } else if ($current >= '0' && $current <= '9') { // Start of number.
                        $state = self::IN_NUMBER;
                        $this->add_pending( $pending );
                        $pending .= $current;
                        break;
                    } else {
                        $pending .= $current;
                        break;
                    }
            }
        }
        $this->add_pending( $pending );
    }
    public function get_tokens() {
        return $this->tokens;
    }
    public function show_tokens() {
        foreach ($this->tokens as $token) {
            $token->show();
        }
    }
}
