<?php
// This file is part of VPL for Moodle - http://vpl.dis.ulpgc.es/
//
// VPL for Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// VPL for Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with VPL for Moodle.  If not, see <http://www.gnu.org/licenses/>.

define( 'AJAX_SCRIPT', true );

require_once(dirname(__FILE__).'/../../../config.php');

$outcome = new stdClass();
$outcome->success = true;
$outcome->response = new stdClass();
$outcome->error = '';
try {
    require_once(dirname(__FILE__).'/../locallib.php');
    require_once(dirname(__FILE__).'/../vpl.class.php');
    require_once(dirname(__FILE__).'/diff.class.php');

    require_login();
    $id = required_param('id', PARAM_INT); // VPL ID.
    $texttocheck = required_param('val', PARAM_RAW);
    $filename = required_param('name', PARAM_RAW);

    $vpl = new mod_vpl( $id );
    $reqfiles = $vpl->get_fgm('required')->getAllFiles();

    if (isset($reqfiles[$filename])) {
        $reqfiletext = $reqfiles[$filename];
        $outcome->response->diff = vpl_diff::calculatediff( explode("\n", $reqfiletext) , explode("\n", $texttocheck) , false );
    } else {
        $outcome->response->diff = [];
    }
} catch ( Exception $e ) {
    $outcome->success = false;
    $outcome->error = $e->getMessage();
}

echo json_encode( $outcome );
die();
