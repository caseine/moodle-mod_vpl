<?php
// This file is part of VPL for Moodle - http://vpl.dis.ulpgc.es/
//
// VPL for Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// VPL for Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with VPL for Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Class to show two files diff
 *
 * @package mod_vpl
 * @copyright 2012 Juan Carlos Rodríguez-del-Pino
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author Juan Carlos Rodríguez-del-Pino <jcrodriguez@dis.ulpgc.es>
 */

defined('MOODLE_INTERNAL') || die();

require_once(dirname(__FILE__).'/../locallib.php');
require_once(dirname(__FILE__).'/../vpl.class.php');
require_once(dirname(__FILE__).'/../vpl_submission.class.php');
require_once(dirname(__FILE__).'/similarity_factory.class.php');
require_once(dirname(__FILE__).'/similarity_sources.class.php');

class vpl_diff {

    /**
     * Calculate the similarity of two lines
     *
     * @param string $line1
     * @param string $line2
     * @return int (3 => trimmed equal, 2 =>removealphanum , 1 => start of line , 0 => not equal)
     */
    public static function diffline($line1, $line2) {
        $line1 = trim( $line1 );
        $line2 = trim( $line2 );
        if ($line1 == $line2) {
            return 3;
        }
        if (self::similine($line1, $line2, '/(\s|[0-9]|[a-z])/i')) {
            return 2;
        }
        if (substr($line1, 0, 4) == substr($line2, 0, 4)) {
            return 1;
        }
        return 0;
    }

    public static function newlineinfo($type, $ln1, $ln2 = 0) {
        $ret = new StdClass();
        $ret->type = $type;
        $ret->ln1 = $ln1;
        $ret->ln2 = $ln2;
        return $ret;
    }

    // Due to PHP arrays architecture, the calculatediff function consumes a lot of memory.
    // To reduce that memory consumption, we put information on the "prev" on the 29th and 30th bits of integers.
    private static $prevx = 2 ** 29;
    private static $prevy = 2 ** 30;
    private static function truevalue($value) {
        return $value & ~(self::$prevx + self::$prevy);
    }

    /**
     * Initialize used matrix
     *
     * @param array $matrix  of arrays to initialize
     * @param $nl1 number of rows
     * @param $nl2 number of columns
     * @return void
     */
    public static function initauxiliarmatrices(&$matrix, $nl1, $nl2) {
        // Set the matrix[0..nl1+1][0..nl2+1] to 0.
        for ($i = 0; $i <= $nl1; $i ++) {
            $matrix[$i] = new SplFixedArray($nl2 + 1);
            for ($j = 0; $j <= $nl2; $j ++) {
                $matrix[$i][$j] = 0;
            }
            // Set prev for first column.
            $matrix[$i][0] = self::$prevx;
        }
        // Set prev for first row.
        for ($j = 0; $j <= $nl2; $j ++) {
            $matrix[0][$j] = self::$prevy;
        }
    }

    public static function similine($line1, $line2, $pattern) {
        return preg_replace($pattern, '', $line1) == preg_replace($pattern, '', $line2);
    }

    /**
     * Calculate diff for two array of lines
     *
     * @param $lines1 array
     *            of string
     * @param $lines2 array
     *            of string
     * @param $detaileddiff bool
     *            whether different levels of similarity should be distinguished
     * @return array of objects with info to show the two array of lines
     */
    public static function calculatediff($lines1, $lines2, $detaileddiff = true) {
        $nmatrixlimit = 1000 * 1000; // Matrix size limit.
        $ret = [];
        $nl1 = count( $lines1 );
        $nl2 = count( $lines2 );
        if ($nl1 == 0 && $nl2 == 0) {
            return false;
        }
        if ($nl1 == 0) { // There is no first file.
            foreach ($lines2 as $pos => $line) {
                $ret[] = self::newlineinfo( '>', 0, $pos + 1 );
            }
            return $ret;
        }
        if ($nl2 == 0) { // There is no second file.
            foreach ($lines1 as $pos => $line) {
                $ret[] = self::newlineinfo( '<', $pos + 1 );
            }
            return $ret;
        }

        if ($nl1 * $nl2 > $nmatrixlimit) {
            $mnl = max($nl1, $nl2);
            for ($i = 1; $i <= $mnl; $i++) {
                $l1 = $i > $nl1 ? 0 : $i;
                $l2 = $i > $nl2 ? 0 : $i;
                $ret[] = self::newlineinfo( '#', $l1, $l2);
            }
            return $ret;
        }

        $matrix = new SplFixedArray($nl1 + 1);
        self::initauxiliarmatrices( $matrix, $nl1, $nl2 );

        // Matrix processing.
        for ($i = 1; $i <= $nl1; $i ++) {
            $line = $lines1[$i - 1];
            for ($j = 1; $j <= $nl2; $j ++) {
                if (self::truevalue($matrix[$i][$j - 1]) > self::truevalue($matrix[$i - 1][$j])) {
                    $max = self::truevalue($matrix[$i][$j - 1]);
                    $best = self::$prevy;
                } else {
                    $max = self::truevalue($matrix[$i - 1][$j]);
                    $best = self::$prevx;
                }
                if ($detaileddiff) {
                    $prize = self::diffline( $line, $lines2[$j - 1] );
                } else {
                    $prize = $line === $lines2[$j - 1] ? 1 : 0;
                }
                if (self::truevalue($matrix[$i - 1][$j - 1]) + $prize >= $max) {
                    $max = self::truevalue($matrix[$i - 1][$j - 1]) + $prize;
                    $best = 0;
                }
                $matrix[$i][$j] = $max + $best;
            }
        }

        // Calculate show info.
        // Backtrack matrix following the prevx/y bits.
        $limit = $nl1 + $nl2;
        $pairs = [];
        $pi = $nl1;
        $pj = $nl2;
        while ( (! ($pi == 0 && $pj == 0)) && $limit > 0 ) {
            $pair = new stdClass();
            $pair->i = $pi;
            $pair->j = $pj;
            $pairs[] = $pair;
            if (($matrix[$pi][$pj] & self::$prevy) > 0) {
                $pj --;
            } else if (($matrix[$pi][$pj] & self::$prevx) > 0) {
                $pi --;
            } else {
                $pi --;
                $pj --;
            }
            $limit --;
        }

        krsort( $pairs );
        $prevpair = new stdClass();
        $prevpair->i = 0;
        $prevpair->j = 0;
        foreach ($pairs as $pair) {
            if ($pair->i == $prevpair->i + 1 && $pair->j == $prevpair->j + 1) { // Regular advance.
                $l1 = $lines1[$pair->i - 1];
                $l2 = $lines2[$pair->j - 1];
                if ($l1 == $l2) {
                    // Equals.
                    $ret[] = self::newlineinfo( '=', $pair->i, $pair->j );
                } else if ( self::similine($l1, $l2, '/\s/')) {
                    // Equal once whitespaces have been removed.
                    $ret[] = self::newlineinfo( '1', $pair->i, $pair->j );
                } else if ( self::similine($l1, $l2, '/(\s|[0-9]|[a-z])/i')) {
                    // Equal once whitespaces and alphanumeric characters have been removed.
                    $ret[] = self::newlineinfo( '2', $pair->i, $pair->j );
                } else {
                    // Different even once whitespaces and alphanumeric characters have been removed.
                    $ret[] = self::newlineinfo( '#', $pair->i, $pair->j );
                }
            } else if ($pair->i == $prevpair->i + 1) { // Removed next line.
                $ret[] = self::newlineinfo( '<', $pair->i, false );
            } else if ($pair->j == $prevpair->j + 1) { // Added one line.
                $ret[] = self::newlineinfo( '>', false, $pair->j );
            } else {
                debugging( "Internal error " . s( $pair ) . " " . s( $prevpair) );
            }
            $prevpair = $pair;
        }
        return $ret;
    }
    public static function show($filename1, $data1, $htmlheader1, $filename2, $data2, $htmlheader2) {
        // Get file lines.
        $nl = vpl_detect_newline( $data1 );
        $lines1 = explode( $nl, $data1 );
        $nl = vpl_detect_newline( $data2 );
        $lines2 = explode( $nl, $data2 );
        // Get diff as an array of info.
        $diff = self::calculatediff( $lines1, $lines2 );
        if ($diff === false) {
            return;
        }
        $separator = [
                '<' => ' <<< ',
                '>' => ' >>> ',
                '=' => ' === ',
                '1' => ' ==# ',
                '2' => ' =## ',
                '#' => ' ### ',
        ];
        $emptyline = "\n";
        $data1 = $emptyline;
        $data2 = $emptyline;
        $datal1 = $emptyline;
        $datal2 = $emptyline;
        $diffl = '';
        $lines1[-1] = '';
        $lines2[-1] = '';
        foreach ($diff as $line) {
            $diffl .= $separator[$line->type] . "\n";
            if ($line->ln1) {
                $datal1 .= sprintf("%4d\n", $line->ln1);
                $data1 .= $lines1[$line->ln1 - 1] . "\n";
            } else {
                $data1 .= $emptyline;
                $datal1 .= $emptyline;
            }
            if ($line->ln2) {
                $datal2 .= sprintf("%4d\n", $line->ln2);
                $data2 .= $lines2[$line->ln2 - 1] . "\n";
            } else {
                $data2 .= $emptyline;
                $datal2 .= $emptyline;
            }
        }
        echo '<div class="sim_details text-nowrap">';

        // Header.
        echo html_writer::div('', 'sim-col d-inline-block');
        echo html_writer::div($htmlheader1, 'sim-file d-inline-block');
        echo html_writer::div('', 'sim-col d-inline-block');
        echo html_writer::div('', 'sim-col d-inline-block');
        echo html_writer::div($htmlheader2, 'sim-file d-inline-block');
        echo '<div style="clear:both;"></div>';

        // Files.
        $simpledisplayer = vpl_sh_factory::get_sh( '' );
        $nl = count($diff) + 1;
        echo '<div class="sim-col sim-gutter d-inline-block align-top">';
        $simpledisplayer->print_file( '', $datal1, null, false, $nl, false );
        echo '</div>';
        echo '<div class="sim-file d-inline-block align-top">';
        $shower = vpl_sh_factory::get_sh( $filename1 );
        $shower->print_file( $filename1, $data1, null, false, $nl, false );
        echo '</div>';
        echo '<div class="sim-col d-inline-block align-top">';
        $simpledisplayer->print_file( '', $diffl, null, false, $nl, false );
        echo '</div>';
        echo '<div class="sim-col sim-gutter d-inline-block align-top">';
        $simpledisplayer->print_file( '', $datal2, null, false, $nl, false );
        echo '</div>';
        echo '<div class="sim-file d-inline-block align-top">';
        $shower = vpl_sh_factory::get_sh( $filename2 );
        $shower->print_file( $filename2, $data2, null, false, $nl, false );
        echo '</div>';

        echo '</div>';
        vpl_sh_factory::syntaxhighlight();
    }
    public static function vpl_get_similfile($f, &$htmlheader, &$filename, &$data) {
        global $DB;
        $htmlheader = '';
        $filename = '';
        $data = '';
        $type = required_param( 'type' . $f, PARAM_INT );
        if ($type == 1) {
            $subid = required_param( 'subid' . $f, PARAM_INT );
            $filename = required_param( 'filename' . $f, PARAM_TEXT );
            $subinstance = $DB->get_record( 'vpl_submissions', [
                    'id' => $subid,
            ] );
            if ($subinstance !== false) {
                $vpl = new mod_vpl( false, $subinstance->vpl );
                $vpl->require_capability( VPL_SIMILARITY_CAPABILITY );
                $submission = new mod_vpl_submission( $vpl, $subinstance );
                $user = $DB->get_record( 'user', [
                        'id' => $subinstance->userid,
                ] );
                if ($user) {
                    $link = vpl_mod_href( '/forms/submissionview.php', 'id', $vpl->get_course_module()->id
                                          , 'userid', $subinstance->userid );
                    $htmlheader .= '<a href="' . $link . '">';
                }
                $htmlheader .= s( $filename ) . ' ';
                if ($user) {
                    $htmlheader .= '</a>';
                    $htmlheader .= $vpl->user_fullname_picture( $user );
                }
                $fg = $submission->get_submitted_fgm();
                $data = $fg->getFileData( $filename );
                \mod_vpl\event\vpl_diff_viewed::log( $submission );
            }
        } else if ($type == 3) {
            $data = '';
            $vplid = required_param( 'vplid' . $f, PARAM_INT );
            $vpl = new mod_vpl( false, $vplid );
            $vpl->require_capability( VPL_SIMILARITY_CAPABILITY );
            $zipname = required_param( 'zipfile' . $f, PARAM_RAW );
            $filename = required_param( 'filename' . $f, PARAM_RAW );
            $htmlheader .= $filename . ' ' . optional_param( 'username' . $f, '', PARAM_TEXT );
            $ext = strtoupper( pathinfo( $zipname, PATHINFO_EXTENSION ) );
            if ($ext != 'ZIP') {
                throw new moodle_exception( 'wrongzipfilename' );
            }
            $zip = new ZipArchive();
            $zipfilename = vpl_similarity_preprocess::get_zip_filepath( $vplid, $zipname );
            if ($zip->open( $zipfilename ) === true) {
                $data = $zip->getFromName( $filename );
                $zip->close();
            }
        } else {
            throw new moodle_exception('invalidnum');;
        }
    }

    /**
     * Computes diff between two files.
     * @param string $filedata1
     * @param string $filedata2
     * @param int $timelimit The maximum amount of time to spend on this computation (in milliseconds).
     *  If provided and the time limit is exceeded, a moodle_exception with code 'difftoolarge' will be thrown.
     * @return int The diff in number of chars.
     */
    public static function compute_filediff($filedata1, $filedata2, $timelimit=0) {
        if (strlen($filedata1) == 0 || strlen($filedata2) == 0) {
            return strlen($filedata1) + strlen($filedata2);
        }
        $lines1 = explode("\n", $filedata1);
        $lines2 = explode("\n", $filedata2);
        $prev = self::compute_diff($lines1, $lines2, $timelimit)->prev;
        // Backtrack to compute total diff as a sum of lines diff.
        $totaldiff = 0;
        $i1 = count($lines1);
        $i2 = count($lines2);
        while ($i1 > 0 || $i2 > 0) {
            if (!isset($prev[$i1][$i2])) {
                $i1--;
                $i2--;
            } else {
                switch ($prev[$i1][$i2]) {
                    case 1 :
                        $totaldiff += strlen( $lines1[$i1 - 1] ) + 1;
                        $i1--;
                        break;
                    case 2 :
                        $totaldiff += strlen( $lines2[$i2 - 1] ) + 1;
                        $i2--;
                        break;
                    case 3 :
                        $totaldiff += self::compute_diff($lines1[$i1 - 1], $lines2[$i2 - 1])->diff;
                    case 0 :
                        $i1--;
                        $i2--;
                        break;
                }
            }
        }
        return $totaldiff;
    }

    /**
     * Computes diff between two arrays or strings, by equality comparison between elements at corresponding positions.
     * @param string|array $array1
     * @param string|array $array2
     * @param int $timelimit The maximum amount of time to spend on this computation (in milliseconds).
     *  If provided and the time limit is exceeded, a moodle_exception with code 'difftoolarge' will be thrown.
     * @return stdClass An object with a 'diff' field containing the diff in number of elements
     *  and a 'prev' field containing a 2-dimensional array of diff data, indexed on [array1][array2] :
     *  0 means identical elements, 1 means element addition in array1,
     *  2 means element addition in array2, 3 means element change.
     */
    public static function compute_diff($array1, $array2, $timelimit = 0) {
        if (is_string($array1)) {
            $n1 = strlen($array1);
            $n2 = strlen($array2);
        } else {
            $n1 = count($array1);
            $n2 = count($array2);
        }
        if ($n1 == 0 || $n2 == 0) {
            $result = new stdClass();
            $result->diff = $n1 + $n2;
            $result->prev = null;
            return $result;
        }
        $queue = array_fill(0, $n1 + $n2 + 1, []);
        $prev = array_fill(0, $n1 + 1, []);
        $z = new stdClass();
        $z->i1 = 0;
        $z->i2 = 0;
        $z->d = 0;
        $z->prev = 0;
        $queue[0][] = $z;
        $priority = 0;
        $starttime = microtime(true);

        // A-star search of shortest diff.
        while (true) {
            if ($timelimit > 0 && microtime(true) - $starttime > $timelimit / 1000) {
                // Time out.
                throw new moodle_exception('difftoolarge');
            }
            while (count($queue[$priority]) == 0) {
                $priority++;
            }
            $z = array_pop($queue[$priority]);
            $i1 = $z->i1;
            $i2 = $z->i2;
            if (isset($prev[$i1][$i2])) {
                continue;
            } else {
                $prev[$i1][$i2] = $z->prev;
            }

            // Identical subarray: 0 distance.
            while ($i1 < $n1 && $i2 < $n2 && $array1[$i1] == $array2[$i2]) {
                $i1++;
                $i2++;
                $prev[$i1][$i2] = 0;
            }

            if ($i1 == $n1 && $i2 == $n2) {
                // Found shortest diff amongst arrays.
                $result = new stdClass();
                $result->diff = $z->d;
                $result->prev = $prev;
                return $result;
            }

            // Element addition in array1.
            if ($i1 < $n1 && !isset($prev[$i1 + 1][$i2])) {
                $z1 = new stdClass();
                $z1->i1 = $i1 + 1;
                $z1->i2 = $i2;
                $z1->d = $z->d + 1;
                $z1->prev = 1;
                $queue[$z1->d + abs(($n1 - $z1->i1) - ($n2 - $z1->i2))][] = $z1;
            }

            // Element addition in array2.
            if ($i2 < $n2 && !isset($prev[$i1][$i2 + 1])) {
                $z2 = new stdClass();
                $z2->i1 = $i1;
                $z2->i2 = $i2 + 1;
                $z2->d = $z->d + 1;
                $z2->prev = 2;
                $queue[$z2->d + abs(($n1 - $z2->i1) - ($n2 - $z2->i2))][] = $z2;
            }

            // Element change.
            if ($i1 < $n1 && $i2 < $n2 && !isset($prev[$i1 + 1][$i2 + 1])) {
                $z3 = new stdClass();
                $z3->i1 = $i1 + 1;
                $z3->i2 = $i2 + 1;
                $z3->d = $z->d + 1;
                $z3->prev = 3;
                $queue[$z3->d + abs(($n1 - $z3->i1) - ($n2 - $z3->i2))][] = $z3;
            }
        }
    }
}
