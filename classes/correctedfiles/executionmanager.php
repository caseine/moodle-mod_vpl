<?php
// This file is part of VPL for Moodle - http://vpl.dis.ulpgc.es/
//
// VPL for Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// VPL for Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with VPL for Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Corrected files behavior definition for execution.
 * @package mod_vpl
 * @copyright 2024 Astor Bizard
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_vpl\correctedfiles;

use mod_vpl, moodle_url, html_writer;

/**
 * Corrected files behavior for execution.
 * @copyright 2024 Astor Bizard
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class executionmanager {

    /**
     * @var string Default directory for corrected files with execution files.
     */
    public const DEFAULT_DIRECTORY = 'vpl_corrected_files';

    /**
     *
     * @param mod_vpl $vpl
     */
    public static function add_files($vpl, &$data) {
        $correctedfilesforexecution = json_decode($vpl->get_instance()->correctedfilesforexecution);
        if (!empty($correctedfilesforexecution->send)) {
            foreach ($vpl->get_fgm('corrected')->getallfiles() as $filename => $filecontents) {
                $dir = $correctedfilesforexecution->directory ?: self::DEFAULT_DIRECTORY; // Failsafe that should not be needed.
                // Add corrected files to $data.
                $data->files[$dir . '/' . $filename] = $filecontents;
                if (empty($correctedfilesforexecution->keepwhenrunning)) {
                    // Files are not to be kept when running, mark them for deletion.
                    $data->filestodelete[$dir . '/' . $filename] = 1;
                }
            }
        }
    }

    /**
     *
     * @param mod_vpl $vpl
     */
    public static function bash_export_dirname($vpl) {
        global $CFG;
        $correctedfilesforexecution = json_decode($vpl->get_instance()->correctedfilesforexecution);
        if (!empty($correctedfilesforexecution->send)) {
            require_once($CFG->dirroot . '/mod/vpl/locallib.php');
            $dir = $correctedfilesforexecution->directory ?: self::DEFAULT_DIRECTORY; // Failsafe that should not be needed.
            return vpl_bash_export('VPL_CORRECTED_FILES_DIR', $dir);
        } else {
            return '';
        }
    }

    public static function get_dirname_html($dirname) {
        return html_writer::span($dirname . '/', 'dirname-inline');
    }

    public static function get_keepfiles_checkbox($vplid, $forexecution) {
        global $PAGE;
        $managelink = html_writer::link(
                new moodle_url('/mod/vpl/forms/managecorrectedfiles.php', [ 'id' => $vplid, 'returnurl' => $PAGE->url ]),
                get_string('manage', VPL));
        if (!empty($forexecution->send)) {
            $label = get_string('correctedfilesunderdir', 'mod_vpl', static::get_dirname_html($forexecution->directory));
            $checked = $forexecution->keepwhenrunning ?? 0;
        } else {
            $label = get_string('correctedfiles', 'mod_vpl');
            $checked = 0;
        }
        return [ $label . ' (' . $managelink . ')', $checked ];
    }

    /**
     * Print current settings about how corrected files are sent along with execution files.
     * @param mod_vpl $vpl
     */
    public static function print_settings($vpl) {
        $forexecution = json_decode($vpl->get_instance()->correctedfilesforexecution);
        if ($forexecution === null || empty($forexecution->send)) {
            echo html_writer::div(get_string('correctedfilesarenotsent', 'mod_vpl'));
        } else {
            echo html_writer::div(get_string('correctedfilesaresent', 'mod_vpl', static::get_dirname_html($forexecution->directory)));
            if (empty($forexecution->keepwhenrunning)) {
                echo html_writer::div('<i class="fa fa-chain-broken mx-1"></i>' . get_string('correctedfilesarenotkept', 'mod_vpl'));
            } else {
                echo html_writer::div('<i class="fa fa-chain mx-1"></i>' . get_string('correctedfilesarekept', 'mod_vpl'));
            }
        }
    }
}
