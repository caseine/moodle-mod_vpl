<?php
// This file is part of VPL for Moodle - http://vpl.dis.ulpgc.es/
//
// VPL for Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// VPL for Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with VPL for Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Corrected files management form definition.
 * @package mod_vpl
 * @copyright 2024 Astor Bizard
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_vpl\correctedfiles;

defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once($CFG->libdir . '/formslib.php');

use moodleform, mod_vpl, stdClass;

/**
 * Corrected files management form.
 * @copyright 2024 Astor Bizard
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class manage_form extends moodleform {

    /**
     * @var mod_vpl
     */
    protected $vpl;

    /**
     * Constructor.
     * @param mod_vpl $vpl
     * @param mixed $action Standard moodleform action.
     */
    public function __construct($vpl, $action) {
        $this->vpl = $vpl;
        parent::__construct($action);
    }

    /**
     * {@inheritDoc}
     * @see moodleform::definition()
     */
    protected function definition() {
        $mform = $this->_form;

        // Corrected files availability.
        $mform->addElement('header', 'availabilityheader', get_string('correctedfilesavailability', 'mod_vpl'));
        $availability = [];
        $flexlinebreak = '<hr class="h-0 w-100 invisible m-0">'; // Use this as it is a semantic line break and avoids adding CSS.
        $availability[] =& $mform->createElement('advcheckbox', 'graders', get_string('userswithgradecapability', 'mod_vpl'));
        $availability[] =& $mform->createElement('html', $flexlinebreak);
        $gradesetting = $this->vpl->get_grade();
        if ($gradesetting != 0) {
            $availability[] =& $mform->createElement('advcheckbox', 'minimalgrade', get_string('userswithgradeatleast', 'mod_vpl'));
            if ($gradesetting > 0) {
                // Grade by points.
                $availability[] =& $mform->createElement('text', 'minimalgradevalue', null, [ 'size' => 4 ]);
                $mform->setDefault('availability[minimalgradevalue]', $gradesetting);
                $mform->setType('availability[minimalgradevalue]', PARAM_RAW_TRIMMED);
            } else {
                // Scale.
                $scalemenu = make_grades_menu($gradesetting);
                $availability[] =& $mform->createElement('select', 'minimalgradevalue', null, $scalemenu);
                $mform->setDefault('availability[minimalgradevalue]', count($scalemenu));
            }
        }
        $availability[] =& $mform->createElement('html', $flexlinebreak);
        $availability[] =& $mform->createElement('advcheckbox', 'everyoneafter', get_string('everyoneafterdate', 'mod_vpl'));
        $availability[] =& $mform->createElement('date_time_selector', 'afterdate');
        $availability[] =& $mform->createElement('html', $flexlinebreak);
        $groupsmenu = groups_list_to_menu(groups_get_all_groups($this->vpl->get_course()->id));
        if ($groupsmenu) {
            $availability[] =& $mform->createElement('advcheckbox', 'usersingroups', get_string('usersingroups', 'mod_vpl'));
            $availability[] =& $mform->createElement('select', 'groups', null, $groupsmenu, [ 'multiple' => true ]);
        }
        $mform->addGroup($availability, 'availability', get_string('correctedfilesareavailableto', 'mod_vpl'), '');
        $mform->addHelpButton('availability', 'correctedfilesareavailableto', 'mod_vpl');
        $mform->setDefault('availability[graders]', 1);
        $mform->disabledIf('availability[minimalgradevalue]', 'availability[minimalgrade]');
        foreach ([ 'day', 'month', 'year', 'hour', 'minute' ] as $field) {
            $mform->disabledIf('availability[afterdate][' . $field . ']', 'availability[everyoneafter]');
        }
        $mform->disabledIf('availability[groups][]', 'availability[usersingroups]');

        // Corrected files for execution.
        $mform->addElement('header', 'executionheader', get_string('correctedfilesforexecution', 'mod_vpl'));
        $forexecution = [];
        $forexecution[] =& $mform->createElement('selectyesno', 'sendtoexecution');
        $forexecution[] =& $mform->createElement('html', get_string('indirectory', 'mod_vpl'));
        $forexecution[] =& $mform->createElement('text', 'directory');
        $mform->addGroup($forexecution, 'forexecution', get_string('includecorrectedfilesforexecution', 'mod_vpl'), null, false);
        $mform->addHelpButton('forexecution', 'includecorrectedfilesforexecution', 'mod_vpl');
        $mform->setDefault('sendtoexecution', 0);
        $mform->setType('directory', PARAM_PATH);
        $mform->setDefault('directory', executionmanager::DEFAULT_DIRECTORY . '/');
        $mform->disabledIf('directory', 'sendtoexecution', 'neq', 1);
        $icon = '<i class="fa ' . mod_vpl_get_fontawesome_icon_map()['mod_vpl:keepfiles'] . ' mr-1"></i>';
        $mform->addElement('advcheckbox', 'keepwhenrunning', $icon . get_string('keepwhenrunning', 'mod_vpl'));
        $mform->setDefault('keepwhenrunning', 0);
        $mform->disabledIf('keepwhenrunning', 'sendtoexecution', 'neq', 1);

        $this->add_action_buttons();
    }

    /**
     * Clean a user-provided directory name.
     * @param string $rawdirname The user-provided directory name, already cleaned by clean_param() as a PARAM_PATH.
     * @return string The cleaned name.
     */
    protected static function clean_directory_name($rawdirname) {
        // Clean leading and trailing slashes, and starting ./ if any (as it is left by PARAM_PATH cleaning).
        $dirname = trim($rawdirname, '/');
        if (strpos($dirname, './') === 0) {
            $dirname = substr($dirname, 2);
        }
        // Limit size to 200 to make sure the DB field can hold it. This is ugly but will be fine for every sane user.
        return substr($dirname, 0, 200);
    }

    /**
     * {@inheritDoc}
     * @param array $data Form submitted data.
     * @param array $files Form submitted files.
     * @see moodleform::validation()
     */
    public function validation($data, $files) {
        $errors = parent::validation($data, $files);

        // Validate that grade is a number between 0 and maxgrade.
        $gradesetting = $this->vpl->get_grade();
        if ($gradesetting > 0 && $data['availability']['minimalgrade']) {
            $res = unformat_float($data['availability']['minimalgradevalue'], true);
            if ($res === null || $res === false || $res < 0 || $res > $gradesetting) {
                $errors['availability'] = get_string('error:invalidmanualgrade', 'mod_vpl', format_float($gradesetting));
            }
        }

        // Validate directory name. We want only alphanumeric characters, hyphens and underscores because it is simpler to handle.
        if ($data['sendtoexecution']) {
            $directory = static::clean_directory_name($data['directory']);
            if (empty($directory) || $directory == '.' || preg_replace('/[^a-zA-Z0-9\/_-]/', '', $directory) !== $directory) {
                $errors['forexecution'] = get_string('error:invaliddirectoryname', 'mod_vpl');
            }
        }

        return $errors;
    }

    /**
     * {@inheritDoc}
     * @param stdClass|array $values Object or array of values
     * @see moodleform::set_data()
     */
    public function set_data($values) {
        if (is_object($values)) {
            $values = (array)$values;
        }
        // Unformat JSON data from the DB.
        if (!empty($values['availability']) && ($availability = json_decode($values['availability'])) !== null) {
            unset($values['availability']);
            $values['availability[graders]'] = $availability->graders ?? 0;
            if (isset($availability->minimalgrade)) {
                $values['availability[minimalgrade]'] = 1;
                $values['availability[minimalgradevalue]'] = $availability->minimalgrade;
            } else {
                $values['availability[minimalgrade]'] = 0;
            }
            if (isset($availability->everyoneafter)) {
                $values['availability[everyoneafter]'] = 1;
                $values['availability[afterdate]'] = $availability->everyoneafter;
            } else {
                $values['availability[everyoneafter]'] = 0;
            }
            if (isset($availability->usersingroups)) {
                $values['availability[usersingroups]'] = 1;
                $values['availability[groups]'] = $availability->usersingroups;
            } else {
                $values['availability[usersingroups]'] = 0;
            }
        }
        if (!empty($values['forexecution']) && ($forexecution = json_decode($values['forexecution'])) !== null) {
            unset($values['forexecution']);
            $values['sendtoexecution'] = $forexecution->send;
            $values['directory'] = $forexecution->directory . '/';
            $values['keepwhenrunning'] = $forexecution->keepwhenrunning;
        }
        parent::set_data($values);
    }

    /**
     * {@inheritDoc}
     * @see moodleform::get_data()
     */
    public function get_data() {
        $data = parent::get_data();
        if ($data) {
            // Format data into JSON for the DB.
            $formattedavailability = new stdClass();
            $formavailability = $data->availability;
            if ($formavailability['graders']) {
                $formattedavailability->graders = 1;
            }
            if (!empty($formavailability['minimalgrade'])) {
                $formattedavailability->minimalgrade = unformat_float($formavailability['minimalgradevalue']);
            }
            if ($formavailability['everyoneafter']) {
                $formattedavailability->everyoneafter = $formavailability['afterdate'];
            }
            if (!empty($formavailability['usersingroups'])) {
                $formattedavailability->usersingroups = $formavailability['groups'];
            }
            $data->availability = json_encode($formattedavailability);

            $data->forexecution = json_encode((object) [
                    'send' => $data->sendtoexecution,
                    'directory' => static::clean_directory_name($data->directory),
                    'keepwhenrunning' => $data->keepwhenrunning,
            ]);
        }
        return $data;
    }
}
