<?php
// This file is part of VPL for Moodle - http://vpl.dis.ulpgc.es/
//
// VPL for Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// VPL for Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with VPL for Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Corrected files availability rules definition.
 * @package mod_vpl
 * @copyright 2024 Astor Bizard
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_vpl\correctedfiles;

use mod_vpl, html_writer, moodle_exception;

/**
 * Corrected files availability rules.
 * @copyright 2024 Astor Bizard
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class availability {
    /**
     * Determine if user can see corrected files.
     * @param mod_vpl $vpl
     * @param int|null $userid User ID or null for current user.
     * @return boolean true iff user can see corrected files.
     */
    public static function can_see($vpl, $userid = null) {
        global $USER, $CFG;
        if (!$userid) {
            $userid = $USER->id;
        }
        if ($vpl->has_capability(VPL_MANAGE_CAPABILITY, $userid)) {
            return true;
        }
        if (!$vpl->is_visible($userid) || $vpl->get_instance()->example) {
            return false;
        }
        $availability = json_decode($vpl->get_instance()->correctedfilesavailability);
        if ($availability === null) {
            // By default, only graders can see corrected files.
            return $vpl->has_capability(VPL_GRADE_CAPABILITY, $userid);
        }
        if (!empty($availability->graders) && $vpl->has_capability(VPL_GRADE_CAPABILITY, $userid)) {
            // Available to graders and user is a grader.
            return true;
        }
        if (!empty($availability->minimalgrade)) {
            require_once($CFG->dirroot . '/mod/vpl/vpl_submission.class.php');
            if ($sub = $vpl->get_effective_submission_for_grade($userid)) {
                $submission = new \mod_vpl_submission($vpl, $sub);
                if ($submission->is_graded() && $submission->reduce_grade($sub->grade) >= $availability->minimalgrade) {
                    // Available with minimal grade and user has achieved that minimal grade.
                    return true;
                }
            }
        }
        if (isset($availability->everyoneafter) && time() >= $availability->everyoneafter) {
            // Available after a date and we are after this date.
            return true;
        }
        if (isset($availability->usersingroups)) {
            $usergroups = groups_get_user_groups($vpl->get_course()->id, $userid)[0];
            if (!empty(array_intersect($usergroups, $availability->usersingroups))) {
                // Available to some groups and user is in one of these.
                return true;
            }
        }
        return false;
    }

    /**
     * Check that user can see corrected files, and throw an exception if not.
     * @param mod_vpl $vpl
     * @param int|null $userid User ID or null for current user.
     */
    public static function require_capability_to_see($vpl, $userid = null) {
        if (!static::can_see($vpl, $userid)) {
            throw new moodle_exception('nopermissions', 'error', $vpl->get_view_url(), get_string('loadcorrectedfiles', 'mod_vpl'));
        }
    }

    /**
     * Print current settings about who can see corrected files.
     * @param mod_vpl $vpl
     */
    public static function print_settings($vpl) {
        $availability = json_decode($vpl->get_instance()->correctedfilesavailability);
        $list = [];
        if ($availability === null) {
            // By default, only graders can see corrected files.
            $list[] = get_string('userswithgradecapability', 'mod_vpl');
        } else {
            if (!empty($availability->graders)) {
                $list[] = get_string('userswithgradecapability', 'mod_vpl');
            }
            if (!empty($availability->minimalgrade)) {
                $list[] = get_string('userswithgradeatleast', 'mod_vpl') . ' ' . $vpl->format_grade($availability->minimalgrade);
            }
            if (isset($availability->everyoneafter)) {
                $list[] = get_string('everyoneafterdate', 'mod_vpl') . ' ' . userdate($availability->everyoneafter);
            }
            if (isset($availability->usersingroups)) {
                $list[] = get_string('usersingroups', 'mod_vpl') . ' ' . format_string(implode(', ',  array_map('groups_get_group_name', $availability->usersingroups)));
            }
        }
        if (empty($list)) {
            echo html_writer::div(get_string('correctedfilesarenotavailable', 'mod_vpl'));
        } else {
            echo html_writer::div(get_string('correctedfilesareavailableto', 'mod_vpl') . html_writer::alist($list));
        }
    }
}
