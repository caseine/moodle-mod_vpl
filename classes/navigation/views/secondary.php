<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace mod_vpl\navigation\views;

use core\navigation\views\secondary as core_secondary;

/**
 * Class secondary_navigation_view.
 *
 * Custom implementation for a plugin.
 *
 * @package     mod_vpl
 * @category    navigation
 * @copyright   2023 Astor Bizard, 2021 Sujith Haridasan <sujith@moodle.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class secondary extends core_secondary {
    /**
     * Define a custom secondary nav order/view.
     *
     * @return array
     */
    protected function get_default_module_mapping(): array {
        $defaultmapping = parent::get_default_module_mapping();

        $defaultmapping[self::TYPE_SETTING] = array_merge($defaultmapping[self::TYPE_SETTING], [
            'roleassign' => 22.2,
            'filtermanage' => 21,
            'roleoverride' => 22,
            'rolecheck' => 22.1,
            'logreport' => 23,
            'backup' => 24,
            'restore' => 25,
            'competencybreakdown' => 26,
        ]);

        $defaultmapping[self::TYPE_CUSTOM] = array_merge($defaultmapping[self::TYPE_CUSTOM], [
            'advgrading' => 18,
            'contentbank' => 27,
        ]);

        $defaultmapping[self::TYPE_ACTIVITY] = [
            'testcases' => 2,
            'executionoptions' => 3,
            'requiredfiles' => 4,
            'correctedfiles' => 5,
            'executionfiles' => 6,
            'maxresourcelimits' => 7,
            'keepfiles' => 8,
            'variations' => 9,
            'overrides' => 10,
            'check_jail_servers' => 11,
            'local_jail_servers' => 12,
            'submission' => 13,
            'edit' => 14,
            'submissionview' => 15,
            'dograde' => 16,
            'previoussubmissionslist' => 17,
            'modulenameplural' => 19,
            'checkgroups' => 20,
        ];

        return $defaultmapping;
    }
}
