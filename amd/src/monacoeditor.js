// This file is part of VPL for Moodle - http://vpl.dis.ulpgc.es/
//
// VPL for Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// VPL for Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with VPL for Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Monaco editor interfacing (to match Ace API).
 *
 * @package mod_vpl
 * @copyright 2022 Astor Bizard
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define(['jquery', 'vs/editor/editor.main', 'core/log'], function($, Monaco, log) {

    function acelangToMonacolang(aceLang) {
        var map = {
                'abap': 'abap',
                'batchfile': 'bat',
                'c_cpp': 'cpp',
                'clojure': 'clojure',
                'coffee': 'coffee',
                'css': 'css',
                'dart': 'dart',
                'golang': 'go',
                'html': 'html',
                'java': 'java',
                'javascript': 'javascript',
                'kotlin': 'kotlin',
                'less': 'less',
                'lua': 'lua',
                'markdown': 'markdown',
                'pascal': 'pascal',
                'perl': 'perl',
                'php': 'php',
                'python': 'python',
                'r': 'r',
                'ruby': 'ruby',
                'scala': 'scala',
                'scheme': 'scheme',
                'scss': 'scss',
                'sh': 'shell',
                'sql': 'sql',
                'swift': 'swift',
                'tcl': 'tcl',
                'twig': 'twig',
                'typescript': 'typescript',
                'vbscript': 'vb',
                'verilog': 'systemverilog',
                'xml': 'xml',
                'yaml': 'yaml'
        };
        if (aceLang in map) {
            return map[aceLang];
        }
        return aceLang;
    }

    function MonacoEditor(container) {
        var self = this;
        this.editor = Monaco.editor.create(container);

        this.commands = {commands: [], buttons: []};

        this.getValue = function() {
            return this.editor.getValue();
        };
        this.setValue = function(value) {
            return this.editor.setValue(value);
        };
        this.destroy = function() {
            return this.editor.dispose();
        };
        this.setFontSize = function(size) {
            return this.editor.updateOptions({fontSize: size});
        };
        this.resize = function() {
            return this.editor.layout();
        };
        this.focus = function() {
            return this.editor.focus();
        };
        this.blur = function() {
            return this.editor.hasTextFocus() ? document.activeElement.blur() : undefined;
        };
        this.gotoLine = function(line, column) {
            return this.editor.setPosition(new Monaco.Position(line, column));
        };
        this.scrollToLine = function(line, center) {
            return center ? this.editor.revealLineInCenter(line) : this.editor.revealLine(line);
        };
        this.getCursorPosition = function() {
            var position = this.editor.getPosition();
            return {row: position.lineNumber - 1, column: position.column - 1};
        };
        this.setReadOnly = function(readOnly) {
            return this.editor.updateOptions({readOnly: readOnly});
        };
        this.undo = function() {
            return this.editor.trigger('mod_vpl', 'undo');
        };
        this.redo = function() {
            return this.editor.trigger('mod_vpl', 'redo');
        };
        this.selectAll = function() {
            var linecount = this.editor.getModel().getLineCount();
            return this.editor.setSelection(new Monaco.Range(1, 1, linecount, this.editor.getModel().getLineLength(linecount) + 1));
        };
        this.execCommand = function(command) {
            var commandMap = {
                    'find': 'actions.find',
                    'findnext': 'editor.action.nextMatchFindAction',
                    'replace': 'editor.action.startFindReplaceAction'
            };
            if (command in commandMap) {
                command = commandMap[command];
            }
            return this.editor.trigger('mod_vpl', command);
        };
        this.theme = null;
        this.setTheme = function(theme) {
            if (theme.startsWith('ace/theme/')) {
                theme = theme.substring(10);
            }
            this.theme = theme;
            return this.editor.updateOptions({theme: theme});
        };
        this.insert = function(text) {
            this.editor.executeEdits('mod_vpl', this.editor.getSelections().map(function(sel) {
                return {identifier: {major: 1, minor: 1}, range: sel, text: text, forceMoveMarkers: true};
            }));
            this.editor.pushUndoStop();
        };
        this.on = function(eventtype, callback, capture) {
            if (typeof capture === 'undefined') {
                capture = false;
            }
            switch (eventtype) {
            case 'change':
                this.editor.onDidChangeModelContent(callback);
                break;
            case 'copy':
                this.editor.getDomNode().addEventListener('copy', function(e) {
                    callback(e.srcElement.value.substring(e.srcElement.selectionStart, e.srcElement.selectionEnd));
                }, capture);
                break;
            default:
                this.editor.getDomNode().addEventListener(eventtype, callback, capture);
                break;
            }
        };
        Object.defineProperty(this, 'onPaste', {
            set: function(newOnPaste) {
                self.on('paste', function(e) {
                    newOnPaste(e);
                    e.stopPropagation();
                    e.preventDefault();
                }, true);
            }
        });

        function MonacoSession() {
            this.setMode = function(mode) {
                if (mode.startsWith('ace/mode/')) {
                    mode = mode.substring(9);
                }
                mode = acelangToMonacolang(mode);
                return Monaco.editor.setModelLanguage(self.editor.getModel(), mode);
            };

            this.getUndoManager = function() {
                return {
                    hasUndo: function() {
                        return self.editor.getModel().canUndo();
                    },
                    hasRedo: function() {
                        return self.editor.getModel().canRedo();
                    }
                };
            };

            this.getAnnotations = function() {
                return Monaco.editor.getModelMarkers({owner: 'mod_vpl_' + container.id}).map(function(marker) {
                    var type;
                    if (marker.severity == Monaco.MarkerSeverity.Error) {
                        type = 'error';
                    } else if (marker.severity == Monaco.MarkerSeverity.Warning) {
                        type = 'warning';
                    } else {
                        type = 'info';
                    }
                    return {
                        type: type,
                        text: marker.message,
                        row: marker.startLineNumber - 1,
                        column: 1
                    };
                });
            };
            this.clearAnnotations = function() {
                this.setAnnotations([]);
            };
            this.setAnnotations = function(annotations) {
                var markers = annotations.map(function(annotation) {
                    var line = annotation.row + 1;
                    if (line > self.editor.getModel().getLineCount()) {
                        // Annotation points beyond the file line count.
                        return {};
                    }
                    var severity;
                    if (annotation.type == 'info') {
                        severity = Monaco.MarkerSeverity.Info;
                    } else if (annotation.type == 'warning') {
                        severity = Monaco.MarkerSeverity.Warning;
                    } else {
                        severity = Monaco.MarkerSeverity.Error;
                    }
                    return {
                        severity: severity,
                        message: annotation.text,
                        startLineNumber: line,
                        startColumn: self.editor.getModel().getLineMinColumn(line),
                        endLineNumber: line,
                        endColumn: self.editor.getModel().getLineMaxColumn(line)
                    };
                });
                Monaco.editor.setModelMarkers(self.editor.getModel(), 'mod_vpl_' + container.id, markers);
            };

            this._decorations = {};
            this._cleanGutterDecorations = function() {
                Object.keys(this._decorations).forEach(function(line) {
                    if (line > self.editor.getModel().getLineCount()) {
                        self.session._deleteGutterDecoration(line);
                    }
                });
            };
            this.addGutterDecoration = function(line, className) {
                line = line + 1;
                var currentDecoration = typeof this._decorations[line] !== 'undefined' ? [this._decorations[line]] : [];
                this._decorations[line] = self.editor.deltaDecorations(
                    currentDecoration,
                    [
                        {
                            range: new Monaco.Range(line, 1, line, self.editor.getModel().getLineLength(line) + 1),
                            options: {
                                linesDecorationsClassName: className
                            }
                        }
                    ]
                )[0];
            };
            this._deleteGutterDecoration = function(line) {
                if (typeof this._decorations[line] !== 'undefined') {
                    self.editor.deltaDecorations(
                        [this._decorations[line]],
                        []
                    );
                    delete this._decorations[line];
                }
            };
            this.removeGutterDecoration = function(line) {
                this._cleanGutterDecorations();
                this._deleteGutterDecoration(line + 1);
            };
            this.setUseSoftTabs = function(softTabs) {
                self.editor.updateOptions({detectIndentation: false, insertSpaces: softTabs});
            };
            this.setTabSize = function(tabSize) {
                self.editor.updateOptions({detectIndentation: false, tabSize: tabSize});
            };

            this.selection = {
                on: function(eventtype, callback) {
                    switch (eventtype) {
                    case 'changeCursor':
                        self.editor.onDidChangeCursorPosition(callback);
                        break;
                    case 'changeSelection':
                        self.editor.onDidChangeCursorSelection(callback);
                        break;
                    default:
                        log.error('Event type not supported for monaco.selection: ' + eventtype);
                        break;
                    }
                }
            };
        }

        this.session = new MonacoSession();

        this.getSession = function() {
            return this.session;
        };
    }

    return {
        create: function(container) {
            var editor = new MonacoEditor(container);

            $('#vpl_ide_dialog_theme .editortheme option').each(function() {
                var themeId = $(this).attr('value');
                if (themeId == 'vs' || themeId == 'vs-dark' || themeId == 'hc-black') {
                    return;
                }
                $.ajax('../editor/monaco/themes/' + $(this).text() + '.json')
                .done(function(themeData) {
                    Monaco.editor.defineTheme(themeId, themeData);
                    if (editor.theme == themeId) {
                        editor.setTheme(themeId); // Reload theme if it was set before loading.
                    }
                });
            });

            return editor;
        }
    };
});