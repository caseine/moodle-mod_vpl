define([], function() {
    var submissionsCheckboxes = document.querySelectorAll('input[name^="selectug"]');
    var selectAll = document.getElementsByName('selectallug')[0];

    function updateButton() {
        var button = document.querySelector('#evaluateselectedform button');
        var checked = Array.from(submissionsCheckboxes).filter(checkbox => checkbox.checked);
        button.innerText = button.innerText.replace(/\(\d\)$/, '(' + checked.length + ')');
        if (checked.length > 0) {
            button.removeAttribute('disabled');
        } else {
            button.setAttribute('disabled', '');
        }
        var userlist = checked.map(checkbox => checkbox.name.substring(8));
        document.querySelector('#evaluateselectedform input[name="userid"]').value = userlist.length ? userlist[0] : '';
        document.querySelector('#evaluateselectedform input[name="userlist"]').value = userlist.join(',');
        selectAll.indeterminate = checked.length > 0 && checked.length < submissionsCheckboxes.length;
        selectAll.checked = checked.length === submissionsCheckboxes.length;
    }

    return {
        setupEvaluateSelectedButton: function() {
            submissionsCheckboxes.forEach(checkbox => { checkbox.onchange = updateButton; });
            updateButton();
            selectAll.onchange = function() {
                if (!selectAll.indeterminate) {
                    submissionsCheckboxes.forEach(checkbox => { checkbox.checked = selectAll.checked; });
                    updateButton();
                }
            };
        }
    };
});
