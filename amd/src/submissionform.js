define(['jquery'], function($) {
    return {
        setup: function() {
            $('select[name="submitmethod"]').change(function() {
                $('#id_headersubmitarchive').toggle($(this).val() == 'archive').removeClass('collapsed');
                $('#id_headersubmitfiles').toggle($(this).val() != 'archive').removeClass('collapsed');
            }).change();
        }
    };
});