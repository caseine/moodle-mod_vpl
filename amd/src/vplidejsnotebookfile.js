// This file is part of VPL for Moodle - http://vpl.dis.ulpgc.es/
//
// VPL for Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// VPL for Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with VPL for Moodle.  If not, see <http://www.gnu.org/licenses/>.

define(
    [
        'jquery',
        'mod_vpl/vplutil',
    ],
    function($, VPLUtil) {
        return function() {
            this.formatAndShowNotebook = function() {
                var tid = this.getTId();
                try {
                    $(tid).html(VPLUtil.formatJSNotebook(this.getContent()))
                    .prepend('<div class="position-absolute text-center w-100 pr-4 bg-white" style="z-index:1">' +
                                '<i class="icon fa fa-info-circle text-info" style="font-size:1em; font-family:FontAwesome"></i>' +
                                M.util.get_string('jsnotebooknotice', 'mod_vpl') +
                             '</div>');
                } catch (syntaxError) {
                    // The file is not a well-formed notebook.
                    // Display a message and the text file.
                    $(tid).text(this.getContent())
                    .prepend('<div>' + M.util.get_string('badjsonnotebook', 'mod_vpl') + '</div>');
                }
            };
            var setOldContent = this.setContent;
            this.setContent = function(c) {
                setOldContent.call(this, c);
                if (this.isOpen()) {
                    this.formatAndShowNotebook();
                }
            };
            this.open = function() {
                this.showFileName();
                this.setOpen(true);
                this.formatAndShowNotebook();
            };
            this.close = function() {
                this.setOpen(false);
            };
            this.isReadOnly = function() {
                return true;
            };
        };
    }
);
