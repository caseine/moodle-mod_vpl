// This file is part of VPL for Moodle - http://vpl.dis.ulpgc.es/
//
// VPL for Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// VPL for Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with VPL for Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Code files extension method using ACE editorfiles. Add to VPLFile object.
 *
 * @copyright 2013 Juan Carlos Rodríguez-del-Pino
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author Juan Carlos Rodríguez-del-Pino <jcrodriguez@dis.ulpgc.es>
 */

/* globals ace */

define(
    [
        'jquery',
        'core/log',
        'mod_vpl/vplutil',
        'mod_vpl/vplui',
    ],
    function($, log, VPLUtil, VPLUI) {

        return function() {
            var self = this;
            var editor = null;
            var session = null;
            var readOnly = self.getFileManager().readOnly;
            var getOldContent = this.getContent;
            this.getContent = function() {
                if (!this.isOpen()) {
                    return getOldContent.call(this);
                }
                return editor.getValue();
            };
            var setOldContent = this.setContent;
            this.setContent = function(c) {
                setOldContent.call(this, c);
                if (this.isOpen()) {
                    editor.setValue(c);
                }
            };
            var oldDestroy = this.destroy;
            this.destroy = function() {
                if (this.isOpen()) {
                    editor.destroy();
                }
                oldDestroy.call(this);
            };
            this.setFontSize = function(size) {
                if (this.isOpen()) {
                    editor.setFontSize(size);
                }
            };
            var oldAdjustSize = this.adjustSize;
            this.adjustSize = function() {
                if (oldAdjustSize.call(this)) {
                    editor.resize(true);
                    return true;
                }
                return false;
            };
            this.gotoLine = function(line, column) {
                if (!this.isOpen()) {
                    return;
                }
                if (typeof column === 'undefined') {
                    column = 0;
                }
                editor.gotoLine(line, column);
                editor.scrollToLine(line, true);
                editor.focus();
                this.updateStatus();
            };
            this.setReadOnly = function(s) {
                readOnly = s;
                if (this.isOpen()) {
                    editor.setReadOnly(s);
                }
            };
            this.isReadOnly = function() {
                return readOnly;
            };
            this.focus = function() {
                if (!this.isOpen()) {
                    return;
                }
                // Workaround to remove jquery-ui theme background color.
                $(this.getTId()).removeClass('ui-widget-content ui-tabs-panel');
                this.adjustSize();
                editor.focus();
                this.updateStatus();
            };
            this.blur = function() {
                if (!this.isOpen()) {
                    return;
                }
                editor.blur();
            };
            this.undo = function() {
                if (!this.isOpen()) {
                    return;
                }
                editor.undo();
                editor.focus();
            };
            this.redo = function() {
                if (!this.isOpen()) {
                    return;
                }
                editor.redo();
                editor.focus();
            };
            this.selectAll = function() {
                if (!this.isOpen()) {
                    return;
                }
                editor.selectAll();
                editor.focus();
            };
            this.hasUndo = function() {
                if (!this.isOpen()) {
                    return false;
                }
                return session.getUndoManager().hasUndo();
            };
            this.hasRedo = function() {
                if (!this.isOpen()) {
                    return false;
                }
                return session.getUndoManager().hasRedo();
            };
            this.hasSelectAll = VPLUtil.returnTrue;
            this.hasFind = VPLUtil.returnTrue;
            this.hasFindReplace = VPLUtil.returnTrue;
            this.hasNext = VPLUtil.returnTrue;
            this.find = function() {
                if (!this.isOpen()) {
                    return;
                }
                editor.execCommand('find');
            };
            this.replace = function() {
                if (!this.isOpen()) {
                    return;
                }
                editor.execCommand('replace');
            };
            this.next = function() {
                if (!this.isOpen()) {
                    return;
                }
                editor.execCommand('findnext');
            };
            this.annotationsBuffer = [];
            this.getAnnotations = function() {
                if (!this.isOpen()) {
                    return [...this.annotationsBuffer];
                }
                return session.getAnnotations();
            };
            this.setAnnotations = function(a) {
                if (!this.isOpen()) {
                    this.annotationsBuffer = [...a];
                    return true;
                }
                return session.setAnnotations(a);
            };
            this.clearAnnotations = function() {
                if (!this.isOpen()) {
                    this.annotationsBuffer = [];
                    return true;
                }
                return session.clearAnnotations();
            };
            this.langSelection = function() {
                if (!this.isOpen()) {
                    return;
                }
                var ext = VPLUtil.fileExtension(this.getFileName());
                var lang = (ext !== '') ? VPLUtil.langType(ext) : 'text';
                editor.getSession().setMode('ace/mode/' + lang);
                this.setLang(lang);
            };
            this.getEditor = function() {
                if (!this.isOpen()) {
                    return false;
                }
                return editor;
            };
            this.setTheme = function(theme) {
                if (!this.isOpen()) {
                    return;
                }
                editor.setTheme("ace/theme/" + theme);
            };
            this.check_difference = function() {
                var URL = "../similarity/diff_check_required_files.php";
                $.ajax({
                    method: "POST",
                    url: URL,
                    data: { id : this.getCurrentIdVpl(), name: this.getFileName(), val : editor.getValue() }
                })
                .done(function( result ) {
                    if (result.success) {
                        var diffData = result.response.diff;
                        var modified = 'diff-w-rf'; // CSS class.
                        diffData.forEach(function(diff) {
                            editor.session.removeGutterDecoration(diff.ln2-1, modified);
                            if (diff.type != "=" && diff.ln2 !== false) {
                                editor.session.addGutterDecoration(diff.ln2-1, modified);
                            }
                        });
                    }
                }).fail(function(jqxhr, status, error) {
                    log.error(error);
                });
            };
            this.updateStatus = function() {
                if (!this.isOpen()) {
                    return;
                }
                var text = '';
                var pos = editor.getCursorPosition();
                var fullname = this.getFileName();
                var name = VPLUtil.getFileName(fullname);
                if (fullname.length > 20 || name != fullname) {
                    text = fullname + ' ';
                }
                text += "Ln " + (pos.row + 1) + ', Col ' + (pos.column + 1);
                text += " " + VPLUtil.langName(VPLUtil.fileExtension(name));
                VPLUI.showIDEStatus(text);
            };

            this.open = function() {

                var containerId = 'vpl_file' + this.getId();
                var $container = $('#' + containerId);

                this.showFileName();
                if (this.isOpen()) {
                    return editor;
                }

                var fileManager = this.getFileManager();

                function afterEditorLoad() {
                    editor.setValue(self.getContent());
                    self.setOpen(true);
                    self.langSelection();
                    editor.setTheme('ace/theme/' + fileManager.getTheme());

                    $container.addClass('ui-corner-bottom')
                    .removeClass('ui-widget-content ui-tabs-panel'); // Workaround to remove jquery-ui theme background color.
                    self.adjustSize();
                    self.updateStatus();

                    editor.gotoLine(0, 0);
                    editor.setReadOnly(readOnly);
                    editor.setFontSize(fileManager.getFontSize());

                    session = editor.getSession();
                    session.setUseSoftTabs(true);
                    session.setTabSize(4);
                    session.setAnnotations(self.annotationsBuffer);

                    self.check_difference();
                    var timeoutID = null;
                    editor.on('change', function() {
                        self.change();
                        // Check for differences with required files once at a time, when the user stops typing.
                        if(timeoutID !== null){
                            clearTimeout(timeoutID);
                        }
                        timeoutID = setTimeout(() => {timeoutID = null; self.check_difference();}, 500);
                    });
                    session.selection.on('changeCursor', function() {
                        self.updateStatus();
                    });

                    if (fileManager.restrictedEdit) {
                        // Control copy and cut (cut also use this) for localClipboard.
                        editor.on('copy', function(t) {
                            fileManager.setClipboard(t);
                        });

                        editor.onPaste = function() {
                            editor.insert(fileManager.getClipboard());
                        };
                    }
                }

                if (fileManager.getCurrentEditor() == 'monaco') {

                    $container.html(
                            '<div id="loading-icon_vpl_file' + this.getId() + '"' +
                                'class="w-100 h-100 d-flex align-items-center justify-content-center">' +
                            '<div class="text-center">' +
                            '<i class="fa fa-pulse fa-spinner" style="font-family: FontAwesome; font-size: 28px;"></i>' +
                            '<h4 class="d-inline-block align-text-bottom ml-1 mb-0">' + VPLUtil.str('loading') + '</h4>' +
                            '<div id="monaco-notloading-notice' + this.getId() + '" class="text-center invisible">' +
                                VPLUtil.str('monaconotloadingnotice') +
                            '</div>' +
                            '</div>' +
                            '</div>'
                    );
                    setTimeout(function() {
                        $('#monaco-notloading-notice' + self.getId()).removeClass('invisible');
                    }, 4000);
                    require(['mod_vpl/monacoeditor'], function(MonacoEditor) {
                        $('#loading-icon_vpl_file' + self.getId()).remove();
                        editor = MonacoEditor.create($container[0]);

                        afterEditorLoad();

                        return editor;
                    });
                } else if (fileManager.getCurrentEditor() == 'ace') {

                    var openAceEditor = function() {
                        ace.require("ext/language_tools");
                        editor = ace.edit(containerId);
                        editor.$blockScrolling = Infinity;

                        editor.setOptions({
                            enableBasicAutocompletion: true,
                            enableSnippets: true,
                        });

                        afterEditorLoad();

                        // Avoid undo of editor initial content.
                        session.setUndoManager(new ace.UndoManager());

                        return editor;
                    };

                    if (typeof ace === 'undefined') {
                        VPLUtil.loadScript(['/ace9/ace.js',
                            '/ace9/ext-language_tools.js'],
                            openAceEditor
                       );
                    } else {
                        return openAceEditor();
                    }

                }
            };
            this.close = function() {
                this.setOpen(false);
                if (editor === null) {
                    return;
                }
                this.setContent(editor.getValue());
                editor.destroy();
                editor = null;
                session = null;
            };
        };
    }
);
